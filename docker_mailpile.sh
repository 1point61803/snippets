#!/bin/bash

# create share folder
mkdir -p /share_docker/mailpile_conf /share_docker/mailpile_gpg
chown 1000:1000 -R /share_docker/mailpile_conf /share_docker/mailpile_gpg

# create tmp network
docker network create net

# get mailpile image
docker pull rroemhild/mailpile

# create mailpile container
docker create --name CTX_mailpile --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/mailpile_conf:/root/.local/share/Mailpile -v /share_docker/mailpile_gpg:/root/.gnupg --network net -p 33411:33411 --log-driver syslog --log-opt tag=DOCKER_mailpile --log-opt syslog-address=udp://10.20.0.30:514 rroemhild/mailpile

# start container
docker start CTX_mailpile

# one shitty line
mkdir -p /share_docker/mailpile_conf /share_docker/mailpile_gpg && chown 1000:1000 -R /share_docker/mailpile_conf /share_docker/mailpile_gpg && docker network create net && docker pull rroemhild/mailpile && docker create --name CTX_mailpile --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/mailpile_conf:/root/.local/share/Mailpile -v /share_docker/mailpile_gpg:/root/.gnupg --network net -p 33411:33411 --log-driver syslog --log-opt tag=DOCKER_mailpile --log-opt syslog-address=udp://10.20.0.30:514 rroemhild/mailpile && docker start CTX_mailpile
