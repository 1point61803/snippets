#!/bin/bash

# create share folder
chown 1000:1000 -R /root/mldonkey_conf

# create tmp network
docker network create net

# get mldonkey image
docker pull chourmovs/mldonkey_arch:main

# create mldonkey container
docker create --name CTX_mldonkey --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /root/mldonkey_conf:/var/lib/mldonkey:rw -v /mnt/misc/P2P/TEMP:/mnt/mldonkey_tmp:rw -v /mnt/misc/P2P/INCOMING:/mnt/mldonkey_completed:rw -v /mnt/video:/mnt/video:ro -v /mnt/docstheatre:/mnt/docstheatre:ro -v /mnt/animated:/mnt/animated/:ro --network net -h mldonkey -p 4000:4000 -p 4001:4001 -p 4080:4080 -p 20562:20562 -p 20566:20566/udp -p 16965:16965 -p 16965:16965/udp --log-driver syslog --log-opt tag=DOCKER_mldonkey --log-opt syslog-address=udp://10.20.0.30:514 chourmovs/mldonkey_arch:main

# start container
docker start CTX_mldonkey

# one shitty line
chown 1000:1000 -R /root/mldonkey_conf & docker network create net && docker pull chourmovs/mldonkey_arch:main && docker create --name CTX_mldonkey --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /root/mldonkey_conf:/var/lib/mldonkey:rw -v /mnt/misc/P2P/TEMP:/mnt/mldonkey_tmp:rw -v /mnt/misc/P2P/INCOMING:/mnt/mldonkey_completed:rw -v /mnt/video:/mnt/video:ro -v /mnt/docstheatre:/mnt/docstheatre:ro -v /mnt/animated:/mnt/animated/:ro --network net -h mldonkey -p 4000:4000 -p 4001:4001 -p 4080:4080 -p 20562:20562 -p 20566:20566/udp -p 16965:16965 -p 16965:16965/udp --log-driver syslog --log-opt tag=DOCKER_mldonkey --log-opt syslog-address=udp://10.20.0.30:514 chourmovs/mldonkey_arch:main && docker start CTX_mldonkey


# new version
# docker create --name CTX_mldonkey --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /root/mldonkey_conf:/config:rw -v /mnt/misc/P2P/TEMP:/mnt/mldonkey_tmp:rw -v /mnt/misc/P2P/INCOMING:/mnt/mldonkey_completed:rw -v /mnt/video:/mnt/video:ro -v /mnt/docstheatre:/mnt/docstheatre:ro -v /mnt/animated:/mnt/animated/:ro --network net -h mldonkey -p 4000:4000 -p 4001:4001 -p 4080:4080 -p 20562:20562 -p 20566:20566/udp -p 16965:16965 -p 16965:16965/udp --log-driver syslog --log-opt tag=DOCKER_mldonkey --log-opt syslog-address=udp://10.20.0.30:514 registry.gitlab.com/ricmaco/docker-mldonkey-server:latest

# new version
# docker create --name CTX_mldonkey --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /root/mldonkey_conf:/var/lib/mldonkey:rw -v /mnt/misc/P2P/TEMP:/mnt/mldonkey_tmp:rw -v /mnt/misc/P2P/INCOMING:/mnt/mldonkey_completed:rw -v /mnt/video:/mnt/video:ro -v /mnt/docstheatre:/mnt/docstheatre:ro -v /mnt/animated:/mnt/animated/:ro --network net -h mldonkey -p 4000:4000 -p 4001:4001 -p 4080:4080 -p 20562:20562 -p 20566:20566/udp -p 16965:16965 -p 16965:16965/udp --log-driver syslog --log-opt tag=DOCKER_mldonkey --log-opt syslog-address=udp://10.20.0.30:514 wibol/mldonkey-ubuntu
