#!/bin/bash

# create tmp network
docker network create net

# get mariadb image
docker pull openspeedtest/latest:latest

# create mariadb container
docker create --name CTX_openspeedtest --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 3380:3000 -p 3001:3001 --log-driver syslog --log-opt tag=DOCKER_openspeedtest --log-opt syslog-address=udp://10.20.0.30:514 openspeedtest/latest:latest

# start container
docker start CTX_openspeedtest
 
# one shitty line
docker network create net && docker pull openspeedtest/latest:latest && docker create --name CTX_openspeedtest --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 3380:3000 -p 3001:3001 --log-driver syslog --log-opt tag=DOCKER_openspeedtest --log-opt syslog-address=udp://10.20.0.30:514 openspeedtest/latest:latest && docker start CTX_openspeedtest
