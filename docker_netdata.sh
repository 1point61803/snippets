#!/bin/bash

# on port 19999 http://<IP>:19999

# create persistent folder
mkdir -p /share_docker/netdataconfig /share_docker/netdatalib /share_docker/netdatacache
chown 1000:1000 -R /share_docker/*

# get mnetdata image
docker pull  netdata/netdata

# create netdata container
docker create --name CTX_netdata --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --pid=host --network=host -v /share_docker/netdataconfig:/etc/netdata -v  /share_docker/netdatalib:/var/lib/netdata -v /share_docker/netdatacache:/var/cache/netdata -v /etc/passwd:/host/etc/passwd:ro -v /etc/group:/host/etc/group:ro -v /proc:/host/proc:ro -v /sys:/host/sys:ro -v /etc/os-release:/host/etc/os-release:ro -v /var/run/docker.sock:/var/run/docker.sock:ro --restart unless-stopped --cap-add SYS_PTRACE --cap-add SYS_ADMIN --security-opt apparmor=unconfined  --log-driver syslog --log-opt tag=DOCKER_netdata --log-opt syslog-address=udp://10.20.0.30:514   netdata/netdata

# start container
docker start CTX_netdata

# one shitty line
mkdir -p /share_docker/netdataconfig /share_docker/netdatalib /share_docker/netdatacache && chown 1000:1000 -R /share_docker/* && docker pull  netdata/netdata && docker create --name CTX_netdata --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --pid=host --network=host -v /share_docker/netdataconfig:/etc/netdata -v  /share_docker/netdatalib:/var/lib/netdata -v /share_docker/netdatacache:/var/cache/netdata -v /etc/passwd:/host/etc/passwd:ro -v /etc/group:/host/etc/group:ro -v /proc:/host/proc:ro -v /sys:/host/sys:ro -v /etc/os-release:/host/etc/os-release:ro -v /var/run/docker.sock:/var/run/docker.sock:ro --restart unless-stopped --cap-add SYS_PTRACE --cap-add SYS_ADMIN --security-opt apparmor=unconfined  --log-driver syslog --log-opt tag=DOCKER_netdata --log-opt syslog-address=udp://10.20.0.30:514   netdata/netdata && docker start CTX_netdata
