#!/bin/bash

# create persistent folder
mkdir -p /share_docker/mysql_data /share_docker/ocs_data/perlcomdata /share_docker/ocs_data/ocsreportsdata /share_docker/ocs_data/varlibdata /share_docker/ocs_data/httpdconfdata /share_docker/init /share_docker/nginx_data/auth /share_docker/nginx_data/certs /share_docker/nginx_data/conf
wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/sql/ocsbase.sql -P /share_docker/init
wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/auth/ocsapi.htpasswd -P /share_docker/nginx_data/auth
wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/certs/ocs-dummy.crt -P /share_docker/nginx_data/certs
wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/certs/ocs-dummy.key -P /share_docker/nginx_data/certs
wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/conf/ocsinventory.conf.template -P /share_docker/nginx_data/conf
chown 1000:1000 -R /share_docker/*
  
# create tmp network
docker network create net

# get mysql image
docker pull mysql:8.0

# create mysql container
docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_USER=$OC_db_user -e MYSQL_PASSWORD=$OC_db_password -e  MYSQL_ROOT_PASSWORD=$OC_db_password -e MYSQL_DATABASE=$OC_db_name -v /share_docker/mysqldb_data:/var/lib/mysql --network net -h $OC_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_oc_mysql --log-opt syslog-address=udp://10.20.0.30:514 mysql:8.0

# get ocs core image
docker pull ocsinventory/ocsinventory-docker-image:2.9.2

# create ocs container
docker create --name CTX_ocs --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OCS_DB_SERVER=$OC_db_host -e OCS_DB_USER=$OC_db_user -e OCS_DB_PASS=$OC_db_password -e OCS_DB_NAME=$OC_db_name -e OCS_SSL_ENABLED=0 -v /share_docker/ocs_data/perlcomdata:/etc/ocsinventory-server -v /share_docker/ocs_data/ocsreportsdata:/usr/share/ocsinventory-reports/ocsreports/extensions -v /share_docker/ocs_data/varlibdata:/var/lib/ocsinventory-reports -v /share_docker/ocs_data/httpdconfdata:/etc/apache2/conf-available --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_oc_ocs --log-opt syslog-address=udp://10.20.0.30:514 ocsinventory/ocsinventory-docker-image:2.9.2

# get nginx image
docker pull nginx

# create nginx container
docker create --name CTX_nginx --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LISTEN_PORT=80 -e PORT_TYPE="" -e SSL_CERT=ocs-dummy.crt -e SSL_KEY=ocs-dummy.key -e  API_AUTH_FILE=ocsapi.htpasswd -e READ_TIMEOUT=300 -e CONNECT_TIMEOUT=300 -e SEND_TIMEOUT=300 -e MAX_BODY_SIZE=1G -v /share_docker/nginx/conf:/etc/nginx/templates -v /share_docker/nginx/certs:/etc/nginx/certs -v /share_docker/nginx/auth:/etc/nginx/auth --network net -p 80:80 -p 443:443 --log-driver syslog --log-opt tag=DOCKER_oc_mysql --log-opt syslog-address=udp://10.20.0.30:514 nginx

# start container
docker start CTX_mysql 
docker start CTX_ocs
docker start CTX_nginx

# one shitty line
mkdir -p /share_docker/mysql_data /share_docker/ocs_data/{perlcomdata,ocsreportsdata,varlibdata,httpdconfdata} /share_docker_init /share_docker/nginx_data/{auth,certs,conf} && wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/sql/ocsbase.sql /share_docker/init && wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/auth/ocsapi.htpasswd /share_docker/nginx_data/auth && wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/certs/ocs-dummy.crt /share_docker/nginx_data/cert && wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/certs/ocs-dummy.key /share_docker/nginx_data/cert && wget https://raw.githubusercontent.com/OCSInventory-NG/OCSInventory-Docker-Image/master/2.9.2/nginx/conf/ocsinventory.conf.template /share_docker/nginx_conf && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull mysql:8.0 && docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_USER=$OC_db_user -e MYSQL_PASSWORD=$OC_db_password -e  MYSQL_ROOT_PASSWORD=$OC_db_password -e MYSQL_DATABASE=$OC_db_name -v /share_docker/mysqldb_data:/var/lib/mysql --network net -h $OC_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_oc_mysql --log-opt syslog-address=udp://10.20.0.30:514 mysql:8.0 && docker pull ocsinventory/ocsinventory-docker-image:2.9.2 && docker create --name CTX_ocs --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OCS_DB_SERVER=$OC_db_host -e OCS_DB_USER=$OC_db_user -e OCS_DB_PASS=$OC_db_password -e OCS_DB_NAME=$OC_db_name -e OCS_SSL_ENABLED=0 -v /share_docker/ocs_data/perlcomdata:/etc/ocsinventory-server -v /share_docker/ocs_data/ocsreportsdata:/usr/share/ocsinventory-reports/ocsreports/extensions -v /share_docker/ocs_data/varlibdata:/var/lib/ocsinventory-reports -v /share_docker/ocs_data/httpdconfdata:/etc/apache2/conf-available --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_oc_ocs --log-opt syslog-address=udp://10.20.0.30:514 ocsinventory/ocsinventory-docker-image:2.9.2 && docker pull nginx && docker create --name CTX_nginx --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LISTEN_PORT=80 -e PORT_TYPE="" -e SSL_CERT=ocs-dummy.crt -e SSL_KEY=ocs-dummy.key -e  API_AUTH_FILE=ocsapi.htpasswd -e READ_TIMEOUT=300 -e CONNECT_TIMEOUT=300 -e SEND_TIMEOUT=300 -e MAX_BODY_SIZE=1G -v /share_docker/nginx/conf:/etc/nginx/templates -v /share_docker/nginx/certs:/etc/nginx/certs -v /share_docker/nginx/auth:/etc/nginx/auth --network net -p 80:80 -p 443:443 --log-driver syslog --log-opt tag=DOCKER_oc_mysql --log-opt syslog-address=udp://10.20.0.30:514 nginx && docker start CTX_mysql && docker start CTX_ocs && docker start CTX_nginx
