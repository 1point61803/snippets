#!/bin/bash

if [[ $# -eq 1 ]]; then
	FF3ver=$1
	STR_date=$(date +%d/%m/%Y)

	echo "FireFly-iii version: $FF3ver"
	
	echo "change working directory and get new version then move env file and older files"
	cd /var/www/
	composer create-project grumpydictator/firefly-iii --no-dev --prefer-dist firefly-iii-updated $FF3
	ls -la | grep firefly-iii
	cp firefly-iii/.env firefly-iii-updated/.env
	cp firefly-iii/storage/upload/* firefly-iii-updated/storage/upload/
	cp firefly-iii/storage/export/* firefly-iii-updated/storage/export/
	
	echo "enter to new firefly directory and prepare and upgrade DB "
	cd firefly-iii-updated
	rm -rf bootstrap/cache/*
	php artisan cache:clear
	php artisan migrate --seed
	php artisan firefly-iii:upgrade-database
	php artisan passport:install
	php artisan cache:clear
	cd ..

	echo "swap older to newer direcoty"
	mv firefly-iii firefly-iii-old-$STR_date
	mv firefly-iii-updated firefly-iii

	echo "change owner and permission"
    chown -R www-data:www-data firefly-iii
	chmod -R 775 firefly-iii/storage	

	echo "restart apache2 service"
	systemctl restart apache2.service	

	exit 0
else
	echo "You need to pass the new FireFly-iii version release"
	echo "Usage: update_FF3.sh <VERSION>"
	exit 1
fi
