#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/roundcube /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/mariadb_data /share_docker/roundcube

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$RC_db_user -e MARIADB_PASSWORD=$RC_db_passwd -e  MARIADB_ROOT_PASSWORD=$RC_db_passwd -e MYSQL_DATABASE=$RC_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h db -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_rc_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get roundcube image
docker pull roundcube/roundcubemail:latest

# create roundcube container
docker create --name CTX_roundcube --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ROUNDCUBEMAIL_DB_TYPE=mysql -e ROUNDCUBEMAIL_DB_HOST=db -e ROUNDCUBEMAIL_DB_PASSWORD=$RC_db_passwd -e ROUNDCUBEMAIL_DB_USER=$RC_db_user -e ROUNDCUBEMAIL_DB_NAME=$RC_db_name -e ROUNDCUBEMAIL_SKIN=elastic -v /share_docker/roundcube:/var/www/html --network net -p 9001:80 --log-driver syslog --log-opt tag=DOCKER_roundcube --log-opt syslog-address=udp://10.20.0.30:514 roundcube/roundcubemail:latest

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_roundcube

# install plug-ins
docker exec -it CTX_roundcube composer.phar require roundcube/carddav
docker exec -it CTX_roundcube composer.phar require johndoh/contextmenu
docker exec -it CTX_roundcube composer.phar require kitist/html5_notifier
docker exec -it CTX_roundcube composer.phar require jfcherng-roundcube/show-folder-size
docker exec -it CTX_roundcube composer.phar require texxasrulez/additional_imap
docker exec -it CTX_roundcube composer.phar require texxasrulez/account_details
docker exec -it CTX_roundcube composer.phar requiredummyluck/login_view
docker exec -it CTX_roundcube composer.phar elm/identity_smtp
docker exec -it CTX_roundcube composer.phar roundcube/elastic4mobile

# one shitty line
mkdir -p /share_docker/roundcube /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/mariadb_data /share_docker/roundcube && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$RC_db_user -e MARIADB_PASSWORD=$RC_db_passwd -e  MARIADB_ROOT_PASSWORD=$RC_db_passwd -e MYSQL_DATABASE=$RC_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 34010:5432 -p 33006:3306 --log-driver syslog --log-opt tag=DOCKER_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull roundcube/roundcubemail:latest && docker create --name CTX_roundcube --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ROUNDCUBEMAIL_DB_TYPE=mysql -e ROUNDCUBEMAIL_DB_HOST=CTX_mariadb -e ROUNDCUBEMAIL_DB_PASSWORD=$RC_db_passwd -e ROUNDCUBEMAIL_DB_USER=$RC_db_user -e ROUNDCUBEMAIL_DB_NAME=$RC_db_name -e ROUNDCUBEMAIL_SKIN=elastic -e ROUNDCUBEMAIL_DEFAULT_HOST=tls://mail.homelab.local -e ROUNDCUBEMAIL_SMTP_SERVER=tls://mail.homelab.local -v /share_docker/roundcube:/var/www/html --network net -p 9001:80 --log-driver syslog --log-opt tag=DOCKER_roundcube --log-opt syslog-address=udp://10.20.0.30:514 roundcube/roundcubemail:latest && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_roundcube
