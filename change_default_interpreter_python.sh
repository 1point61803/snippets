#!/bin/sh
# list of python interpreter
ls -l /usr/bin/python*
update-alternatives --list python

# status of the active and priority interpreter
update-alternatives --display python
update-alternatives --query python

# set active one specific binary
update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2
