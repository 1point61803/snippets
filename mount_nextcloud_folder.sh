#!/bin/bash

# include secret
source SECRET

echo "Import files from NextCloud folder" 

nextcloudcmd --trust --user $PM_nc_user --password $PM_nc_password /root/IMPORT $PM_nc_addr &> /var/log/NC_importer.log

# for gnome online account use this path
http://10.40.0.50/remote.php
