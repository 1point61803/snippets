#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/penpot_postgres_data /share_docker/penpot_assets_data
chown 1000:1000 -R /share_docker/penpot_postgres_data /share_docker/penpot_assets_data

# get and mondify config file
wget -P /share_docker https://raw.githubusercontent.com/penpot/penpot/main/docker/images/config.env
sed -r -i "s/(PENPOT_DATABASE_USERNAME=\s*).*/\1$PE_db_user/" /share_docker/config.env
sed -r -i "s/(PENPOT_DATABASE_PASSWORD\s*=\s*).*/\1$PE_db_password/" /share_docker/config.env
chown 1000:1000 -R /share_docker/config.env

# create tmp network
docker network create net

# get redis image
docker pull redis:6

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net  --log-driver syslog --log-opt tag=DOCKER_pe_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:6

# get postgres image
docker pull postgres:13

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_INITDB_ARGS=--data-checksums -e POSTGRES_DB=$PE_db_name -e POSTGRES_USER=$PE_db_user -e POSTGRES_PASSWORD=$PE_db_password -v /share_docker/penpot_postgres_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_pe_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13

# get penpot exporter image
docker pull penpotapp/exporter:latest

# create penpot exporter container
docker create --name CTX_pe_exporter --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PENPOT_PUBLIC_URI=http://penpot-frontend --network net --log-driver syslog --log-opt tag=DOCKER_pe_exporter --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/exporter:latest

# get penpot backend image
docker pull penpotapp/backend:latest

# create penpot backend container
docker create --name CTX_pe_backend --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/penpot_assets_data:/opt/data --network net --log-driver syslog --log-opt tag=DOCKER_pe_backend --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/backend:latest

# get penpot frontend image
docker pull penpotapp/frontend:latest

# create penpot frontend container
docker create --name CTX_pe_frontend --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/penpot_assets_data:/opt/data --network net -p 9001:80  --log-driver syslog --log-opt tag=DOCKER_pe_frontend --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/frontend:latest

# start container
docker start CTX_redis
docker start CTX_postgres
docker start CTX_pe_exporter
docker start CTX_pe_backend
docker start CTX_pe_frontend

# one shitty line
mkdir -p /share_docker/penpot_postgres_data /share_docker/penpot_assets_data && chown 1000:1000 -R /share_docker/penpot_postgres_data /share_docker/penpot_assets_data && wget -P /share_docker https://raw.githubusercontent.com/penpot/penpot/main/docker/images/config.env && sed -r -i "s/(PENPOT_DATABASE_USERNAME=\s*).*/\1$PE_db_user/" /share_docker/config.env && sed -r -i "s/(PENPOT_DATABASE_PASSWORD\s*=\s*).*/\1$PE_db_password/" /share_docker/config.env && chown 1000:1000 -R /share_docker/config.env && docker network create net && docker pull redis:6 && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net  --log-driver syslog --log-opt tag=DOCKER_pe_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:6 && docker pull postgres:13 && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_INITDB_ARGS=--data-checksums -e POSTGRES_DB=$PE_db_name -e POSTGRES_USER=$PE_db_user -e POSTGRES_PASSWORD=$PE_db_password -v /share_docker/penpot_postgres_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_pe_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13 && docker pull penpotapp/exporter:latest && docker create --name CTX_pe_exporter --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PENPOT_PUBLIC_URI=http://penpot-frontend --network net --log-driver syslog --log-opt tag=DOCKER_pe_exporter --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/exporter:latest && docker pull penpotapp/backend:latest && docker create --name CTX_pe_backend --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/penpot_assets_data:/opt/data --network net --log-driver syslog --log-opt tag=DOCKER_pe_backend --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/backend:latest && docker pull penpotapp/frontend:latest && docker create --name CTX_pe_frontend --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/penpot_assets_data:/opt/data --network net -p 9001:80  --log-driver syslog --log-opt tag=DOCKER_pe_frontend --log-opt syslog-address=udp://10.20.0.30:514 penpotapp/frontend:latest && docker start CTX_redis && docker start CTX_postgres && docker start CTX_pe_exporter && docker start CTX_pe_backend && docker start CTX_pe_frontend




