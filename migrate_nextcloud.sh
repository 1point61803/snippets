#!/bin/bash

    Install nc on the new server.
    Set the old and new server to maintenance mode.
    Dump the database on the old.
    Copy the complete data directory to the new server.
    Restore the database on the new server.
    Edit the config.php on the new server. I think you have to copy passwordsalt and secret from the old to the new server.
    Change the DNS record to point at your new server.
    Turn off maintenance mode on the new server.

