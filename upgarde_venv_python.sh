#!/bin/bash

#active venv
cd /path/to/src_prj
source bin/activate

#create list of requeriments pkgs
pip freeze > requirements.txt

#upgrade it 
pip install -r requirements.txt --upgrade
