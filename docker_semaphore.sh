#!/bin/bash

# include secret
source SECRET

# create persistent folder
mkdir -p /share_docker/mariadb_data /share_docker/semaphore_conf /share_docker/semaphore_plb
chown 1001:1001 -R /share_docker/mariadb_data /share_docker/semaphore_conf /share_docker/semaphore_plb
  
# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1001 -e PGID=1001 -e MARIADB_USER=$SM_db_user -e MARIADB_PASSWORD=$SM_db_password -e  MARIADB_ROOT_PASSWORD=$SM_db_password -e MYSQL_DATABASE=$SM_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h $SM_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_sm_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1001 -e PGID=1001 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_sm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get semaphore image
docker pull semaphoreui/semaphore:latest

# create semaphore container
docker create --name CTX_semaphore --restart unless-stopped -e PUID=1001 -e PGID=1001 -e TZ=Europe/Rome -e SEMAPHORE_DB_DIALECT=$SM_db_dialect -e SEMAPHORE_ADMIN_PASSWORD=$SM_admin_pwd -e SEMAPHORE_ADMIN_NAME=$SM_admin_name -e SEMAPHORE_ADMIN_EMAIL=$SM_admin_mail -e SEMAPHORE_ADMIN=$SM_admin -e SEMAPHORE_DB_USER=$SM_db_user -e SEMAPHORE_DB_PASS=$SM_db_password -e SEMAPHORE_DB_HOST=$SM_db_host -e SEMAPHORE_DB_PORT=3306 -e SEMAPHORE_DB_DIALECT=mysql -e  SEMAPHORE_DB=$SM_db_name -e SEMAPHORE_PLAYBOOK_PATH=/tmp/semaphore -e SEMAPHORE_ACCESS_KEY_ENCRYPTION=$SM_db_encr -v /share_docker/semaphore_plb:/tmp/semaphore -v /share_docker/semaphore_conf:/etc/semaphore --network net -p 3000:3000 --log-driver syslog --log-opt tag=DOCKER_semaphore --log-opt syslog-address=udp://10.20.0.30:514 semaphoreui/semaphore:latest

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_semaphore

# one shitty line
mkdir -p /share_docker/mariadb_data /share_docker/semaphore_conf /share_docker/semaphore_plb && chown 1001:1001 -R /share_docker/mariadb_data /share_docker/semaphore_conf /share_docker/semaphore_plb && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1001 -e PGID=1001 -e MARIADB_USER=$SM_db_user -e MARIADB_PASSWORD=$SM_db_password -e  MARIADB_ROOT_PASSWORD=$SM_db_password -e MYSQL_DATABASE=$SM_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h $SM_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_sm_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1001 -e PGID=1001 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_sm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull semaphoreui/semaphore:latest && docker create --name CTX_semaphore --restart unless-stopped -e PUID=1001 -e PGID=1001 -e TZ=Europe/Rome -e SEMAPHORE_DB_DIALECT=$SM_db_dialect -e SEMAPHORE_ADMIN_PASSWORD=$SM_admin_pwd -e SEMAPHORE_ADMIN_NAME=$SM_admin_name -e SEMAPHORE_ADMIN_EMAIL=$SM_admin_mail -e SEMAPHORE_ADMIN=$SM_admin -e SEMAPHORE_DB_USER=$SM_db_user -e SEMAPHORE_DB_PASS=$SM_db_password -e SEMAPHORE_DB_HOST=$SM_db_host -e SEMAPHORE_DB_PORT=3306 -e SEMAPHORE_DB_DIALECT=mysql -e  SEMAPHORE_DB=$SM_db_name -e SEMAPHORE_PLAYBOOK_PATH=/tmp/semaphore -e SEMAPHORE_ACCESS_KEY_ENCRYPTION=$SM_db_encr -v /share_docker/semaphore_plb:/tmp/semaphore -v /share_docker/semaphore_conf:/etc/semaphore --network net -p 3000:3000 --log-driver syslog --log-opt tag=DOCKER_semaphore --log-opt syslog-address=udp://10.20.0.30:514 semaphoreui/semaphore:latest && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_semaphore
