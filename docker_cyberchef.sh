#!/bin/bash

# create tmp network
docker network create net

# get ytdl image
docker pull mpepping/cyberchef

# create ytdl container
docker create --name CTX_cyberchef --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_cyerchef --log-opt syslog-address=udp://10.20.0.30:514 mpepping/cyberchef

# start container
docker start CTX_cyberchef
 
# one shitty line
docker network create net && docker pull mpepping/cyberchef && docker create --name CTX_cyberchef --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_cyerchef --log-opt syslog-address=udp://10.20.0.30:514 mpepping/cyberchef && docker start CTX_cyberchef
