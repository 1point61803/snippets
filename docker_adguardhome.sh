#!/bin/bash

# get image
docker pull adguard/adguardhome

# create share folder
mkdir -p /share_docker/workdir /share_docker/confdir
chown 1000:1000 -R /share_docker/workdir /share_docker/confdir

# create container
docker create --name adguardhome --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/workdir:/opt/adguardhome/work -v /share_docker/confdir:/opt/adguardhome/conf -p 53:53/tcp -p 53:53/udp -p 67:67/udp -p 68:68/udp -p 80:80/tcp -p 443:443/tcp -p 443:443/udp -p 3000:3000/tcp -p 853:853/tcp -p 784:784/udp -p 853:853/udp -p 8853:8853/udp -p 5443:5443/tcp -p 5443:5443/udp --log-driver syslog --log-opt tag=DOCKER_adguardhome --log-opt syslog-address=udp://10.20.0.30:514 adguard/adguardhome

# start container
docker start adguardhome

# one shitty line
docker pull adguard/adguardhome && mkdir -p /share_docker/workdir /share_docker/confdir && chown 1000:1000 -R /share_docker/workdir /share_docker/confdir && docker create --name adguardhome --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/workdir:/opt/adguardhome/work -v /share_docker/confdir:/opt/adguardhome/conf -p 53:53/tcp -p 53:53/udp -p 67:67/udp -p 68:68/udp -p 80:80/tcp -p 443:443/tcp -p 443:443/udp -p 3000:3000/tcp -p 853:853/tcp -p 784:784/udp -p 853:853/udp -p 8853:8853/udp -p 5443:5443/tcp -p 5443:5443/udp --log-driver syslog --log-opt tag=DOCKER_adguardhome --log-opt syslog-address=udp://10.20.030:514 adguard/adguardhome && docker start adguardhome
