 #!/bin/bash

# create share folder
mkdir -p /share_docker/ytdl_conf
chown 1000:1000 -R /share_docker/ytdl_conf

# create tmp network
docker network create net

# get ytdl image
docker pull ghcr.io/jmbannon/ytdl-sub:latest

# create ytdl container
docker create --name CTX_ytdl --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/ytdl_conf:/config --mount type=bind,source=/mnt/misc/TEMP_YT,target=/tv_shows --network net --log-driver syslog --log-opt tag=DOCKER_ytdl --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/jmbannon/ytdl-sub:latest

# start container
docker start CTX_ytdl
 
# one shitty line
mkdir -p /share_docker/ytdl_conf && chown 1000:1000 -R /share_docker/ytdl_conf && docker network create net && docker pull ghcr.io/jmbannon/ytdl-sub:latest && docker create --name CTX_ytdl --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/ytdl_conf:/config --mount type=bind,source=/mnt/misc/TEMP_YT,target=/tv_shows --network net --log-driver syslog --log-opt tag=DOCKER_ytdl --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/jmbannon/ytdl-sub:latest && docker start CTX_ytdl
