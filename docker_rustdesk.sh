#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/rustdesk_S /share_docker/rustdesk_R
chown 1000:1000 -R /share_docker/rustdesk_S /share_docker/rustdesk_R

# create tmp network
docker network create net

# get rustdeck image
docker pull rustdesk/rustdesk-server

# create rustdeck server  container
docker create --name CTX_rustdeckS --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rustdesk_S:/root --network net -h rustdecks -p 21115:21115 -p 21116:21116 -p 21116:21116/udp -p 21118:21118 --log-driver syslog --log-opt tag=DOCKER_RD_server --log-opt syslog-address=udp://10.20.0.30:514 rustdesk/rustdesk-server hbbs -r rustdecks:21117 

# create rustdeck relay cotainer
docker create --name CTX_rustdeckR --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rustdesk_R:/root --network net -h rustdeckr -p 21117:21117 -p 21119:21119 --log-driver syslog --log-opt tag=DOCKER_RD_relay --log-opt syslog-address=udp://10.20.0.30:514  rustdesk/rustdesk-server hbbr


# start container
docker start CTX_rustdeckS
docker start CTX_rustdeckR


# one shitty line
mkdir -p /share_docker/rustdesk_S /share_docker/rustdesk_R && chown 1000:1000 -R /share_docker/rustdesk_S /share_docker/rustdesk_R && docker network create net && docker pull rustdesk/rustdesk-server && docker create --name CTX_rustdeckS --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rustdesk_S:/root --network net -h rustdecks -p 21115:21115 -p 21116:21116 -p 21116:21116/udp -p 21118:21118 --log-driver syslog --log-opt tag=DOCKER_RD_server --log-opt syslog-address=udp://10.20.0.30:514 rustdesk/rustdesk-server hbbs -r rustdecks:21117 && docker create --name CTX_rustdeckR --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rustdesk_R:/root --network net -h rustdeckr -p 21117:21117 -p 21119:21119 --log-driver syslog --log-opt tag=DOCKER_RD_relay --log-opt syslog-address=udp://10.20.0.30:514  rustdesk/rustdesk-server hbbr && docker start CTX_rustdeckS && docker start CTX_rustdeckR
