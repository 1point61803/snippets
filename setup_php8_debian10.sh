#!/bin/bash
# setup php8 on debian 10 lxc

# prepare debian for extra repo
apt install -y lsb-release ca-certificates apt-transport-https software-properties-common

# add sury repo
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add -

# update and install all the pkgs
apt update && apt install -y php8.0 php-common-8.0 php8.0-mysql libapache2-mod-php8.0 php8.0-{cli,zip,gd,fpm,json,common,mysql,zip,mbstring,curl,xml,bcmath,imap,ldap,intl}

# active php8.0 fpm
a2enmod proxy_fcgi setenvif
a2enconf php8.0-fpm



# in line cmd
apt install -y lsb-release ca-certificates apt-transport-https software-properties-common gnupg && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list && wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add - && apt update && apt install -y php8.0 php-common-8.0 php8.0-mysql libapache2-mod-php8.0 php8.0-{cli,zip,gd,fpm,json,common,mysql,zip,mbstring,curl,xml,bcmath,imap,ldap,intl} && a2enmod proxy_fcgi setenvif && a2enconf php8.0-fpm
