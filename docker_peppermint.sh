#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/postgres_data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get posgres image
docker pull  postgres:latest

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$PPM_db_user -e POSTGRES_PASSWORD=$PPM_db_password -e POSTGRES_DB=$PPM_db_name -v /share_docker/postgres_data:/var/lib/postgresql/data --network net -h postgresdb -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_ppm_postgresdb --log-opt syslog-address=udp://10.20.0.30:514  postgres:latest

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h adminer -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_ppm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get bookstack image
docker pull pepperlabs/peppermint:latest

# create bookstack container
docker create --name CTX_peppermint --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DB_USERNAME=$PPM_db_user -e DB_PASSWORD=$PPM_db_password -e DB_HOST=$PPM_db_host -e SECRET=$PPM_secret --network net -h peppermint -p 3000:3000 -p 5003:5003 -v /share_docker/bookstack_conf:/config --log-driver syslog --log-opt tag=DOCKER_ppm_peppermint --log-opt syslog-address=udp://10.20.0.30:514 pepperlabs/peppermint:latest

# start bookstack container
docker start CTX_postgres
docker start CTX_adminer
docker start CTX_peppermint

# one shitty line
mkdir -p /share_docker/postgres_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull  postgres:latest && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$PPM_db_user -e POSTGRES_PASSWORD=$PPM_db_password -e POSTGRES_DB=$PPM_db_name -v /share_docker/postgres_data:/var/lib/postgresql/data --network net -h postgresdb -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_ppm_postgresdb --log-opt syslog-address=udp://10.20.0.30:514  postgres:latest && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h adminer -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_ppm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull pepperlabs/peppermint:latest && docker create --name CTX_peppermint --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DB_USERNAME=$PPM_db_user -e DB_PASSWORD=$PPM_db_password -e DB_HOST=$PPM_db_host -e SECRET=$PPM_secret --network net -h peppermint -p 3000:3000 -p 5003:5003 -v /share_docker/bookstack_conf:/config --log-driver syslog --log-opt tag=DOCKER_ppm_peppermint --log-opt syslog-address=udp://10.20.0.30:514 pepperlabs/peppermint:latest && docker start CTX_postgres && docker start CTX_adminer && docker start CTX_peppermint
