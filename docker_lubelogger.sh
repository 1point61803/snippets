#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/ll_config /share_docker/ll_data /share_docker/ll_translations /share_docker/ll_documents /share_docker/ll_images /share_docker/ll_temp /share_docker/ll_log /share_docker/ll_keys
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get lubelogger image
docker pull ghcr.io/hargata/lubelogger:latest

# create mariadb container
docker create --name CTX_lubelogger --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LC_ALL=en_US.UTF-8 -e LANG=en_US.UTF-8 -e LOGGING__LOGLEVEL__DEFAULT=Error -v /share_docker/ll_config:/App/config -v /share_docker/ll_data:/App/data -v /share_docker/ll_translations:/App/wwwroot/translations -v /share_docker/ll_documents:/App/wwwroot/documents -v /share_docker/ll_images:/App/wwwroot/images -v /share_docker/ll_temp:/App/wwwroot/temp -v /share_docker/ll_log:/App/log -v /share_docker/ll_keys:/root/.aspnet/DataProtection-Keys --network net -h lubelogger -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_lubelogger --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/hargata/lubelogger:latest

# start bookstack container
docker start CTX_lubelogger

# one shitty line
mkdir -p /share_docker/ll_config /share_docker/ll_data /share_docker/ll_translations /share_docker/ll_documents /share_docker/ll_images /share_docker/ll_temp /share_docker/ll_log /share_docker/ll_keys && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull ghcr.io/hargata/lubelogger:latest && docker create --name CTX_lubelogger --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LC_ALL=en_US.UTF-8 -e LANG=en_US.UTF-8 -e LOGGING__LOGLEVEL__DEFAULT=Error -v /share_docker/ll_config:/App/config -v /share_docker/ll_data:/App/data -v /share_docker/ll_translations:/App/wwwroot/translations -v /share_docker/ll_documents:/App/wwwroot/documents -v /share_docker/ll_images:/App/wwwroot/images -v /share_docker/ll_temp:/App/wwwroot/temp -v /share_docker/ll_log:/App/log -v /share_docker/ll_keys:/root/.aspnet/DataProtection-Keys --network net -h lubelogger -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_lubelogger --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/hargata/lubelogger:latest && docker start CTX_lubelogger
