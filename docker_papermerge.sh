#!/bin/bash

# create share folder
mkdir -p /share_docker/config /share_docker/data
chown 1000:1000 -R /share_docker/config /share_docker/data

# get papermerge image
docker pull lscr.io/linuxserver/papermerge

# create papermerge container
docker create --name papermerge --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/config:/config -v /share_docker/data:/data -p 8000:8000  --log-driver syslog --log-opt tag=DOCKER_papermerge --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/papermerge

# start container
docker start papermerge

# one shitty line
mkdir -p /share_docker/config /share_docker/data && chown 1000:1000 -R /share_docker/config /share_docker/data && docker pull lscr.io/linuxserver/papermerge && docker create --name papermerge --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/config:/config -v /share_docker/data:/data -p 8000:8000  --log-driver syslog --log-opt tag=DOCKER_papermerge --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/papermerge && docker start papermerge

