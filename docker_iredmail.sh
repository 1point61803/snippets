#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/iredmail/data/backup-mysql /share_docker/iredmail/data/mailboxes /share_docker/iredmail/data/mlmmj /share_docker/iredmail/data/mlmmj-archive /share_docker/iredmail/data/imapsieve_copy /share_docker/iredmail/data/custom /share_docker/iredmail/data/ssl /share_docker/iredmail/data/mysql /share_docker/iredmail/data/clamav /share_docker/iredmail/data/sa_rules /share_docker/iredmail/data/postfix_queue

# get dcker conf
wget https://raw.githubusercontent.com/iredmail/dockerized/master/iredmail-docker.conf -O /share_docker/iredmail/iredmail-docker.conf

chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get iredmail image
docker pull iredmail/mariadb:stable

# create iredmail container
docker create --name CTX_iredmail --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/iredmail/iredmail-docker.conf --network net -h 1point61803.com -p 80:80 -p 443:443 -p 110:110 -p 995:995 -p 143:143 -p 993:993 -p 25:25 -p 465:465 -p 587:587 -v /share_docker/iredmail/data/backup-mysql:/var/vmail/backup/mysql -v /share_docker/iredmail/data/mailboxes:/var/vmail/vmail1 -v /share_docker/iredmail/data/mlmmj:/var/vmail/mlmmj -v /share_docker/iredmail/data/mlmmj-archive:/var/vmail/mlmmj-archive -v /share_docker/iredmail/data/imapsieve_copy:/var/vmail/imapsieve_copy -v /share_docker/iredmail/data/custom:/opt/iredmail/custom -v /share_docker/iredmail/data/ssl:/opt/iredmail/ssl -v /share_docker/iredmail/data/mysql:/var/lib/mysql -v /share_docker/iredmail/data/clamav:/var/lib/clamav -v /share_docker/iredmail/data/sa_rules:/var/lib/spamassassin -v /share_docker/iredmail/data/postfix_queue:/var/spool/postfix --log-driver syslog --log-opt tag=DOCKER_iredmail --log-opt syslog-address=udp://10.20.0.30:514 iredmail/mariadb:stable

# start iredmail container
docker start CTX_iredmail

# one shitty line
mkdir -p /share_docker/iredmail/data/backup-mysql /share_docker/iredmail/data/mailboxes /share_docker/iredmail/data/mlmmj /share_docker/iredmail/data/mlmmj-archive /share_docker/iredmail/data/imapsieve_copy /share_docker/iredmail/data/custom /share_docker/iredmail/data/ssl /share_docker/iredmail/data/mysql /share_docker/iredmail/data/clamav /share_docker/iredmail/data/sa_rules /share_docker/iredmail/data/postfix_queue && wget https://raw.githubusercontent.com/iredmail/dockerized/master/iredmail-docker.conf -O /share_docker/iredmail/iredmail-docker.conf && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull iredmail/mariadb:stable && docker create --name CTX_iredmail --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/iredmail/iredmail-docker.conf --network net -h 1point61803.com -p 80:80 -p 443:443 -p 110:110 -p 995:995 -p 143:143 -p 993:993 -p 25:25 -p 465:465 -p 587:587 -v /share_docker/iredmail/data/backup-mysql:/var/vmail/backup/mysql -v /share_docker/iredmail/data/mailboxes:/var/vmail/vmail1 -v /share_docker/iredmail/data/mlmmj:/var/vmail/mlmmj -v /share_docker/iredmail/data/mlmmj-archive:/var/vmail/mlmmj-archive -v /share_docker/iredmail/data/imapsieve_copy:/var/vmail/imapsieve_copy -v /share_docker/iredmail/data/custom:/opt/iredmail/custom -v /share_docker/iredmail/data/ssl:/opt/iredmail/ssl -v /share_docker/iredmail/data/mysql:/var/lib/mysql -v /share_docker/iredmail/data/clamav:/var/lib/clamav -v /share_docker/iredmail/data/sa_rules:/var/lib/spamassassin -v /share_docker/iredmail/data/postfix_queue:/var/spool/postfix --log-driver syslog --log-opt tag=DOCKER_iredmail --log-opt syslog-address=udp://10.20.0.30:514 iredmail/mariadb:stable && docker start CTX_iredmail
