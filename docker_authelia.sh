#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/authelia_conf
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# expose secret
echo $AU_JWT_SECRET | docker secret create JWT_SECRET -
echo $AU_SESSION_SECRET | docker secret create SESSION_SECRET -
echo $AU_STORAGE_PASSWORD | docker secret create STORAGE_PASSWORD -
echo $AU_STORAGE_ENCRYPTION_KEY | docker secret create STORAGE_ENCRYPTION_KEY -

# get authelia image
docker pull docker.io/authelia/authelia:latest

# create authelia container
docker create --name CTX_authelia --restart unless-stopped -s JWT_SECRET -s SESSION_SECRET -s STORAGE_PASSWORD -s STORAGE_ENCRYPTION_KEY -e PUID=1000 -e PGID=1000 -e AUTHELIA_IDENTITY_VALIDATION_RESET_PASSWORD_JWT_SECRET_FILE='/run/secrets/JWT_SECRET' -e AUTHELIA_SESSION_SECRET_FILE='/run/secrets/SESSION_SECRET' -e AUTHELIA_STORAGE_POSTGRES_PASSWORD_FILE='/run/secrets/STORAGE_PASSWORD' -e AUTHELIA_STORAGE_ENCRYPTION_KEY_FILE='/run/secrets/STORAGE_ENCRYPTION_KEY' --network net -h authelia -p 9091:9091 -v /share_docker/authelia_conf:/config --log-driver syslog --log-opt tag=DOCKER_authelia --log-opt syslog-address=udp://10.20.0.30:514 docker.io/authelia/authelia:latest

# start authelia container
docker start CTX_authelia

# one shitty line
mkdir -p /share_docker/authelia_conf && chown 1000:1000 -R /share_docker/* && docker network create net && echo $AU_JWT_SECRET | docker secret create JWT_SECRET - && echo $AU_SESSION_SECRET | docker secret create SESSION_SECRET - && echo $AU_STORAGE_PASSWORD | docker secret create STORAGE_PASSWORD - && echo $AU_STORAGE_ENCRYPTION_KEY | docker secret create STORAGE_ENCRYPTION_KEY - && docker pull docker.io/authelia/authelia:latest && docker create --name CTX_authelia --restart unless-stopped -s JWT_SECRET -s SESSION_SECRET -s STORAGE_PASSWORD -s STORAGE_ENCRYPTION_KEY -e PUID=1000 -e PGID=1000 -e AUTHELIA_IDENTITY_VALIDATION_RESET_PASSWORD_JWT_SECRET_FILE='/run/secrets/JWT_SECRET' -e AUTHELIA_SESSION_SECRET_FILE='/run/secrets/SESSION_SECRET' -e AUTHELIA_STORAGE_POSTGRES_PASSWORD_FILE='/run/secrets/STORAGE_PASSWORD' -e AUTHELIA_STORAGE_ENCRYPTION_KEY_FILE='/run/secrets/STORAGE_ENCRYPTION_KEY' --network net -h authelia -p 9091:9091 -v /share_docker/authelia_conf:/config --log-driver syslog --log-opt tag=DOCKER_authelia --log-opt syslog-address=udp://10.20.0.30:514 docker.io/authelia/authelia:latest && docker start CTX_authelia
