#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/flatnotes
chown 1000:1000 -R /share_docker/flatnotes

# create tmp network
docker network create net

# get flatnotes image
docker pull dullage/flatnotes:latest

# create flatnotes container
docker create --name CTX_flatnotes --restart unless-stopped -e PUID=1000 -e PGID=1000  -e "FLATNOTES_AUTH_TYPE=password"  -e "FLATNOTES_USERNAME=$FN_user"  -e "FLATNOTES_PASSWORD=$FT_password" -e "FLATNOTES_SECRET_KEY=$FT_secret" --network net -p 8080:8080 -v /share_docker/data:/data --log-driver syslog --log-opt tag=DOCKER_flatnotes --log-opt syslog-address=udp://10.20.0.30:514 dullage/flatnotes:latest

# start flatnotes container
docker start CTX_flatnotes

# one shitty line
mkdir -p /share_docker/flatnotes && chown 1000:1000 -R /share_docker/flatnotes && docker network create net && docker pull dullage/flatnotes:latest && docker create --name CTX_flatnotes --restart unless-stopped -e PUID=1000 -e PGID=1000  -e "FLATNOTES_AUTH_TYPE=password"  -e "FLATNOTES_USERNAME=$FN_user"  -e "FLATNOTES_PASSWORD=$FT_password" -e "FLATNOTES_SECRET_KEY=$FT_secret" --network net -p 8080:8080 -v /share_docker/data:/data --log-driver syslog --log-opt tag=DOCKER_flatnotes --log-opt syslog-address=udp://10.20.0.30:514 dullage/flatnotes:latest && docker start CTX_flatnotes
