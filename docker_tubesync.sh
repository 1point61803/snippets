#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/tubesync_conf
chown 1000:1000 -R /share_docker/tubesync_conf

# create tmp network
docker network create net

# get tubesync image
docker pull  ghcr.io/meeb/tubesync:latest

# create tubesync container
docker create --name CTX_tubesync --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e HTTP_USER=$TS_user -e HTTP_PASS=$TS_password -v /share_docker/tubesync_conf:/config --mount type=bind,source=/mnt/misc/TEMP_YT,target=/downloads --network net -h mariadb -p 4848:4848 --log-driver syslog --log-opt tag=DOCKER_tubesync --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/meeb/tubesync:latest

# start container
docker start CTX_tubesync

# one shitty line
mkdir -p /share_docker/tubesync_conf && chown 1000:1000 -R /share_docker/tubesync_conf && docker network create net && docker pull  ghcr.io/meeb/tubesync:latest && docker create --name CTX_tubesync --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e HTTP_USER=$TS_user -e HTTP_PASS=$TS_password -v /share_docker/tubesync_conf:/config --mount type=bind,source=/mnt/misc/TEMP_YT,target=/downloads --network net -h mariadb -p 4848:4848 --log-driver syslog --log-opt tag=DOCKER_tubesync --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/meeb/tubesync:latest && docker start CTX_tubesync
