#!/bin/bash

# create share folder
mkdir -p /share_docker/rainloop_data
chown 1000:1000 -R /share_docker/rainloop_data

# create tmp network
docker network create net

# get rainloop image
docker pull martadinata666/rainloop

# create rainloop container
docker create --name CTX_rainloop --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rainloop_data:/var/www/localhost/htdocs/data --network net -p 8085:80 --log-driver syslog --log-opt tag=DOCKER_rainloop --log-opt syslog-address=udp://10.20.0.30:514 martadinata666/rainloop

# start container
docker start CTX_rainloop

# one shitty line
mkdir -p /share_docker/rainloop_data && chown 1000:1000 -R /share_docker/rainloop_data && docker network create net && docker pull martadinata666/rainloop && docker create --name CTX_rainloop --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/rainloop_data:/var/www/localhost/htdocs/data --network net -p 8085:80 --log-driver syslog --log-opt tag=DOCKER_rainloop --log-opt syslog-address=udp://10.20.0.30:514 martadinata666/rainloop && docker start CTX_rainloop

# admin url -> http://ip-address:8085/?admin / login User: admin  / Pass: 12345
# user url -> http://ip-address:8085/ / Username: youremail@domain.tld / Pass: yourpassword
