#!/bin/bash

# include secret
source SECRET

# create tmp network
docker network create net

# get nessus image
docker pull tenableofficial/nessus:latest

# create nessus container
docker create --name CTX_nessus --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ACTIVATION_CODE=$SS_activation -e USERNAME=$SS_user -e PASSWORD=$SS_password --network net -p 8834:8834 --log-driver syslog --log-opt tag=DOCKER_nessus --log-opt syslog-address=udp://10.20.0.30:514 tenableofficial/nessus:latest

# start container
docker start CTX_nessus

# one shitty line
docker network create net && docker pull tenableofficial/nessus:latest && docker create --name CTX_nessus --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ACTIVATION_CODE=$SS_activation -e USERNAME=$SS_user -e PASSWORD=$SS_password --network net -p 8834:8834 --log-driver syslog --log-opt tag=DOCKER_nessus --log-opt syslog-address=udp://10.20.0.30:514 tenableofficial/nessus:latest && docker start CTX_nessus
