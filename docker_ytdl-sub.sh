#!/bin/bash

# create share folder
mkdir -p /share_docker/ytdl-sub-config/ /share_docker/ytdl-sub-data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get jellyfin image
docker pull ghcr.io/jmbannon/ytdl-sub-gui:latest

# create jellyfin container
docker create --name CTX_ytdlsub --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h ytdlsub -p 8443:8443 -v /share_docker/ytdl-sub-config:/config -v /share_docker/ytdl-sub-data:/tv_shows  --log-driver syslog --log-opt tag=DOCKER_ytdlsub --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/jmbannon/ytdl-sub-gui:latest

# start jellyfin container
docker start CTX_ytdlsub

# one shitty line
mkdir -p /share_docker/ytdl-sub-config/ /share_docker/ytdl-sub-data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull ghcr.io/jmbannon/ytdl-sub-gui:latest && docker create --name CTX_ytdlsub --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h ytdlsub -p 8443:8443 -v /share_docker/ytdl-sub-config:/config -v /share_docker/ytdl-sub-data:/tv_shows  --log-driver syslog --log-opt tag=DOCKER_ytdlsub --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/jmbannon/ytdl-sub-gui:latest && docker start CTX_ytdlsub
