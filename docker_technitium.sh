#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/technitium_config
chown 1000:1000 -R /share_docker/technitium_config

# create tmp network
docker network create net

# get technitium image
docker pull technitium/dns-server:latest

# create technitium container
docker create --name CTX_technitium --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DNS_SERVER_DOMAIN=TECHNITIUM -e DNS_SERVER_FORWARDERS=10.20.0.10 -v /share_docker/technitium_config:/etc/dns/config --network net -p 53:53/tcp -p 53:53/udp -p 5380:5380/tcp --log-driver syslog --log-opt tag=DOCKER_technitium --log-opt syslog-address=udp://10.20.0.30:514 technitium/dns-server:latest

# start container
docker start CTX_technitium

# one shitty line
mkdir -p /share_docker/technitium_config && chown 1000:1000 -R /share_docker/technitium_config && docker network create net && docker pull technitium/dns-server:latest && docker create --name CTX_technitium --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DNS_SERVER_DOMAIN=TECHNITIUM -e DNS_SERVER_FORWARDERS=10.20.0.10 -v /share_docker/technitium_config:/etc/dns/config --network net -p 5380:5380/tcp -p 53:53/udp -p 53:53/tcp -p 853:853/tcp -p 443:443/tcp -p 80:80/tcp -p 8053:8053/tcp --log-driver syslog --log-opt tag=DOCKER_technitium --log-opt syslog-address=udp://10.20.0.30:514 technitium/dns-server:latest && docker start CTX_technitium
