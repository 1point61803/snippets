#!/bin/bash

# include secret
source SECRET

# create persistent folder
mkdir -p /share_docker/mongo_data /share_docker/elasticsearch_data /share_docker/graylog_data/config

# fix permission
chown -R 1000:1000 /share_docker/elasticsearch_data
chown -R 1100:1100 /share_docker/graylog_data

# setup graylog config file
wget -P /share_docker/graylog_data/config/ https://raw.githubusercontent.com/Graylog2/graylog-docker/4.3/config/graylog.conf
wget -P /share_docker/graylog_data/config/ https://raw.githubusercontent.com/Graylog2/graylog-docker/4.3/config/log4j2.xml
  
# create tmp network
docker network create net

# get mongo image
docker pull mongo:4.2

# create mongo container
docker create --name CTX_mongo -v /share_docker/mongo_data:/data/db --network net -h mongo --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_mongo --log-opt syslog-address=udp://10.20.0.30:514 mongo:4.2

# get elasticsearch image
docker pull docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.0

# create elastic search container
docker create --name CTX_elasticsearch -e "discovery.type=single-node" -e "ES_JAVA_OPTS=-Dlog4j2.formatMsgNoLookups=true -Xms512m -Xmx1024m" -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net -h elasticsearch --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_elasticsearch --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.0

# get graylog image
docker pull graylog/graylog:4.3

# create graylog container
docker create --name CTX_graylog --network net -h graylog -p 9000:9000 -p 12201:12201 -p 514:514 -p 514:514/udp -p 1514 -p 1514/udp -p 4514:4514 -p 4514:4514/udp -p 12201:12201/udp -e TZ=Europe/Rome -e GRAYLOG_ROOT_PASSWORD_SHA2=$GL_password_hash -e  GRAYLOG_HTTP_EXTERNAL_URI="http://127.0.0.1:9000/" -v /share_docker/graylog_data:/usr/share/graylog/data --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_graylog --log-opt syslog-address=udp://10.20.0.30:514 graylog/graylog:4.3


# start container
docker start mongo 
docker start elasticsearch 
docker start graylog

# one shitty line
mkdir -p /share_docker/mongo_data /share_docker/elasticsearch_data /share_docker/graylog_data/config && chown -R 1000:1000 /share_docker/elasticsearch_data && chown -R 1100:1100 /share_docker/graylog_data && wget -P /share_docker/graylog_data/config/ https://raw.githubusercontent.com/Graylog2/graylog-docker/4.3/config/graylog.conf && wget -P /share_docker/graylog_data/config/ https://raw.githubusercontent.com/Graylog2/graylog-docker/4.3/config/log4j2.xml && docker network create net && docker pull mongo:4.2 && docker create --name CTX_mongo -v /share_docker/mongo_data:/data/db --network net --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_mongo --log-opt syslog-address=udp://10.20.0.30:514 mongo:4.2 && docker pull docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.0 && docker create --name CTX_elasticsearch -e "discovery.type=single-node" -e "ES_JAVA_OPTS=-Dlog4j2.formatMsgNoLookups=true -Xms512m -Xmx1024m" -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_elasticsearch --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.0 && docker pull graylog/graylog:4.3 && docker create --name CTX_graylog --network net -p 9000:9000 -p 12201:12201 -p 514:514 -p 514:514/udp -p 1514 -p 1514/udp -p 4514:4514 -p 4514:4514/udp -p 12201:12201/udp -e GRAYLOG_ROOT_PASSWORD_SHA2=$GL_password_hash -e  GRAYLOG_HTTP_EXTERNAL_URI="http://127.0.0.1:9000/" -v /share_docker/graylog_data:/usr/share/graylog/data --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_graylog --log-opt syslog-address=udp://10.20.0.30:514 graylog/graylog:4.3 && docker start mongo && docker start elasticsearch && docker start graylog
