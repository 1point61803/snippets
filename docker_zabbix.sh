#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/mysqldb_data 
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get mariadb image
docker pull mysql:8.0

# create mariadb container
docker create --name CTX_mysqldb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -v /share_docker/mysqldb_data:/var/lib/mysql --network net -h $ZA_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_za_mysqldb --log-opt syslog-address=udp://10.20.0.30:514 mysql:8.0 --character-set-server=utf8 --collation-server=utf8_bin --default-authentication-plugin=mysql_native_password

# get java gateway image
docker pull zabbix/zabbix-java-gateway:alpine-5.4-latest

# create java gateway container
docker create --name CTX_javagateway --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h $ZA_javagateway_host --log-driver syslog --log-opt tag=DOCKER_za_javagateway --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-java-gateway:alpine-5.4-latest

# get zabbix server image
docker pull zabbix/zabbix-server-mysql:alpine-5.4-latest

# create zabbix server container
docker create --name CTX_server --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DB_SERVER_HOST=$ZA_db_host -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e  MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -e ZBX_JAVAGATEWAY=$ZA_javagateway_host --network net -h $ZA_server_host -p 10051:10051 --log-driver syslog --log-opt tag=DOCKER_za_server --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-server-mysql:alpine-5.4-latest

# get zabbix proxy image
docker pull zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

# create zabbix proxy container
docker create --name CTX_proxy --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ZBX_SERVER_HOST=$ZA_server_host -e DB_SERVER_HOST=$ZA_db_host -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e  MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -e ZBX_JAVAGATEWAY=$ZA_javagateway_host --network net -h $ZA_proxy_host -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_za_proxy --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest

# start zabbix container
docker start CTX_mysqldb
docker start CTX_javagateway
docker start CTX_server
docker start CTX_proxy

# one shitty line
mkdir -p /share_docker/mysqldb_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull mysql:8.0 && docker create --name CTX_mysqldb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -v /share_docker/mysqldb_data:/var/lib/mysql --network net -h $ZA_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_za_mysqldb --log-opt syslog-address=udp://10.20.0.30:514 mysql:8.0 --character-set-server=utf8 --collation-server=utf8_bin --default-authentication-plugin=mysql_native_password && docker pull zabbix/zabbix-java-gateway:alpine-5.4-latest && docker create --name CTX_javagateway --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h $ZA_javagateway_host --log-driver syslog --log-opt tag=DOCKER_za_javagateway --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-java-gateway:alpine-5.4-latest && docker pull zabbix/zabbix-server-mysql:alpine-5.4-latest && docker create --name CTX_server --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DB_SERVER_HOST=$ZA_db_host -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e  MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -e ZBX_JAVAGATEWAY=$ZA_javagateway_host --network net -h $ZA_server_host -p 10051:10051 --log-driver syslog --log-opt tag=DOCKER_za_server --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-server-mysql:alpine-5.4-latest && docker pull zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest && docker create --name CTX_proxy --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ZBX_SERVER_HOST=$ZA_server_host -e DB_SERVER_HOST=$ZA_db_host -e MYSQL_DATABASE=$ZA_db_name -e MYSQL_USER=$ZA_db_user -e  MYSQL_PASSWORD=$ZA_db_password -e MYSQL_ROOT_PASSWORD=$ZA_db_password -e ZBX_JAVAGATEWAY=$ZA_javagateway_host --network net -h $ZA_proxy_host -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_za_proxy --log-opt syslog-address=udp://10.20.0.30:514 zabbix/zabbix-web-nginx-mysql:alpine-5.4-latest && docker start CTX_mysqldb && docker start CTX_javagateway && docker start CTX_server && docker start CTX_proxy
