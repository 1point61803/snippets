#!/bin/bash

# create share folder
mkdir -p /share_docker/pf_config/
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get pinchplat image
docker pull keglin/pinchflat:latest

# create pinchplat container
docker create --name CTX_pinchflat --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --user=1000:1000 --network net -h pinchflat -p 8945:8945 -v /share_docker/pf_config:/config  --mount type=bind,source=/mnt/misc/TEMP_YT/,target=/downloads --log-driver syslog --log-opt tag=DOCKER_pinchflat --log-opt syslog-address=udp://10.20.0.30:514 keglin/pinchflat:latest

# start pichflat container
docker start CTX_pinchflat

# one shitty line
mkdir -p /share_docker/pf_config/ && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull keglin/pinchflat:latest && docker create --name CTX_pinchflat --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --user=1000:1000 --network net -h pinchflat -p 8945:8945 -v /share_docker/pf_config:/config --mount type=bind,source=/mnt/misc/TEMP_YT/,target=/downloads --log-driver syslog --log-opt tag=DOCKER_pinchflat --log-opt syslog-address=udp://10.20.0.30:514 keglin/pinchflat:latest && docker start CTX_pinchflat  
