#!/bin/bash

# create share folder
mkdir -p /share_docker/speedtesttracker_conf
chown 1000:1000 -R /share_docker/speedtesttracker_conf

# create tmp network
docker network create net

# get speedtest-tracker image
docker pull henrywhitaker3/speedtest-tracker

# create speedtest-tracker container
docker create --name CTX_speedtesttracker --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OOKLA_EULA_GDPR=true -v /share_docker/speedtesttracker_conf:/config --network net -p 8765:80 --log-driver syslog --log-opt tag=DOCKER_speedtesttracker --log-opt syslog-address=udp://10.20.0.30:514 henrywhitaker3/speedtest-tracker

# start container
docker start CTX_speedtesttracker
 
# one shitty line
mkdir -p /share_docker/speedtesttracker_conf && chown 1000:1000 -R /share_docker/speedtesttracker_conf && docker network create net && docker pull henrywhitaker3/speedtest-tracker && docker create --name CTX_speedtesttracker --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OOKLA_EULA_GDPR=true -v /share_docker/speedtesttracker_conf:/config --network net -p 8765:80 --log-driver syslog --log-opt tag=DOCKER_speedtesttracker --log-opt syslog-address=udp://10.20.0.30:514 henrywhitaker3/speedtest-tracker && docker start CTX_speedtesttracker
