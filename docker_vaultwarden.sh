#!/bin/bash

# create share folder
mkdir -p /share_docker/vaultwarden_data /share_docker/vaultwarden_SSl_keys/
chown 1000:1000 -R /share_docker/vaultwarden_data /share_docker/vaultwarden_SSl_keys/

# get vaultwarden server image
docker pull vaultwarden/server:latest

# create vaultwarden server container
docker create --name CTX_vaultwarden --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ROCKET_TLS='{certs="/ssl/vault.pem",key="/ssl/vault_key.pem"}' -v /share_docker/vaultwarden_SSl_keys/:/ssl/ -v /share_docker/vaultwarden_data/:/data/ -p 443:80  -p 3012:3012 --log-driver syslog --log-opt tag=DOCKER_vaulwarden_server --log-opt syslog-address=udp://10.20.0.30:514 vaultwarden/server:latest

# start container
docker start CTX_vaultwarden

# one shitty line
mkdir -p /share_docker/vaultwarden_data && chown 1000:1000 -R /share_docker/vaultwarden_data && docker pull vaultwarden/server:latest && docker create --name CTX_vaultwarden --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ROCKET_TLS='{certs="/ssl/vault.pem",key="/ssl/vault_key.pem"}' -v /share_docker/vaultwarden_SSl_keys/:/ssl/ -v /share_docker/vaultwarden_data/:/data/ -p 443:80  -p 3012:3012 --log-driver syslog --log-opt tag=DOCKER_vaulwarden_server --log-opt syslog-address=udp://10.20.0.30:514 vaultwarden/server:latest && docker start CTX_vaultwarden
