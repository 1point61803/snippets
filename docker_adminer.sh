#!/bin/bash

# include secret
source SECRET

# create share folder

# create tmp network
docker network create net

# get jellyfin image
docker pull adminer

# create jellyfin container
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_jellyfin --log-opt syslog-address=udp://10.20.0.30:514 adminer

# start jellyfin container
docker start CTX_adminer

# one shitty line
docker network create net && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_jellyfin --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker start CTX_adminer
