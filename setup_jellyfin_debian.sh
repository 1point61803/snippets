#!/bin/bash

# setup jellyfin on LXC debian 10
apt update && apt upgrade -y

apt install apt-transport-https gnupg -y
wget -O - https://repo.jellyfin.org/jellyfin_team.gpg.key | apt-key add -
echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release ) $( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release ) main" | tee /etc/apt/sources.list.d/jellyfin.list
apt update
apt install jellyfin

systemctl enable jellyfin.service

# in line cmd
apt update && apt upgrade -y && apt install apt-transport-https gnupg -y && wget -O - https://repo.jellyfin.org/jellyfin_team.gpg.key | apt-key add - && echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release ) $( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release ) main" | tee /etc/apt/sources.list.d/jellyfin.list && apt update && apt install jellyfin && systemctl enable jellyfin.service

