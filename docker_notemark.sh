#!/bin/bash

# include secret
source SECRET

# create persistent folder
mkdir -p /share_docker/postgres_data /share_docker/notemark_data
chown 1000:1000 -R /share_docker/*
  
# create tmp network
docker network create net

# get postgres image
docker pull postgres:15-alpine

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e POSTGRES_DB=$NM_db_name -e POSTGRES_USER=$NM_db_user -e POSTGRES_PASSWORD=$NM_db_password -v /share_docker/postgres_data:/var/lib/postgresql/data --network net -h db -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_NM_postgres --log-opt syslog-address=udp://10.20.0.30:514  postgres:15-alpine

# get note mark backend image
docker pull ghcr.io/enchant97/note-mark-backend

# create note mark backend container
docker create --name CTX_notemarckB --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e JWT_SECRET="zQktd8RjcmC5" -e CORS_ORIGINS="*" -e DB__TYPE=postgres -e DB__URI="host=db user=$NM_db_user password=$NM_db_password dbname=$NM_db_name port=5432 sslmode=disable TimeZone=Europe/Rome" -v /share_docker/notemark_data:/data --network net -p 8001:8000 --log-driver syslog --log-opt tag=DOCKER_NM_backend --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/enchant97/note-mark-backend

# get notemark frontend image
docker pull ghcr.io/enchant97/note-mark-frontend

# create notemark frontend container
docker create --name CTX_notemarckF --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_NM_frontend --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/enchant97/note-mark-frontend

# start container
docker start CTX_postgres 
docker start CTX_notemarckB
docker start CTX_notemarckF

# one shitty line
mkdir -p /share_docker/postgres_data /share_docker/notemark_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull postgres:15-alpine && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e POSTGRES_DB=$NM_db_name -e POSTGRES_USER=$NM_db_user -e POSTGRES_PASSWORD=$NM_db_password -v /share_docker/postgres_data:/var/lib/postgresql/data --network net -h db -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_NM_postgres --log-opt syslog-address=udp://10.20.0.30:514  postgres:15-alpine && docker pull ghcr.io/enchant97/note-mark-backend && docker create --name CTX_notemarckB --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e JWT_SECRET="zQktd8RjcmC5" -e CORS_ORIGINS="*" -e DB__TYPE=postgres -e DB__URI="host=db user=$NM_db_user password=$NM_db_password dbname=$NM_db_name port=5432 sslmode=disable TimeZone=Europe/Rome" -v /share_docker/notemark_data:/data --network net -p 8001:8000 --log-driver syslog --log-opt tag=DOCKER_NM_backend --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/enchant97/note-mark-backend && docker pull ghcr.io/enchant97/note-mark-frontend && docker create --name CTX_notemarckF --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_NM_frontend --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/enchant97/note-mark-frontend && docker start CTX_postgres && docker start CTX_CTX_notemarckB && docker start CTX_CTX_notemarckF
