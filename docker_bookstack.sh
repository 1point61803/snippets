#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/bookstack_conf /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$BS_db_user -e MARIADB_PASSWORD=$BS_db_password -e  MARIADB_ROOT_PASSWORD=$BS_db_password -e MARIADB_DATABASE=$BS_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h mariadb -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_ff_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h adminer -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_bs_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get bookstack image
docker pull lscr.io/linuxserver/bookstack

# create bookstack container
docker create --name CTX_bookstack --restart unless-stopped -e PUID=1000 -e PGID=1000  -e APP_URL=http://<IP>:6875 -e DB_HOST=mariadb -e DB_PORT=3306 -e DB_USER=$BS_db_user -e DB_PASS=$BS_db_password -e DB_DATABASE=$BS_db_name --network net -h bookstack -p 6875:80 -v /share_docker/bookstack_conf:/config --log-driver syslog --log-opt tag=DOCKER_bs_bookstack --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/bookstack

# start bookstack container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_bookstack

# one shitty line
mkdir -p /share_docker/bookstack_conf /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$BS_db_user -e MARIADB_PASSWORD=$BS_db_password -e  MARIADB_ROOT_PASSWORD=$BS_db_password -e MARIADB_DATABASE=$BS_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h mariadb -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_ff_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h adminer -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_bs_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull lscr.io/linuxserver/bookstack && docker create --name CTX_bookstack --restart unless-stopped -e PUID=1000 -e PGID=1000  -e APP_URL=http://<IP>:6875 -e DB_HOST=mariadb -e DB_PORT=3306 -e DB_USER=$BS_db_user -e DB_PASS=$BS_db_password -e DB_DATABASE=$BS_db_name --network net -h bookstack -p 6875:80 -v /share_docker/bookstack_conf:/config --log-driver syslog --log-opt tag=DOCKER_bs_bookstack --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/bookstack && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_bookstack
