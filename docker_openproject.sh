#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/openproject_pgdata /share_docker/openproject_assets
chown 1000:1000 -R /share_docker/openproject_pgdata /share_docker/openproject_assets

# create tmp network
docker network create net

# get openproject image
docker pull openproject/community:12

# create openproject container
docker create --name CTX_openproject --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OPENPROJECT_HOST__NAME=$OP_domain -e OPENPROJECT_SECRET_KEY_BASE=$OP_secret -v /share_docker/openproject_pgdata:/var/openproject/pgdata -v /share_docker/openproject_assets:/var/openproject/assets --network net -p 8080:80 --log-driver syslog --log-opt tag=DOCKER_openproject --log-opt syslog-address=udp://10.20.0.30:514 openproject/community:12

# start container
docker start CTX_openproject

# one shitty line
mkdir -p /share_docker/openproject_pgdata /share_docker/openproject_assets && chown 1000:1000 -R /share_docker/openproject_pgdata /share_docker/openproject_assets && docker network create net && docker pull openproject/community:12 && docker create --name CTX_openproject --restart unless-stopped -e PUID=1000 -e PGID=1000 -e OPENPROJECT_HOST__NAME=$OP_domain -e OPENPROJECT_SECRET_KEY_BASE=$OP_secret -v /share_docker/openproject_pgdata:/var/openproject/pgdata -v /share_docker/openproject_assets:/var/openproject/assets --network net -p 8080:80 --log-driver syslog --log-opt tag=DOCKER_openproject --log-opt syslog-address=udp://10.20.0.30:514 openproject/community:12 && docker start CTX_openproject
