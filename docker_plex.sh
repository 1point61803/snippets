#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/config /share_docker/data /share_docker/transcode
chown 1000:1000 -R /share_docker/config /share_docker/data /share_docker/transcode

# create tmp network
docker network create net

# get plex image
docker pull plexinc/pms-docker

# create plex container
docker create --name CTX_plex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ADVERTISE_IP=$PL_ip -v /share_docker/config:/config -v /share_docker/data:/data -v /share_docker/transcode:/transcode --network net -p 32400:32400/tcp -p 3005:3005/tcp -p 8324:8324/tcp -p 32469:32469/tcp -p 1900:1900/udp -p 32410:32410/udp -p 32412:32412/udp -p 32413:32413/udp -p 32414:32414/udp -e PLEX_CLAIM=$PL_claim_token  --log-driver syslog --log-opt tag=DOCKER_plex --log-opt syslog-address=udp://10.20.0.30:514 plexinc/pms-docker

# start container
docker start CTX_plex

# one shitty line
mkdir -p /share_docker/config /share_docker/data /share_docker/transcode && chown 1000:1000 -R /share_docker/config /share_docker/data /share_docker/transcode && docker network create net && docker pull plexinc/pms-docker && docker create --name CTX_plex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ADVERTISE_IP=$PL_ip -v /share_docker/config:/config -v /share_docker/data:/data -v /share_docker/transcode:/transcode --network net -p 32400:32400/tcp -p 3005:3005/tcp -p 8324:8324/tcp -p 32469:32469/tcp -p 1900:1900/udp -p 32410:32410/udp -p 32412:32412/udp -p 32413:32413/udp -p 32414:32414/udp -e PLEX_CLAIM=$PL_claim_token  --log-driver syslog --log-opt tag=DOCKER_plex --log-opt syslog-address=udp://10.20.0.30:514 plexinc/pms-docker && docker start CTX_plex
