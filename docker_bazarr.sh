#!/bin/bash

# create share folder
mkdir -p /share_docker/bazarr_config
chown 1000:1000 -R /share_docker/bazarr_config

# create tmp network
docker network create net

# get redis image
docker pull lscr.io/linuxserver/bazarr

# create redis container
docker create --name CTX_bazarr --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 6767:6767 -v /share_docker/firefly:/config --mount type=bind,source=/mnt/misc/TEMP_CINEMA,target=/temp_cinema --mount type=bind,source=/mnt/video,target=/video --mount type=bind,source=/mnt/animated,target=/animations --mount type=bind,source=/mnt/misc/TEMP_SERIE,target=/temp_serie --log-driver syslog --log-opt tag=DOCKER_bazarr --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/bazarr

# start container
docker start CTX_bazarr

# one shitty line
mkdir -p /share_docker/bazarr_config && chown 1000:1000 -R /share_docker/bazarr_config && docker network create net && docker pull lscr.io/linuxserver/bazarr && docker create --name CTX_bazarr --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 6767:6767 -v /share_docker/firefly:/config --mount type=bind,source=/mnt/misc/TEMP_CINEMA,target=/temp_cinema --mount type=bind,source=/mnt/video,target=/video --mount type=bind,source=/mnt/animated,target=/animations --mount type=bind,source=/mnt/misc/TEMP_SERIE,target=/temp_serie --log-driver syslog --log-opt tag=DOCKER_bazarr --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/bazarr && docker start CTX_bazarr
