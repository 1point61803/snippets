#!/bin/bash

# create persistent folder
mkdir -p /share_docker/elasticsearch_data /share_docker/cortex_job
chown 1000:1000 -R /share_docker/*
  
# create tmp network
docker network create net

# get elasticsearch image
docker pull elasticsearch:7.9.1

# create elasticsearch container
docker create --name CTX_elasticsearch --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e http.host=0.0.0.0 -e discovery.type=single-node -e script.allowed_types=inline -e thread_pool.search.queue_size=100000 -e thread_pool.write.queue_size=10000 -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net -h elasticsearch --log-driver syslog --log-opt tag=DOCKER_C_elasticsearch --log-opt syslog-address=udp://10.20.0.30:514 elasticsearch:7.9.1

# get cortex image
docker pull nuclias/nuclias_connect_core:1.2.0

# create cortex container
docker create --name CTX_cortex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e - job_directory=/share_docker/cortex_job -v /var/run/docker.sock:/var/run/docker.sock -v /share_docker/cortex_job:/share_docker/cortex_job --network net -h cortex -p 9001:9001 --log-driver syslog --log-opt tag=DOCKER_C_cortex --log-opt syslog-address=udp://10.20.0.30:514 thehiveproject/cortex:3.1.1

# start container
docker start CTX_elasticsearch
docker start CTX_cortex

# one shitty line
mkdir -p /share_docker/elasticsearch_data /share_docker/cortex_job && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull elasticsearch:7.9.1 && docker create --name CTX_elasticsearch --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e http.host=0.0.0.0 -e discovery.type=single-node -e script.allowed_types=inline -e thread_pool.search.queue_size=100000 -e thread_pool.write.queue_size=10000 -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net -h elasticsearch --log-driver syslog --log-opt tag=DOCKER_C_elasticsearch --log-opt syslog-address=udp://10.20.0.30:514 elasticsearch:7.9.1 && docker pull nuclias/nuclias_connect_core:1.2.0 && docker create --name CTX_cortex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e - job_directory=/share_docker/cortex_job -v /var/run/docker.sock:/var/run/docker.sock -v /share_docker/cortex_job:/share_docker/cortex_job --network net -h cortex -p 9001:9001 --log-driver syslog --log-opt tag=DOCKER_C_cortex --log-opt syslog-address=udp://10.20.0.30:514 thehiveproject/cortex:3.1.1 && docker start CTX_elasticsearch && docker start CTX_cortex
