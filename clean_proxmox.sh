#!/bin/bash
echo "###_ CLEAN TEMPORARY FILES _###"
date +"%m-%d-%y"
echo "###_ STORAGE 4TB _###"
$temp1 ls -1 /storage/Misc/.recycle | wc -l
echo "files -> " $temp1
rm -rf /storage/Misc/.recycle/*
echo "###_ DATA 1TB _###"
$temp2 ls -1 /data/Backup/.recycle | wc -l
echo "files -> " $temp2
rm -rf /data/Backup/.recycle/*
echo "###_ CLEAN DONE _###"

