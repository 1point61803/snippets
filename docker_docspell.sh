#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/postgres_data /share_docker/solr_data /share_docker/docs_imp /share_docker/env
chown 1000:1000 -R /share_docker/postgres_data /share_docker/solr_data /share_docker/docs_imp /share_docker/env

# create file env
echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nDOCSPELL_SERVER_INTERNAL__URL=http://docspell-restserver:7880\nDOCSPELL_SERVER_ADMIN__ENDPOINT_SECRET=$DS_admin_password\nDOCSPELL_SERVER_AUTH_SERVER__SECRET=$DS_secret\nDOCSPELL_SERVER_BACKEND_JDBC_PASSWORD=$Ds_db_password\nDOCSPELL_SERVER_BACKEND_JDBC_URL=jdbc:postgresql://db:5432/$DS_db_name\nDOCSPELL_SERVER_BACKEND_JDBC_USER=$DS_db_user\nDOCSPELL_SERVER_BIND_ADDRESS=0.0.0.0\nDOCSPELL_SERVER_FULL__TEXT__SEARCH_ENABLED=true\nDOCSPELL_SERVER_FULL__TEXT__SEARCH_SOLR_URL=http://solr:8983/solr/docspell\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_ENABLED=true\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_HTTP__HEADER_ENABLED=true\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_HTTP__HEADER_HEADER__VALUE=integration-password123\nDOCSPELL_SERVER_BACKEND_SIGNUP_MODE=open\nDOCSPELL_SERVER_BACKEND_SIGNUP_NEW__INVITE__PASSWORD=$DS_invite_password\nDOCSPELL_SERVER_BACKEND_ADDONS_ENABLED=false" > /share_docker/env/restserver.env
echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nDOCSPELL_JOEX_APP__ID=joex1\nDOCSPELL_JOEX_PERIODIC__SCHEDULER_NAME=joex1\nDOCSPELL_JOEX_SCHEDULER_NAME=joex1\nDOCSPELL_JOEX_BASE__URL=http://joex:7878\nDOCSPELL_JOEX_BIND_ADDRESS=0.0.0.0\nDOCSPELL_JOEX_FULL__TEXT__SEARCH_ENABLED=true\nDOCSPELL_JOEX_FULL__TEXT__SEARCH_SOLR_URL=http://solr:8983/solr/docspell\nDOCSPELL_JOEX_JDBC_PASSWORD=$DS_db_password\nDOCSPELL_JOEX_JDBC_URL=jdbc:postgresql://postgresql:5432/$DS_db_name\nDOCSPELL_JOEX_JDBC_USER=$DS_db_user\nDOCSPELL_JOEX_ADDONS_EXECUTOR__CONFIG_RUNNER=docker,trivial\nDOCSPELL_JOEX_CONVERT_HTML__CONVERTER=weasyprint" > /share_docker/env/joex.env

# create tmp network
docker network create net --subnet=192.168.0.0/24

# get postgres image
docker pull postgres:15.2

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$DS_db_user -e POSTGRES_PASSWORD=$DS_db_password 
-e POSTGRES_DB=$DS_db_name -v /share_docker/postgres_data:/var/lib/postgresql/data/ --network net -p 5432:5432 -h postgresql --ip $DS_ip_db --log-driver syslog --log
-opt tag=DOCKER_ds_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:15.2

# get solr image
docker pull solr:9

# create solr cotainer
docker create --name CTX_solr --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h solr --ip $DS_ip_solr -v /share_docker/solr_data:/var/solr --log-driver syslog --log-opt tag=DOCKER_ds_solr --log-opt syslog-address=udp://10.20.0.30:514 solr:9 bash -c 'precreate-core docspell; exec solr -f -Dsolr.modules=analysis-extras'

# get restserver image
docker pull docspell/restserver:latest

# create restserver container
docker create --name CTX_restserver --restart unless-stopped --env-file /share_docker/env/restserver.env --network net -h restserver -p 7880:7880 --ip $DS_ip_restserver --log-driver syslog --log-opt tag=DOCKER_ds_restserver --log-opt syslog-address=udp://10.20.0.30:514 docspell/restserver:latest

# get joex image
docker pull docspell/joex:latest

# create joex container
docker create --name CTX_joex --restart unless-stopped --env-file /share_docker/env/joex.env --network net -h joex -p 7881:7880 --ip $DS_ip_joex --log-driver syslog --log-opt tag=DOCKER_ds_joex --log-opt syslog-address=udp://10.20.0.30:514 docspell/joex:latest -J-Xmx3G

# get dsc image
docker pull docspell/dsc:latest

# create dsc container
docker create --name CTX_dsc --restart unless-stopped --network net -h dsc --ip $DS_ip_dsc -v /share_docker/docs_imp:/opt/docs --log-driver syslog --log-opt tag=DOCKER_ds_dsc --log-opt syslog-address=udp://10.20.0.30:514 docspell/dsc:latest  bash -c 'dsc -d http://docspell-restserver:7880 watch --delete -ir /opt/docs'

# start container
docker start CTX_postgres
docker start CTX_solr
docker start CTX_restserver
docker start CTX_joex
docker start CTX_dsc

# one shitty line
mkdir -p /share_docker/postgres_data /share_docker/solr_data /share_docker/docs_imp /share_docker/env && chown 1000:1000 -R /share_docker/postgres_data /share_docker/solr_data /share_docker/docs_imp /share_docker/env && echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nDOCSPELL_SERVER_INTERNAL__URL=http://docspell-restserver:7880\nDOCSPELL_SERVER_ADMIN__ENDPOINT_SECRET=$DS_admin_password\nDOCSPELL_SERVER_AUTH_SERVER__SECRET=$DS_secret\nDOCSPELL_SERVER_BACKEND_JDBC_PASSWORD=$Ds_db_password\nDOCSPELL_SERVER_BACKEND_JDBC_URL=jdbc:postgresql://db:5432/$DS_db_name\nDOCSPELL_SERVER_BACKEND_JDBC_USER=$DS_db_user\nDOCSPELL_SERVER_BIND_ADDRESS=0.0.0.0\nDOCSPELL_SERVER_FULL__TEXT__SEARCH_ENABLED=true\nDOCSPELL_SERVER_FULL__TEXT__SEARCH_SOLR_URL=http://solr:8983/solr/docspell\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_ENABLED=true\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_HTTP__HEADER_ENABLED=true\nDOCSPELL_SERVER_INTEGRATION__ENDPOINT_HTTP__HEADER_HEADER__VALUE=integration-password123\nDOCSPELL_SERVER_BACKEND_SIGNUP_MODE=open\nDOCSPELL_SERVER_BACKEND_SIGNUP_NEW__INVITE__PASSWORD=$DS_invite_password\nDOCSPELL_SERVER_BACKEND_ADDONS_ENABLED=false" > /share_docker/env/restserver.env && echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nDOCSPELL_JOEX_APP__ID=joex1\nDOCSPELL_JOEX_PERIODIC__SCHEDULER_NAME=joex1\nDOCSPELL_JOEX_SCHEDULER_NAME=joex1\nDOCSPELL_JOEX_BASE__URL=http://joex:7878\nDOCSPELL_JOEX_BIND_ADDRESS=0.0.0.0\nDOCSPELL_JOEX_FULL__TEXT__SEARCH_ENABLED=true\nDOCSPELL_JOEX_FULL__TEXT__SEARCH_SOLR_URL=http://solr:8983/solr/docspell\nDOCSPELL_JOEX_JDBC_PASSWORD=$DS_db_password\nDOCSPELL_JOEX_JDBC_URL=jdbc:postgresql://postgresql:5432/$DS_db_name\nDOCSPELL_JOEX_JDBC_USER=$DS_db_user\nDOCSPELL_JOEX_ADDONS_EXECUTOR__CONFIG_RUNNER=docker,trivial\nDOCSPELL_JOEX_CONVERT_HTML__CONVERTER=weasyprint" > /share_docker/env/joex.env && docker network create net --subnet=192.168.0.0/24 && docker pull postgres:15.2 && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$DS_db_user -e POSTGRES_PASSWORD=$DS_db_password -e POSTGRES_DB=$DS_db_name -v /share_docker/postgres_data:/var/lib/postgresql/data/ --network net -p 5432:5432 -h postgresql --ip $DS_ip_db --log-driver syslog --log-opt tag=DOCKER_ds_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:15.2 && docker pull solr:9 && docker create --name CTX_solr --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h solr --ip $DS_ip_solr -v /share_docker/solr_data:/var/solr --log-driver syslog --log-opt tag=DOCKER_ds_solr --log-opt syslog-address=udp://10.20.0.30:514 solr:9 bash -c 'precreate-core docspell; exec solr -f -Dsolr.modules=analysis-extras' && docker pull docspell/restserver:latest && docker create --name CTX_restserver --restart unless-stopped --env-file /share_docker/env/restserver.env --network net -h restserver -p 7880:7880 --ip $DS_ip_restserver --log-driver syslog --log-opt tag=DOCKER_ds_restserver --log-opt syslog-address=udp://10.20.0.30:514 docspell/restserver:latest && docker pull docspell/joex:latest && docker create --name CTX_joex --restart unless-stopped --env-file /share_docker/env/joex.env --network net -h joex -p 7881:7880 --ip $DS_ip_joex --log-driver syslog --log-opt tag=DOCKER_ds_joex --log-opt syslog-address=udp://10.20.0.30:514 docspell/joex:latest -J-Xmx3G && docker pull docspell/dsc:latest && docker create --name CTX_dsc --restart unless-stopped --network net -h dsc --ip $DS_ip_dsc -v /share_docker/docs_imp:/opt/docs --log-driver syslog --log-opt tag=DOCKER_ds_dsc --log-opt syslog-address=udp://10.20.0.30:514 docspell/dsc:latest  bash -c 'dsc -d http://docspell-restserver:7880 watch --delete -ir --not-matches **/.* --header Docspell-Integration:integration-password123 /opt/docs' && docker start CTX_postgres && docker start CTX_solr && docker start CTX_restserver && docker start CTX_joex && docker start CTX_dsc
