#!/bin/bash

# create persistent folder
mkdir -p /share_docker/kavita_data
chown 1000:1000 -R /share_docker/*

# get kavita image
docker pull  jvmilazz0/kavita:latest

# create kavita container
docker create --name CTX_kavita --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --pid=host --network=host -h kavita -p 5000:5000 --mount type=bind,source=/mnt/books,target=/manga  -v  /share_docker/kavita_data:/kavita/config --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_kavita --log-opt syslog-address=udp://10.20.0.30:514   jvmilazz0/kavita:latest

# start container
docker start CTX_kavita

# one shitty line
mkdir -p /share_docker/kavita_data && chown 1000:1000 -R /share_docker/* && docker pull  jvmilazz0/kavita:latest && docker create --name CTX_kavita --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --pid=host --network=host -h kavita -p 5000:5000 --mount type=bind,source=/mnt/books,target=/manga  -v  /share_docker/kavita_data:/kavita/config --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_kavita --log-opt syslog-address=udp://10.20.0.30:514   jvmilazz0/kavita:latest && docker start CTX_kavita
