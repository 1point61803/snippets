#!/bin/bash

# create share folder
mkdir -p /share_docker/stirling_config/ /share_docker/stirling_data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get jellyfin image
docker pull frooodle/s-pdf:latest

# create jellyfin container
docker create --name CTX_stirling --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h stirling -p 8080:8080 -v /share_docker/strirling_config:/configs -v /share_docker/stirling_data:/usr/share/tesseract-ocr/4.00/tessdata  --log-driver syslog --log-opt tag=DOCKER_stirling --log-opt syslog-address=udp://10.20.0.30:514 frooodle/s-pdf:latest

# start jellyfin container
docker start CTX_stirling

# one shitty line
mkdir -p /share_docker/stirling_config/ /share_docker/stirling_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull frooodle/s-pdf:latest && docker create --name CTX_stirling --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h stirling -p 8080:8080 -v /share_docker/strirling_config:/configs -v /share_docker/stirling_data:/usr/share/tesseract-ocr/4.00/tessdata  --log-driver syslog --log-opt tag=DOCKER_stirling --log-opt syslog-address=udp://10.20.0.30:514 frooodle/s-pdf:latest && docker start CTX_stirling
