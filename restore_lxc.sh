#!/bin/sh
# test lzo backup/snapshot LXC
lzop -t /data/VMsContainerData/dump/vzdump-lxc-107-XYZ.tar.lzo

# decompress lzo backup/snapshot LXC
lzop -d /data/VMsContainerData/dump/vzdump-lxc-107-XYZ.tar.lzo

# resotre tar backup/snapshot LXC
pct restore 107 vzdump-lxc-107-XYZ.tar --storage local-lvm

# restore tar backup/snapshot LXc with unpriviligied option
pct restore 1002 vzdump-lxc-1001-2016_12_17-tar.lzo -ignore-unpack-errors 1 --storage local-lvm -unprivileged
