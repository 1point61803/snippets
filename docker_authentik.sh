#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/postgres-data /share_docker/redis_data /share_docker/authentik_media /share_docker/authentik_template /share_docker/postgres-data /share_docker/authentik_certs /share_docker/authentik_conf
chown 1000:1000 -R /share_docker/*

# prepare configuration file for DB and the two authetik image
apk add pwgen && echo -e "AUTHENTIK_SECRET_KEY=$(pwgen -s 50 1)\nAUTHENTIK_ERROR_REPORTING__ENABLED=true\nPOSTGRES_PASSWORD=$AU_db_password\nPOSTGRES_USER=$AU_db_user\nPOSTGRES_DB=$AU_db_name\nAUTHENTIK_REDIS__HOST= $AU_redis_host\nAUTHENTIK_POSTGRESQL__HOST=$AU_db_host\nAUTHENTIK_POSTGRESQL__USER=$AU_db_user\nAUTHENTIK_POSTGRESQL__NAME=$AU_db_name\nAUTHENTIK_POSTGRESQL__PASSWORD=$AU_db_password" > /share_docker/authentik_conf/conf.env 

# create tmp network
docker network create net

# get postgres image
docker pull docker.io/library/postgres:16-alpine

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 --env-file /share_docker/authentik_conf/conf.env --network net -h $AU_db_host -v /share_docker/postgres-data:/var/lib/postgresql/data --log-driver syslog --log-opt tag=DOCKER_au_postgres --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/postgres:16-alpine

# get redis image
docker pull docker.io/library/redis:alpine

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h $AU_redis_host -v /share_docker/redis_data:/data --log-driver syslog --log-opt tag=DOCKER_au_redis --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/redis:alpine --save 60 1 --loglevel warning

# get authentik server image
docker pull ghcr.io/goauthentik/server:latest

# create authetik server container
docker create --name CTX_server --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AUTHENTIK_REDIS__HOST=$AU_redis_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host --env-file /share_docker/authentik_conf/conf.env --network net -h $AU_server_host -p 9000:9000 -p 9443:9443 -v /share_docker/authentik_media:/media -v /share_docker/authentik_template:/templates --log-driver syslog --log-opt tag=DOCKER_au_server --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/goauthentik/server:latest server

# get authentik worker image
docker pull ghcr.io/goauthentik/server:latest

# create authetik worker container
docker create --name CTX_worker --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AUTHENTIK_REDIS__HOST=$AU_redis_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host --env-file /share_docker/authentik_conf/conf.env --network net -h worker -v /var/run/docker.sock:/var/run/docker.sock -v /share_docker/authentik_media:/media -v /share_docker/authentik_certs:/certs -v /share_docker/authentik_template:/templates --log-driver syslog --log-opt tag=DOCKER_au_server --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/goauthentik/server:latest worker

# start all the containers
docker start CTX_postgres
docker start CTX_redis
docker start CTX_server
docker start CTX_worker

# one shitty line
mkdir -p /share_docker/postgres-data /share_docker/redis_data /share_docker/authentik_media /share_docker/authentik_template /share_docker/postgres-data /share_docker/authentik_certs /share_docker/authentik_conf && chown 1000:1000 -R /share_docker/* && apk add pwgen && echo -e "AUTHENTIK_SECRET_KEY=$(pwgen -s 50 1)\nAUTHENTIK_ERROR_REPORTING__ENABLED=true\nPOSTGRES_PASSWORD=$AU_db_password\nPOSTGRES_USER=$AU_db_user\nPOSTGRES_DB=$AU_db_name" > /share_docker/authentik_conf/conf.env && docker network create net && docker pull docker.io/library/postgres:12-alpine && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 --env-file /share_docker/authentik_conf/conf.env --network net -h $AU_db_host -v /share_docker/postgres-data:/var/lib/postgresql/data --log-driver syslog --log-opt tag=DOCKER_au_postgres --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/postgres:12-alpine && docker pull docker.io/library/redis:alpine && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h $AU_redis_host -v /share_docker/redis_data:/data --log-driver syslog --log-opt tag=DOCKER_au_redis --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/redis:alpine --save 60 1 --loglevel warning && docker pull ghcr.io/goauthentik/server:latest && docker create --name CTX_server --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AUTHENTIK_REDIS__HOST=$AU_redis_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host --env-file /share_docker/authentik_conf/conf.env --network net -h $AU_server_host -p 9000:9000 -p 9443:9443 -v /share_docker/authentik_media:/media -v /share_docker/authentik_template:/templates --log-driver syslog --log-opt tag=DOCKER_au_server --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/goauthentik/server:latest server && docker pull ghcr.io/goauthentik/server:latest && docker create --name CTX_worker --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AUTHENTIK_REDIS__HOST=$AU_redis_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host -e AUTHENTIK_POSTGRESQL__HOST=$AU_db_host --env-file /share_docker/authentik_conf/conf.env --network net -h worker -v /var/run/docker.sock:/var/run/docker.sock -v /share_docker/authentik_media:/media -v /share_docker/authentik_certs:/certs -v /share_docker/authentik_template:/templates --log-driver syslog --log-opt tag=DOCKER_au_server --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/goauthentik/server:latest worker && docker start CTX_postgres && docker start CTX_redis && docker start CTX_server && docker start CTX_worker
