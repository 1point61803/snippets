#!/bin/bash

# create share folder (mount -t nfs <NAS IP>:/mnt/book /mnt/books)
mkdir -p /share_docker/calibre_config/ /share_docker/calibre_data /mnt/books
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get jellyfin image
docker pull lscr.io/linuxserver/calibre-web:latest

# create jellyfin container
docker create --name CTX_calibre --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e DOCKER_MODS=linuxserver/mods:universal-calibre -e OAUTHLIB_RELAX_TOKEN_SCOPE=1 --network net -h calibre-web -p 8083:8083 -v /share_docker/calibre_config:/config --mount type=bind,source=/mnt/books,target=/books  --log-driver syslog --log-opt tag=DOCKER_calibre --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/calibre-web:latest

# start jellyfin container
docker start CTX_calibre

# one shitty line
mkdir -p /share_docker/calibre_config/ /share_docker/calibre_data /mnt/books && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull lscr.io/linuxserver/calibre-web:latest && docker create --name CTX_calibre --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e DOCKER_MODS=linuxserver/mods:universal-calibre -e OAUTHLIB_RELAX_TOKEN_SCOPE=1 --network net -h calibre-web -p 8083:8083 -v /share_docker/calibre_config:/config --mount type=bind,source=/mnt/books,target=/books  --log-driver syslog --log-opt tag=DOCKER_calibre --log-opt syslog-address=udp://10.20.0.30:514 lscr.io/linuxserver/calibre-web:latest && docker start CTX_calibre
