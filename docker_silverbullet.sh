#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/silverbullet_data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get silverbullet image
docker pull zefhemel/silverbullet

# create silverbullet container
docker create --name CTX_silverbullet --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SB_USER=$SB_user:$SB_password --network net -p 3000:3000 -v /share_docker/silverbullet_data:/space --log-driver syslog --log-opt tag=DOCKER_sb_silverbullet --log-opt syslog-address=udp://10.20.0.30:514 zefhemel/silverbullet

# start silverbullet container
docker start CTX_silverbullet

# one shitty line
mkdir -p /share_docker/silverbullet_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull zefhemel/silverbullet && docker create --name CTX_silverbullet --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SB_USER=$SB_user:$SB_password --network net -p 3000:3000 -v /share_docker/silverbullet_data:/space --log-driver syslog --log-opt tag=DOCKER_sb_silverbullet --log-opt syslog-address=udp://10.20.0.30:514 zefhemel/silverbullet && docker start CTX_silverbullet
