#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/watchyourlan
chown 1000:1000 -R /share_docker/watchyourlan

# create tmp network
docker network create net

# get mariadb image
docker pull aceberg/watchyourlan

# create mariadb container
docker create --name CTX_watchyourlan --restart unless-stopped -e PUID=1000 -e PGID=1000 -e IFACE=ens18 -e DBPATH=/data/db.sqlite -e GUIIP=127.0.0.1 -e GUIPORT=8840 -v /share_docker/watchyourlan:/data --network host --log-driver syslog --log-opt tag=DOCKER_watchyourlan --log-opt syslog-address=udp://10.20.0.30:514 aceberg/watchyourlan

# start container
docker start CTX_watchyourlan
 
# one shitty line
mkdir -p /share_docker/watchyourlan && chown 1000:1000 -R /share_docker/watchyourlan && docker network create net && docker pull aceberg/watchyourlan && docker create --name CTX_watchyourlan --restart unless-stopped -e PUID=1000 -e PGID=1000 -e IFACE=ens18 -e DBPATH=/data/db.sqlite -e GUIIP=127.0.0.1 -e GUIPORT=8840 -v /share_docker/watchyourlan:/data --network host --log-driver syslog --log-opt tag=DOCKER_watchyourlan --log-opt syslog-address=udp://10.20.0.30:514 aceberg/watchyourlan && docker start CTX_watchyourlan
