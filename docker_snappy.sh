#!/bin/bash


# create share folder
mkdir -p /share_docker/s_data
chown 1000:1000 -R /share_docker/s_data

# create tmp network
docker network create net

# get mldonkey image
docker pull kouinkouin/snappymail:latest

# create mldonkey container
docker create --name CTX_snappymail --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /share_docker/s_data:/snappymail/data:rw --network net -p 8888:8888 --log-driver syslog --log-opt tag=DOCKER_snappymail --log-opt syslog-address=udp://10.20.0.30:514 kouinkouin/snappymail:latest

# start container
docker start CTX_snappymail

# one shitty line
mkdir -p /share_docker/s_data && chown 1000:1000 -R /share_docker/s_data && docker network create net && docker pull kouinkouin/snappymail:latest && docker create --name CTX_snappymail --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /share_docker/s_data:/snappymail/data:rw --network net -p 8888:8888 --log-driver syslog --log-opt tag=DOCKER_snappymail --log-opt syslog-address=udp://10.20.0.30:514 kouinkouin/snappymail:latest && docker start CTX_snappymail
