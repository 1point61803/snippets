#!/bin/bash

# create share folder
mkdir -p /share_docker/app_data
chown 1000:1000 -R /share_docker/app_data

# create tmp network
docker network create net

# get ytmaterial image
docker pull tzahi12345/youtubedl-material:latest

# create ytmaterial container
docker create --name CTX_ytmaterial --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 8998:17442 -v /share_docker/app_data:/app/appdata -v /mnt/misc/TEMP_YT/:/app/audio -v /mnt/misc/TEMP_YT/:/app/video -v /mnt/misc/TEMP_YT/:/app/subscriptions -v /mnt/misc/TEMP_YT/:/app/users --log-driver syslog --log-opt tag=DOCKER_ytmaterial --log-opt syslog-address=udp://10.20.0.30:514 tzahi12345/youtubedl-material:latest

# start container
docker start CTX_ytmaterial

# one shitty line
mkdir -p /share_docker/app_data && chown 1000:1000 -R /share_docker/app_data && docker network create net && docker pull tzahi12345/youtubedl-material:latest && docker create --name CTX_ytmaterial --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --network net -p 8998:17442 -v /share_docker/app_data:/app/appdata -v /mnt/misc/TEMP_YT/:/app/audio -v /mnt/misc/TEMP_YT/:/app/video -v /mnt/misc/TEMP_YT/:/app/subscriptions -v /mnt/misc/TEMP_YT/:/app/users --log-driver syslog --log-opt tag=DOCKER_ytmaterial --log-opt syslog-address=udp://10.20.0.30:514 tzahi12345/youtubedl-material:latest && docker start CTX_ytmaterial
