#!/bin/bash

# create share folder
mkdir -p /share_docker/searxng_conf
chown 1000:1000 -R /share_docker/searxng_conf

# get searxng server image
docker pull searxng/searxng

# create searxng server container
docker create --name CTX_searxng --restart unless-stopped -e PUID=1000 -e PGID=1000 -e "BASE_URL=http://searxng.service.local:8080/" -e "INSTANCE_NAME=searxNG" -v /share_docker/searxng_conf/:/etc/searxng/ -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_searxng --log-opt syslog-address=udp://10.20.0.30:514 searxng/searxng

# start container
docker start CTX_searxng

# one shitty line
mkdir -p /share_docker/searxng_conf && chown 1000:1000 -R /share_docker/searxng_conf && docker pull searxng/searxng && docker create --name CTX_searxng --restart unless-stopped -e PUID=1000 -e PGID=1000 -e "BASE_URL=http://searxng.service.local:8080/" -e "INSTANCE_NAME=searxNG" -v /share_docker/searxng_conf/:/etc/searxng/ -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_searxng --log-opt syslog-address=udp://10.20.0.30:514 searxng/searxng && docker start CTX_searxng
