#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/jelly-config/ /share_docker/jelly-cache
chown 1000:1000 -R /share_docker/jelly-config/ /share_docker/jelly-cache
# $JL_mount_media contains ALL media directory bind mount

# create tmp network
docker network create net

# get jellyfin image
docker pull jellyfin/jellyfin

# create jellyfin container
docker create --name CTX_jellyfin --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8096:8096 -v /share_docker/jelly-config:/config -v /share_docker/jelly-cache:/cache $JL_mount_media --log-driver syslog --log-opt tag=DOCKER_jellyfin --log-opt syslog-address=udp://10.20.0.30:514 jellyfin/jellyfin

# start jellyfin container
docker start CTX_jellyfin

# one shitty line
mkdir -p /share_docker/jelly-config/ /share_docker/jelly-cache && chown 1000:1000 -R /share_docker/jelly-config/ /share_docker/jelly-cache && docker network create net && docker pull jellyfin/jellyfin && docker create --name CTX_jellyfin --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8096:8096 -v /share_docker/jelly-config:/config -v /share_docker/jelly-cache:/cache $JL_mount_media --log-driver syslog --log-opt tag=DOCKER_jellyfin --log-opt syslog-address=udp://10.20.0.30:514 jellyfin/jellyfin && docker start CTX_jellyfin
