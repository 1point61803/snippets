#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/agendav
chown 1000:1000 -R /share_docker/agendav

# create tmp network
docker network create net

# get agendav image
docker pull ghcr.io/nagimov/agendav-docker:latest

# create agendav container
docker create --name CTX_agendav --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AGENDAV_SERVER_NAME=<BAIKAL_IP> -e AGENDAV_TITLE="WebCal"  -e AGENDAV_FOOTER="WebCal" -e AGENDAV_ENC_KEY=my_encrypt10n_k3y -e AGENDAV_CALDAV_SERVER=http://<BAIKAL_IP>/dav.php -e AGENDAV_CALDAV_PUBLIC_URL=http://<BAIKAL_IP>/dav.php -e AGENDAV_TIMEZONE=UTC -e  AGENDAV_LANG=en -e AGENDAV_LOG_DIR=/tmp --network net -p 8080:8080 -v /share_docker/agendav:/tmp --log-driver syslog --log-opt tag=DOCKER_agendav --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/nagimov/agendav-docker:latest

# start agendav container
docker start CTX_agendav

# one shitty line
mkdir -p /share_docker/agendav && chown 1000:1000 -R /share_docker/agendav && docker network create net && docker pull ghcr.io/nagimov/agendav-docker:latest && docker create --name CTX_agendav --restart unless-stopped -e PUID=1000 -e PGID=1000 -e AGENDAV_SERVER_NAME=<BAIKAL_IP> -e AGENDAV_TITLE="WebCal"  -e AGENDAV_FOOTER="WebCal" -e AGENDAV_ENC_KEY=my_encrypt10n_k3y -e AGENDAV_CALDAV_SERVER=http://<BAIKAL_IP>/dav.php -e AGENDAV_CALDAV_PUBLIC_URL=http://<BAIKAL_IP>/dav.php -e AGENDAV_TIMEZONE=UTC -e  AGENDAV_LANG=en -e AGENDAV_LOG_DIR=/tmp/ --network net -p 8080:8080 -v /share_docker/agendav:/tmp --log-driver syslog --log-opt tag=DOCKER_agendav --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/nagimov/agendav-docker:latest && docker start CTX_agendav
