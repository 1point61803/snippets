#!/bin/bash
echo "###_ SETUP BACKUP PROXMOX _###"
date +"%m-%d-%y"
echo "###_ INFO BTRFS _###"
btrfs fi show
echo "###_ STORAGE 4TB _###"
btrfs device stats /storage
btrfs scrub start /storage
btrfs filesystem defragment /storage
echo "###_ DATA 1TB _###"
btrfs device stats /data
btrfs scrub start /data
btrfs filesystem defragment /data

echo "###_ BACKUP PROXMOX _###"
rsync -avuh --delete --exclude /dev --exclude /sys --exclude /proc --exclude /data --exclude /storage --exclude /mnt --exclude /run --exclude /media --exclude /srv --exclude /var --exclude /lib* --stats / /data/Backup/BK_proxmox/
