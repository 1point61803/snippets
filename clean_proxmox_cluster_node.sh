#!/bin/bash

# remove cluster node in proxmox
# stop cluster service
systemctl stop pve-cluster
systemctl stop corosync
pmxcfs -l

# remove all config
rm /etc/pve/corosync.conf
rm /etc/corosync/*

# umount and restart cluster service
killall pmxcfs
systemctl start pve-cluster 
