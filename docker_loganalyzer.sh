#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/loganalayzer_data /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/loganalayzer_data /share_docker/mariadb_data

# create tmp network
docker network create net --subnet=172.18.0.0/24

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$LA_db_user -e MARIADB_PASSWORD=$LA_db_password -e  MARIADB_ROOT_PASSWORD=$LA_db_password -e MYSQL_DATABASE=$LA_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net --ip 172.18.0.2 -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_mariadb --log-opt syslog-address=udp://10.20.0.30:1514 mariadb --transaction-isolation=READ-COMMITTED --binlog-format=ROW --innodb-file-per-table=1 --skip-innodb-read-only-compressed

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net --ip 172.18.0.10 -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_adminer --log-opt syslog-address=udp://10.20.0.30:1514 adminer

# get loganalyzer image
docker pull aguyonnet/loganalyzer

# create loganalyzer container
docker create --name CTX_loganalyzer --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_PASSWORD=$LA_db_password -e MYSQL_DATABASE=$LA_db_name -e MYSQL_USER=$LA_db_user -e MYSQL_HOST=mariadb -v /share_docker/loganalayzer_data:/var/log/syslog --network net --ip 172.18.0.2 -p 80:80 --log-driver syslog --log-opt tag=DOCKER_nextcloud --log-opt syslog-address=udp://10.20.0.30:514 aguyonnet/loganalyzer

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_loganalyzer

# one shitty line
mkdir -p /share_docker/loganalayzer_data /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/loganalayzer_data /share_docker/mariadb_data && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$LA_db_user -e MARIADB_PASSWORD=$LA_db_password -e  MARIADB_ROOT_PASSWORD=$LA_db_password -e MYSQL_DATABASE=$LA_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_mariadb --log-opt syslog-address=udp://10.20.0.30:1514 mariadb --transaction-isolation=READ-COMMITTED --binlog-format=ROW --innodb-file-per-table=1 --skip-innodb-read-only-compressed && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_adminer --log-opt syslog-address=udp://10.20.0.30:1514 adminer && docker pull aguyonnet/loganalyzer && docker create --name CTX_loganalyzer --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_PASSWORD=$LA_db_password -e MYSQL_DATABASE=$LA_db_name -e MYSQL_USER=$LA_db_user -e MYSQL_HOST=mariadb -v /share_docker/loganalayzer_data:/var/log/syslog --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_nextcloud --log-opt syslog-address=udp://10.20.0.30:514 aguyonnet/loganalyzer && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_loganalyzer

# prepare mysql
docker exec -it CTX_mariadb bash
| wget https://raw.githubusercontent.com/rsyslog/rsyslog/master/plugins/ommysql/createDB.sql
| mysql -u root -p < createDB.sql
| mysql -u root -p 
||  GRANT ALL ON Syslog.* TO '$LA_user'@'%' IDENTIFIED BY '$LA_password';
||  FLUSH PRIVILEGES;
||  EXIT

# prepare rsyslog
apk add rsyslog-mysql
vim /etc/rsyslog.conf
| module(load="ommysql")
| module(load="imudp")
| input( 
|	type="imudp"
| 	port="514"
| )
| *.* :ommysql:$LA_db_ip,Syslog,$LA_user,$LA_password
