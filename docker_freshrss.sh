#!/bin/bash

# get image
docker pull linuxserver/freshrss

# create container
docker create --name=freshrss -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -p 80:80 -v /share_docker:/config --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_freshrss --log-opt syslog-address=udp://10.20.0.30:514 linuxserver/freshrss

# start container
docker start freshrss

# one shitty line
docker pull linuxserver/freshrss && docker create --name=freshrss -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -p 80:80 -v /share_docker:/config --restart unless-stopped --log-driver syslog --log-opt tag=DOCKER_freshrss --log-opt syslog-address=udp://10.20.0.30:514 linuxserver/freshrss && docker start freshrss
