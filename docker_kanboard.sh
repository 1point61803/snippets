#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/kanboard_data /share_docker/kanboard_plugins /share_docker/kanboard_ssl /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$KB_db_user -e MARIADB_PASSWORD=$KB_db_password -e  MARIADB_ROOT_PASSWORD=$KB_db_password -e MYSQL_DATABASE=$KB_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h db -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_kb_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb --default-authentication-plugin=mysql_native_password

# get kanboard image
docker pull kanboard/kanboard:latest

# create kanboard container
docker create --name CTX_kanboard --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PLUGIN_INSTALLER=true -e DATABASE_URL=mysql://$KB_db_user:$KB_db_password@db/$KB_db_name --network net -h kanboard -p 80:80 -p 443:443 -v /share_docker/kanboard_data:/var/www/app/data -v /share_docoker/kanboard_plugins:/var/www/app/plugins -v /share_docoker/kanboard_ssl:/etc/nginx/ssl --log-driver syslog --log-opt tag=DOCKER_KB_kanboard --log-opt syslog-address=udp://10.20.0.30:514 kanboard/kanboard

# start jellyfin container
docker start CTX_mariadb
docker start CTX_kanboard

# one shitty line
mkdir -p /share_docker/kanboard_data /share_docker/kanboard_plugins /share_dcoker/kanboard_ssl /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$KB_db_user -e MARIADB_PASSWORD=$KB_db_password -e  MARIADB_ROOT_PASSWORD=$KB_db_password -e MYSQL_DATABASE=$KB_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h db -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_kb_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull kanboard/kanboard:latest && docker create --name CTX_kanboard --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PLUGIN_INSTALLER=true -e DATABASE_URL=mysql://$KB_db_user:$KB_db_password@db/$KB_db_name --network -h kanboard net -p 80:80 -p 443:443 -v /share_docker/kanboard_data:/var/www/app/data -v /share_dcoker/kanboard_plugins:/var/www/app/plugins -v /share_dcoker/kanboard_ssl:/etc/nginx/ssl --log-driver syslog --log-opt tag=DOCKER_KB_kanboard --log-opt syslog-address=udp://10.20.0.30:514 kanboard/kanboard:latest && docker start CTX_mariadb && docker start CTX_kanboard
