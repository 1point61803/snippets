#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/ttrss_data
chown 1000:1000 -R /share_docker/ttrss_data

# create tmp network
docker network create net

# get postgres image
docker pull postgres:latest

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$TT_db_user -e POSTGRES_PASSWORD=$TT_db_password -e DB_EXTENSION=pg_trgm -p 5432:5432 -v /share_docker/ttrss_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:latest

# get ttrss server image
docker pull wangqiru/ttrss

# create ttrss server container
docker create --name CTX_ttrss --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SELF_URL_PATH=http://ttrss.service.local -e DB_HOST=CTX_postgres -e DB_PORT=5432 -e DB_NAME=myttrss -e DB_USER=$TT_db_user -e DB_PASS=$TT_db_password --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_ttrss --log-opt syslog-address=udp://10.20.0.30:514 wangqiru/ttrss

# start container
docker start CTX_postgres
docker start CTX_ttrss

# one shitty line
mkdir -p /share_docker/ttrss_data && chown 1000:1000 -R /share_docker/ttrss_data && docker network create net && docker pull postgres:latest && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$TT_db_user -e POSTGRES_PASSWORD=$TT_db_password -e DB_EXTENSION=pg_trgm -p 5432:5432 -v /share_docker/ttrss_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:latest && docker pull wangqiru/ttrss && docker create --name CTX_ttrss --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SELF_URL_PATH=http://ttrss.service.local -e DB_HOST=CTX_postgres -e DB_PORT=5432 -e DB_NAME=myttrss -e DB_USER=$TT_db_user -e DB_PASS=$TT_db_password --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_ttrss --log-opt syslog-address=udp://10.20.0.30:514 wangqiru/ttrss && docker start CTX_postgres && docker start CTX_ttrss

