#!/bin/bash

# create tmp network
docker network create net

# get jellyfin image
docker pull dnknth/ldap-ui

# create jellyfin container [option LDAP_URL: ldap:/// - LOGIN_ATTR:]
docker create --name CTX_ldapui --restart unless-stopped -e PUID=1000 -e PGID=1000 -e BASE_DN=dc=infra,dc=local --network net -h ldapui -p 5000:5000 --log-driver syslog --log-opt tag=DOCKER_ldapui --log-opt syslog-address=udp://10.20.0.30:514 dnknth/ldap-ui

# start jellyfin container
docker start CTX_ldapui

# one shitty line
docker network create net && docker pull dnknth/ldap-ui && docker create --name CTX_ldapui --restart unless-stopped -e PUID=1000 -e PGID=1000 -e BASE_DN=dc=infra,dc=local --network net -h ldapui -p 5000:5000 --log-driver syslog --log-opt tag=DOCKER_ldapui --log-opt syslog-address=udp://10.20.0.30:514 dnknth/ldap-ui && docker start CTX_ldapui
