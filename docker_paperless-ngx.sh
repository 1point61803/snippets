#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/RD_data /share_docker/PG_data /share_docker/PP_data /share_docker/PP_media /share_docker/PP_export
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get redis image
docker pull redis:6.0

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/RD_data:/data --network net -h redis -p 6379:6379  --log-driver syslog --log-opt tag=DOCKER_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:6.0

# start container
docker start CTX_redis

# get postgres image
docker pull postgres:13

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_DB=$PP_pg_db -e POSTGRES_USER=$PP_pg_user -e POSTGRES_PASSWORD=$PP_pg_password -v /share_docker/PG_data:/var/lib/postgresql/data --network net -h postgres -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13

# start container
docker start CTX_postgres

# get paperless-ngx image
docker pull ghcr.io/paperless-ngx/paperless-ngx:latest

# create paperless-ngx container
docker create --name CTX_paperless-ngx --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PAPERLESS_DBENGINE=postgres -e PAPERLESS_DBHOST=postgres -e PAPERLESS_DBUSER=$PP_pg_user -e PAPERLESS_DBPASS=$PP_pg_password --env-file /root/paperlessngx.env -e PAPERLESS_REDIS=redis://redis:6379 -e PAPERLESS_TIKA_ENABLED=1 -e PAPERLESS_TIKA_GOTENBERG_ENDPOINT=http://gotenberg:3000 -e PAPERLESS_TIKA_ENDPOINT=http://tika:9998 -v /share_docker/PP_data:/usr/src/paperless/data -v /share_docker/PP_media:/usr/src/paperless/media -v /share_docker/PP_export:/usr/src/paperless/export -v /mnt/misc/Documenti/Importati/:/usr/src/paperless/consume --network net -h paperlessngx -p 8000:8000  --log-driver syslog --log-opt tag=DOCKER_paperless-ngx --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/paperless-ngx/paperless-ngx:latest

# start container
docker start CTX_paperless-ngx

# get gotenberg image
docker pull gotenberg/gotenberg:7.4

# create gotenberg container
docker create --name CTX_gotenberg --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h gotenberg -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_gotenberg --log-opt syslog-address=udp://10.20.0.30:514 gotenberg/gotenberg:7.4 gotenberg --chromium-disable-routes=true

# start container
docker start CTX_gotenberg

# get tika image
docker pull ghcr.io/paperless-ngx/tika:latest

# create tika container
docker create --name CTX_tika --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h tika -p 9998:9998  --log-driver syslog --log-opt tag=DOCKER_tika --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/paperless-ngx/tika:latest

# start container
docker start CTX_tika

# one shitty line
mkdir -p /share_docker/RD_data /share_docker/PG_data /share_docker/PP_data /share_docker/PP_media /share_docker/PP_config && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull redis:6.0 && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/RD_data:/data --network net -h redis -p 6379:6379  --log-driver syslog --log-opt tag=DOCKER_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:6.0 && docker start CTX_redis && docker pull postgres:13 && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_DB=$PP_pg_db -e POSTGRES_USER=$PP_pg_user -e POSTGRES_PASSWORD=$PP_pg_password -v /share_docker/PG_data:/var/lib/postgresql/data --network net -h postgres -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13 && docker start CTX_postgres && docker pull ghcr.io/paperless-ngx/paperless-ngx:latest && docker create --name CTX_paperless-ngx --restart unless-stopped -e PUID=1000 -e PGID=1000 -e PAPERLESS_DBENGINE=postgres -e PAPERLESS_DBHOST=postgres -e PAPERLESS_DBUSER=$PP_pg_user -e PAPERLESS_DBPASS=$PP_pg_password --env-file /root/paperlessngx.env -e PAPERLESS_REDIS=redis://redis:6379 -e PAPERLESS_TIKA_ENABLED=1 -e PAPERLESS_TIKA_GOTENBERG_ENDPOINT=http://gotenberg:3000 -e PAPERLESS_TIKA_ENDPOINT=http://tika:9998 -v /share_docker/PP_data:/usr/src/paperless/data -v /share_docker/PP_media:/usr/src/paperless/media -v /share_docker/PP_export:/usr/src/paperless/export -v /mnt/misc/Documenti/Importati/:/usr/src/paperless/consume --network net -h paperlessngx -p 8000:8000  --log-driver syslog --log-opt tag=DOCKER_paperless-ngx --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/paperless-ngx/paperless-ngx:latest && docker start CTX_paperless-ngx && docker pull gotenberg/gotenberg:7.4 && docker create --name CTX_gotenberg --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h gotenberg -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_gotenberg --log-opt syslog-address=udp://10.20.0.30:514 gotenberg/gotenberg:7.4 gotenberg --chromium-disable-routes=true && docker start CTX_gotenberg && docker pull ghcr.io/paperless-ngx/tika:latest && docker create --name CTX_tika --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h tika -p 9998:9998  --log-driver syslog --log-opt tag=DOCKER_tika --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/paperless-ngx/tika:latest && docker start CTX_tika
