#!/bin/bash

# create share folder
mkdir -p /share_docker/watchtower
chown 1000:1000 -R /share_docker/watchtower

# create file env
echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env


# create tmp network
docker network create net

# get promtail image
docker pull containrrr/watchtower

# create promtail container
docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower

# start container
docker start CTX_watchtower

# one shitty line
mkdir -p /share_docker/watchtower && chown 1000:1000 -R /share_docker/watchtower && echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env && docker network create net && docker pull containrrr/watchtower && docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower && docker start CTX_watchtower
