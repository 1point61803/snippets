#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/env_conf /share_docker/mysql_data /share_docker/snipe-it_conf

# create env file
cat <<EOF >> /share_docker/env_conf/env
# Mysql Parameters
MYSQL_PORT_3306_TCP_ADDR=snipe-mysql
MYSQL_PORT_3306_TCP_PORT=3306
MYSQL_ROOT_PASSWORD=$SI_db_root_password
MYSQL_DATABASE=$SI_db_name
MYSQL_USER=$SI_db_user
MYSQL_PASSWORD=$SI_db_password

# Snipe-IT Settings
APP_ENV=production
APP_DEBUG=false
APP_KEY=$SI_api_token
APP_URL=http://127.0.0.1:80
APP_TIMEZONE=Europe/Rome
APP_LOCALE=en
EOF

chown 1000:1000 -R /share_docker/env_conf /share_docker/mysql_data /share_docker/snipe-it_conf

# create tmp network
docker network create net

# get mysql image
docker pull mysql:5.6

# create mysql container
docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --env-file /share_docker/env_conf/env -v /share_docker/mysql_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_SI_mysql --log-opt syslog-address=udp://10.20.0.30:514 mysql:5.6 --default-authentication-plugin=mysql_native_password

# get snipe-it image
docker pull snipe/snipe-it

# create snipe-it container
docker create --name CTX_snipeit --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/env_conf/env --network net -p 3051:80 --log-driver syslog --log-opt tag=DOCKER_SI_snipeit --log-opt syslog-address=udp://10.20.0.30:514 snipe/snipe-it

# start container
docker start CTX_mysql
docker start CTX_snipeit
 
# one shitty line
mkdir -p /share_docker/env_conf /share_docker/mysql_data /share_docker/snipe-it_conf && cat <<EOF >> /share_docker/env_conf/env
# Mysql Parameters
MYSQL_PORT_3306_TCP_ADDR=snipe-mysql
MYSQL_PORT_3306_TCP_PORT=3306
MYSQL_ROOT_PASSWORD=$SI_db_root_password
MYSQL_DATABASE=$SI_db_name
MYSQL_USER=$SI_db_user
MYSQL_PASSWORD=$SI_db_password

# Snipe-IT Settings
APP_ENV=production
APP_DEBUG=false
APP_KEY=$SI_api_token
APP_URL=http://127.0.0.1:80
APP_TIMEZONE=Europe/Rome
APP_LOCALE=en
EOF && chown 1000:1000 -R /share_docker/env_conf /share_docker/mysql_data /share_docker/snipe-it_conf && docker network create net && docker pull mysql:5.6 && docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --env-file /share_docker/env_conf/env -v /share_docker/mysql_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_SI_mysql --log-opt syslog-address=udp://10.20.0.30:514 mysql:5.6 --default-authentication-plugin=mysql_native_password && docker pull snipe/snipe-it && docker create --name CTX_snipeit --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/env_conf/env --network net -p 3051:80 --log-driver syslog --log-opt tag=DOCKER_SI_snipeit --log-opt syslog-address=udp://10.20.0.30:514 snipe/snipe-it && docker start CTX_mysql && docker start CTX_snipeit
