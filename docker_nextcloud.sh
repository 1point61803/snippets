#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/mariadb_data /share_docker/nextcloud
chown 1000:1000 -R /share_docker/mariadb_data /share_docker/nextcloud

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$NC_db_user -e MARIADB_PASSWORD=$NC_db_password -e  MARIADB_ROOT_PASSWORD=$NC_db_password -e MYSQL_DATABASE=$NC_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_nc_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb --transaction-isolation=READ-COMMITTED --binlog-format=ROW --innodb-file-per-table=1 --skip-innodb-read-only-compressed

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_nc_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get nextcloud image
docker pull nextcloud

# create nextcloud container
docker create --name CTX_nextcloud --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_PASSWORD=$NC_db_password -e MYSQL_DATABASE=$NC_db_name -e MYSQL_USER=$NC_db_user -e MYSQL_HOST=CTX_mariadb -v /share_docker/nextcloud:/var/www/html --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_nextcloud --log-opt syslog-address=udp://10.20.0.30:514 nextcloud

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_nextcloud

# one shitty line
mkdir -p /share_docker/mariadb_data /share_docker/nextcloud chown 1000:1000 -R /share_docker/mariadb_data /share_docker/nextcloud && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$NC_db_user -e MARIADB_PASSWORD=$NC_db_password -e  MARIADB_ROOT_PASSWORD=$NC_db_password -e MYSQL_DATABASE=$NC_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_nc_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb --transaction-isolation=READ-COMMITTED --binlog-format=ROW --innodb-file-per-table=1 --skip-innodb-read-only-compressed && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_nc_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull nextcloud && docker create --name CTX_nextcloud --link CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_PASSWORD=$NC_db_password -e MYSQL_DATABASE=$NC_db_name -e MYSQL_USER=$NC_db_user -e MYSQL_HOST=mariadb -v /share_docker/nextcloud:/var/www/html --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_nextcloud --log-opt syslog-address=udp://10.20.0.30:514 nextcloud && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_nextcloud
