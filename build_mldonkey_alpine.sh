#!/bin/bash

apk add make autoconf g++ libbz2 libc6-compat libmagic miniupnpc ocaml-dev ocaml-compiler-libs ocaml-runtime ocamlbuild zlib-dev

wget https://github.com/ygrek/mldonkey/releases/download/release-3-1-7-2/mldonkey-3.1.7-2.tar.bz2

tar -xvf mldonkey-3.1.7-2.tar.bz2 

cd mldonkey-3.1.7-2

./configure --enable-batch --disable-multinet --disable-gui --enable-gd --enable-magic X 2

make

