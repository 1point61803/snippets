#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/data
chown 1000:1000 -R /share_docker/data

# create tmp network
docker network create net

# get papermerge image
docker pull nitnelave/lldap:stable

# create papermerge container
docker create --name CTX_lldap --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LLDAP_JWT_SECRET=$LL_secret -e LLDAP_LDAP_USER_PASS=$LL_password -e  LLDAP_LDAP_BASE_DN=$LL_base_dn -v /share_docker/data:/data  --network net -p 3890:3890 -p 17170:17170 --log-driver syslog --log-opt tag=DOCKER_lldap --log-opt syslog-address=udp://10.20.0.30:514 nitnelave/lldap:stable

# start container
docker start CTX_lldap

# one shitty line
mkdir -p /share_docker/data && chown 1000:1000 -R /share_docker/data && docker network create net && docker pull nitnelave/lldap:stable && docker create --name CTX_lldap --restart unless-stopped -e PUID=1000 -e PGID=1000 -e LLDAP_JWT_SECRET=$LL_secret -e LLDAP_LDAP_USER_PASS=$LL_password -e  LLDAP_LDAP_BASE_DN=$LL_base_dn -v /share_docker/data:/data  --network net -p 3890:3890 -p 17170:17170 --log-driver syslog --log-opt tag=DOCKER_lldap --log-opt syslog-address=udp://10.20.0.30:514 nitnelave/lldap:stable && docker start CTX_lldap
