#!/bin/bash

# clone web components jellyfin app
git clone https://github.com/jellyfin/jellyfin-web.git

# clone tizen app jellyfin
git clone https://github.com/jellyfin/jellyfin-tizen.git

# prepare web components
cd jellyfin-web/
yarn install

# prepare tizen app
cd jellyfin-tizen
JELLYFIN_WEB_DIR=../jellyfin-web/dist yarn install

# compile web components
/path/to/bin/tizen build-web -e ".*" -e gulpfile.js -e README.md -e "node_modules/*" -e "package*.json" -e "yarn.lock"

# compile tizen app
/path/to/bin/tizen  package -t wgt -o . -- .buildResult                                                               

# connect to a TV with debug mode active
--

# load the test app into the TV
/path/to/bin/tizen install -n Jellyfin.wgt -t UJU6400        


