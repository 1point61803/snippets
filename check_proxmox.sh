#!/bin/bash
echo "###_ PROXMOX _###"
echo "###_ UPTIME _###"
uptime

echo "###_ PROXMOX _###"
echo "###_ APT UPDATE _###"
apt update

echo "###_ PROXMOX _###"
echo "###_ APT UPGRADE _###"
apt upgrade -y

echo "###_ PROXMOX _###"
echo "###_ CLEAN DEBIAN _###"
apt clean
apt autoremove -y

echo "###_ PROXMOX _###"
echo "###_ INFO _###"
df -h
free -mt
dpkg -l | egrep "^rc" | cut -d' ' -f3
lvs


echo "###_ LXC [200->218] _###"
echo "###_ UPTIME _###"
ansible -m shell -a "uptime" lxc_all

echo "###_ LXC [200->218] _###"
echo "###_ APT UPDATE _###"
ansible -m shell -a "apt update" lxc_all

echo "###_ LXC [200->218] _###"
echo "###_ APT UPGRADE _###"
ansible -m shell -a "apt upgrade -y" lxc_all

echo "###_ LXC [200->218] _###"
echo "###_ CLEAN DEBIAN _###"
ansible -m shell -a "apt autoclean" lxc_all
ansible -m shell -a "apt autoremove -y" lxc_all

echo "###_ LXC [200->218] _###"
echo "###_ INFO _###"
ansible -m shell -a "df -h" lxc_all
ansible -m shell -a "free -mt" lxc_all
ansible -m shell -a "dpkg -l | egrep "^rc" | cut -d' ' -f3" lxc_all
