#!/bin/bash

# create share folder
mkdir -p /share_docker/ossec_data
chown 1000:1000 -R /share_docker/ossec_data

# create tmp network
docker network create net

# get ossec image
docker pull atomicorp/ossec-docker

# create ossec container
docker create --name CTX_ossec --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/ossec_data:/var/ossec/data --network net -p 1514:1514/udp -p 1515:1515/tcp --log-driver syslog --log-opt tag=DOCKER_ossec --log-opt syslog-address=udp://10.20.0.30:514 atomicorp/ossec-docker

# start container
docker start CTX_ossec

# one shitty line
mkdir -p /share_docker/ossec_data && chown 1000:1000 -R /share_docker/ossec_data && docker network create net && docker pull atomicorp/ossec-docker && docker create --name CTX_ossec --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/ossec_data:/var/ossec/data --network net -p 1514:1514/udp -p 1515:1515/tcp --log-driver syslog --log-opt tag=DOCKER_ossec --log-opt syslog-address=udp://10.20.0.30:514 atomicorp/ossec-docker && docker start CTX_ossec 
