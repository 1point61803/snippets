#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/postgres/config /share_folder/invidious /share_docker/watchtower
chown 1000:1000 -R /share_docker/*

# create file env
echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env

echo -e "PUID=1000\nPGID=1000\nPOSTGRES_INITDB_ARGS=--data-checksums\nPOSTGRES_DB=$IN_db_name\nPOSTGRES_USER=$IN_db_user\nPOSTGRES_PASSWORD=$IN_db_password" > /share_docker/postgres/run.env

echo -e "PUID=1000\nPGID=1000\nINVIDIOUS_CONFIG: |\ndb:\ndbname: $IN_db_name\nuser: $IN_db_user\npassword: $IN_db_password\nhost: invidious-db\nport: 5432\ncheck_tables: true" > /share_docker/invidious/run.env

# get init db script
wget https://raw.githubusercontent.com/iv-org/invidious/master/docker/init-invidious-db.sh -p /share_docker/postgres
chown 1000:1000 /share_docker/postgres/init-invidious-db.sh
chmod +x /share_docker/postgres/init-invidious-db.sh

# create tmp network
docker network create net

# get postgres image
docker pull docker.io/library/postgres:14

# create postgres container
docker create --name CTX_postgres --restart unless-stopped --env-file /share_docker/postgres/run.env -v /share_docker/postgres:/var/lib/postgresql/data -v /share_docker/postgres/config/sql:/config/sql -v /share_docker/postgrs/init-invidious-db.sh:/docker-entrypoint-initdb.d/init-invidious-db.sh --network net -h IN_db_host --log-driver syslog --log-opt tag=DOCKER_in_postgres --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/postgres:14

# get promtail image
docker pull quay.io/invidious/invidious:latest

# create promtail container
docker create --name CTX_invidious --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_invidious --log-opt syslog-address=udp://10.20.0.30:514 quay.io/invidious/invidious:latest

# get promtail image
docker pull containrrr/watchtower

# create promtail container
docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_in_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower

# start container
docker start CTX_watchtower
docker start CTX_postgres
docker start CTX_invidious

# one shitty line
mkdir -p /share_docker/postgres/config /share_folder/invidious /share_docker/watchtower && chown 1000:1000 -R /share_docker/* && echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env && echo -e "PUID=1000\nPGID=1000\nPOSTGRES_INITDB_ARGS=--data-checksums\nPOSTGRES_DB=$IN_db_name\nPOSTGRES_USER=$IN_db_user\nPOSTGRES_PASSWORD=$IN_db_password" > /share_docker/postgres/run.env && echo -e "PUID=1000\nPGID=1000\nINVIDIOUS_CONFIG: |\ndb:\ndbname: $IN_db_name\nuser: $IN_db_user\npassword: $IN_db_password\nhost: invidious-db\nport: 5432\ncheck_tables: true" > /share_docker/invidious/run.env && wget https://raw.githubusercontent.com/iv-org/invidious/master/docker/init-invidious-db.sh -p /share_docker/postgres && chown 1000:1000 /share_docker/postgres/init-invidious-db.sh && chmod +x /share_docker/postgres/init-invidious-db.sh && docker network create net && docker pull docker.io/library/postgres:14 && docker create --name CTX_postgres --restart unless-stopped --env-file /share_docker/postgres/run.env -v /share_docker/postgres:/var/lib/postgresql/data -v /share_docker/postgres/config/sql:/config/sql -v /share_docker/postgrs/init-invidious-db.sh:/docker-entrypoint-initdb.d/init-invidious-db.sh --network net -h IN_db_host --log-driver syslog --log-opt tag=DOCKER_in_postgres --log-opt syslog-address=udp://10.20.0.30:514 docker.io/library/postgres:14 && docker pull quay.io/invidious/invidious:latest && docker create --name CTX_invidious --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_invidious --log-opt syslog-address=udp://10.20.0.30:514 quay.io/invidious/invidious:latest && docker pull containrrr/watchtower && docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_in_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower && docker start CTX_watchtower && docker start CTX_postgres && docker start CTX_invidious

