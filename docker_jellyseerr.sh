 #!/bin/bash

# create share folder
mkdir -p /share_docker/jellyseerr_conf
chown 1000:1000 -R /share_docker/jellyseerr_conf

# create tmp network
docker network create net

# get jellyseerr image
docker pull fallenbagel/jellyseerr:latest

# create jellyseerr container
docker create --name CTX_jellyseerr --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/jellyseerr_conf:/app/config --network net -p 5055:5055 --log-driver syslog --log-opt tag=DOCKER_jellyseerr --log-opt syslog-address=udp://10.20.0.30:514  fallenbagel/jellyseerr:latest

# start container
docker start CTX_jellyseerr
 
# one shitty line
mkdir -p /share_docker/jellyseerr_conf && chown 1000:1000 -R /share_docker/jellyseerr_conf && docker network create net && docker pull fallenbagel/jellyseerr:latest && docker create --name CTX_jellyseerr --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/jellyseerr_conf:/app/config --network net -p 5055:5055 --log-driver syslog --log-opt tag=DOCKER_jellyseerr --log-opt syslog-address=udp://10.20.0.30:514  fallenbagel/jellyseerr:latest && docker start CTX_jellyseerr
