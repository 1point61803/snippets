#!/bin/bash

# add docker and docker-compose
apk add docker docker-compose

# create and add normal user to docker group
adduser dockeruser
addgroup dockeruser docker


# add docker service start at boot
rc-update add docker boot
service docker start

# one shitty line
apk add docker docker-compose && adduser dockeruser && addgroup dockeruser docker && rc-update add docker boot && service docker start && mkdir /share_docker
