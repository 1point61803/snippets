#!/bin/bash

# include secret
source SECRET

# create persistent folder
mkdir -p /share_docker/mariadb_data /share_docker/rundeck_conf
chown 1000:1000 -R /share_docker/mariadb_data /share_docker/rundeck_conf
  
# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$RD_db_user -e MARIADB_PASSWORD=$RD_db_password -e  MARIADB_ROOT_PASSWORD=$RD_db_password -e MYSQL_DATABASE=$RD_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h $RD_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_sm_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_sm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get rundeck image
docker pull rundeck/rundeck:latest

# create rundeck container
docker create --name CTX_rundeck --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e RUNDECK_DATABASE_DRIVER=org.mariadb.jdbc.Driver -e  RUNDECK_DATABASE_USERNAME=$RD_db_name -e RUNDECK_DATABASE_PASSWORD=$RD_db_password -e RUNDECK_DATABASE_URL=jdbc:mysql://$RD_db_host/rundeck?autoReconnect=true&useSSL=false -e RUNDECK_GRAILS_URL=localhost:4440 -v /share_docker/rundeck_conf:/home/rundeck/etc/rundeckpro-license.key --network net -p 4440:4440 --log-driver syslog --log-opt tag=DOCKER_rundeck --log-opt syslog-address=udp://10.20.0.30:514 rundeck/rundeck:latest

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_rundeck

# one shitty line
mkdir -p /share_docker/mariadb_data /share_docker/rundeck_conf && chown 1000:1000 -R /share_docker/mariadb_data /share_docker/rundeck_conf && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$RD_db_user -e MARIADB_PASSWORD=$RD_db_password -e  MARIADB_ROOT_PASSWORD=$RD_db_password -e MYSQL_DATABASE=$RD_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h $RD_db_host -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_sm_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_sm_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull rundeck/rundeck:latest && docker create --name CTX_rundeck --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e RUNDECK_DATABASE_DRIVER=org.mariadb.jdbc.Driver -e  RUNDECK_DATABASE_USERNAME=$RD_db_name -e RUNDECK_DATABASE_PASSWORD=$RD_db_password -e RUNDECK_DATABASE_URL=jdbc:mysql://$RD_db_host/rundeck?autoReconnect=true&useSSL=false -e RUNDECK_GRAILS_URL=localhost:4440 -v /share_docker/rundeck_conf:/home/rundeck/etc/rundeckpro-license.key --network net -p 4440:4440 --log-driver syslog --log-opt tag=DOCKER_rundeck --log-opt syslog-address=udp://10.20.0.30:514 rundeck/rundeck:latest && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_rundeck
