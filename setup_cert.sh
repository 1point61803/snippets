#!/bin/bash

# create private ca key
openssl genpkey -algorithm RSA -aes128 -out private-ca.key -outform PEM -pkeyopt rsa_keygen_bits:2048

# create self signed ca-cert
openssl req -x509 -new -nodes -sha256 -days 3650 -key private-ca.key -out self-signed-ca-cert.crt

# create private key 
openssl genpkey -algorithm RSA -out bitwarden.key -outform PEM -pkeyopt rsa_keygen_bits:2048

# create csr file
openssl req -new -key bitwarden.key -out bitwarden.csr

# create vault key
openssl rsa -in bitwarden.key -out vault_key.pem -outform PEM

# create file ext
echo "authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = vaultwarden.local
DNS.2 = vaulwarden.service.local" > bitwarden.ext


# create general crt file 
openssl x509 -req -in bitwarden.csr -CA self-signed-ca-cert.crt -CAkey private-ca.key -CAcreateserial -out bitwarden.crt -days 365 -sha256 -extfile bitwarden.ext

# create vault cert
openssl x509 -in bitwarden.crt -out vault.pem -outform PEM

