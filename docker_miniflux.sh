#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/miniflux_data
chown 1000:1000 -R /share_docker/miniflux_data

# create tmp network
docker network create net

# get postgres image
docker pull postgres:latest

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$MN_db_user -e POSTGRES_PASSWORD=$MN_db_password -v /share_docker/miniflux_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:latest

# get miniflux server image
docker pull miniflux/miniflux:latest

# create miniflux server container
docker create --name CTX_miniflux --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DATABASE_URL=$MN_db_url -e RUN_MIGRATIONS=1 -e CREATE_ADMIN=1 -e  ADMIN_USERNAME=$MN_admin_user -e ADMIN_PASSWORD=$MN_admin_password --network net -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_miniflux --log-opt syslog-address=udp://10.20.0.30:514 miniflux/miniflux:latest

# start container
docker start CTX_postgres
docker start CTX_miniflux

# Run database migrations
docker exec CTX_miniflux /usr/bin/miniflux -migrate

# Create the first user
docker exec CTX_miniflux /usr/bin/miniflux -create-admin

# one shitty line
mkdir -p /share_docker/miniflux_data && chown 1000:1000 -R /share_docker/miniflux_data && docker network create net && docker pull postgres:latest && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_USER=$MN_db_user -e POSTGRES_PASSWORD=$MN_db_password -v /share_docker/miniflux_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:latest && docker pull miniflux/miniflux:latest && docker create --name CTX_miniflux --restart unless-stopped -e PUID=1000 -e PGID=1000 -e DATABASE_URL=$MN_db_url -e RUN_MIGRATIONS=1 -e CREATE_ADMIN=1 -e  ADMIN_USERNAME=$MN_admin_user -e ADMIN_PASSWORD=$MN_admin_password --network net -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_miniflux --log-opt syslog-address=udp://10.20.0.30:514 miniflux/miniflux:latest && docker start CTX_postgres && docker start CTX_miniflux && docker exec  CTX_miniflux /usr/bin/miniflux -migrate && docker exec CTX_miniflux /usr/bin/miniflux -create-admin
