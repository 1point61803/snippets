#!/bin/bash

# include secret
source SECRET

# create tmp network
docker network create net

# get jellyfin image
docker pull wheelybird/ldap-user-manager:v1.5

# create jellyfin container
docker create --name CTX_ytdlsub --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SERVER_HOSTNAME=$LD_hostname -e LDAP_URI=$LD_uri -e LDAP_BASE_DN=$LD_domainname -e LDAP_REQUIRE_STARTTLS=TRUE -e LDAP_ADMINS_GROUP=$LD_admingroup -e LDAP_ADMIN_BIND_DN=$LD_admin_domainname -e LDAP_ADMIN_BIND_PWD=$LD_admin_pwd -e "LDAP_IGNORE_CERT_ERRORS=true"  -e LDAP_EMAIL_DN=$LD_domain_mail  --network net -h ldapusermanager -p 80:80 -p 443:443 --log-driver syslog --log-opt tag=DOCKER_ldapusermanager --log-opt syslog-address=udp://10.20.0.30:514 wheelybird/ldap-user-manager:v1.5

# start jellyfin container
docker start CTX_ytdlsub

# one shitty line
docker network create net && docker pull wheelybird/ldap-user-manager:v1.5 && docker create --name CTX_ytdlsub --restart unless-stopped -e PUID=1000 -e PGID=1000 -e SERVER_HOSTNAME=$LD_hostname -e LDAP_URI=$LD_uri -e LDAP_BASE_DN=$LD_domainname -e LDAP_REQUIRE_STARTTLS=TRUE -e LDAP_ADMINS_GROUP=$LD_admingroup -e LDAP_ADMIN_BIND_DN=$LD_admin_domainname -e LDAP_ADMIN_BIND_PWD=$LD_admin_pwd -e "LDAP_IGNORE_CERT_ERRORS=true"  -e LDAP_EMAIL_DN=$LD_domain_mail  --network net -h ldapusermanager -p 80:80 -p 443:443 --log-driver syslog --log-opt tag=DOCKER_ldapusermanager --log-opt syslog-address=udp://10.20.0.30:514 wheelybird/ldap-user-manager:v1.5 && docker start CTX_ytdlsub
