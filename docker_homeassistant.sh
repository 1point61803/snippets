#!/bin/bash

# create share folder
mkdir -p /share_docker/homeassistant /share_docker/nodered
chown 1000:1000 -R /share_docker/homeassistant /share_docker/nodered

# create tmp network
docker network create net

# get nodered image
docker pull nodered/node-red

# create nodered container
docker create --name CTX_nodered --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/nodered:/data --network net -p 1880:1880  --log-driver syslog --log-opt tag=DOCKER_nodered --log-opt syslog-address=udp://10.20.0.30:514 nodered/node-red

# get homeassistant image
docker pull ghcr.io/home-assistant/home-assistant:stable

# create homeassistant container
docker create --name CTX_homeassistant --link CTX_nodered --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/homeassistant:/config --network net -p 8123:8123  --log-driver syslog --log-opt tag=DOCKER_homeassistant --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/home-assistant/home-assistant:stable

# start container
docker start CTX_homeassistant
docker start CTX_nodered

# one shitty line
mkdir -p /share_docker/homeassistant /share_docker/nodered && chown 1000:1000 -R /share_docker/homeassistant /share_docker/nodered && docker pull nodered/node-red && docker create --name CTX_nodered --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/nodered:/data --network net -p 1880:1880  --log-driver syslog --log-opt tag=DOCKER_nodered --log-opt syslog-address=udp://10.20.0.30:514 nodered/node-red && docker pull ghcr.io/home-assistant/home-assistant:stable && docker create --name CTX_homeassistant --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/homeassistant:/config --network net -p 8123:8123  --log-driver syslog --log-opt tag=DOCKER_homeassistant --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/home-assistant/home-assistant:stable && docker start CTX_homeassistant && docker start CTX_nodered

# add homeassistant plugin
npm install node-red-contrib-home-assistant-websocket
