#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/stalwart
chown 1000:1000 -R /share_docker/stalwart

# create tmp network
docker network create net

# get stalwart image
docker pull stalwartlabs/mail-server:latest

# create stalwart container
docker create --name CTX_stalwart --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h stalwart -p 443:443 -p 8080:8080 -p 25:25 -p 587:587 -p 465:465 -p 143:143 -p 993:993 -p 4190:4190 -v /share_docker/stalwart:/opt/stalwart-mail --log-driver syslog --log-opt tag=DOCKER_stalwart --log-opt syslog-address=udp://10.20.0.30:514 stalwartlabs/mail-server:latest

# start stalwart container
docker start CTX_stalwart

# one shitty line
mkdir -p /share_docker/stalwart && chown 1000:1000 -R /share_docker/stalwart && docker network create net && docker pull stalwartlabs/mail-server:latest && docker create --name CTX_stalwart --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h stalwart -p 443:443 -p 8080:8080 -p 25:25 -p 587:587 -p 465:465 -p 143:143 -p 993:993 -p 4190:4190 -v /share_docker/stalwart:/opt/stalwart-mail --log-driver syslog --log-opt tag=DOCKER_stalwart --log-opt syslog-address=udp://10.20.0.30:514 stalwartlabs/mail-server:latest && docker start CTX_stalwart

# see this link for the init process
#https://stalw.art/docs/install/docker/?ref=selfh.st
