#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/guacamole_conf /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/mariadb_data /share_docker/guacamole_conf

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$GM_db_user -e MARIADB_PASSWORD=$GM_db_password -e  MARIADB_ROOT_PASSWORD=$GM_db_password -e MYSQL_DATABASE=$GM_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h mariadb -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_GM_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8070:8080 --log-driver syslog --log-opt tag=DOCKER_GM_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get guacamole image
docker pull guacamole/guacd

# create guacamole container
docker create --name CTX_guacamoleDEAMON --restart unless-stopped -e PUID=1000 -e PGID=1000 -e GUACD_LOG_LEVEL=info --network net -h guacamoleDEAMON -p 4822:4822 --log-driver syslog --log-opt tag=DOCKER_guacamoleDEAMON  --log-opt syslog-address=udp://10.20.0.30:514 guacamole/guacd

# get guacamole image
docker pull guacamole/guacamole

# create guacamole container
docker create --name CTX_guacamoleWEB --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_HOSTNAME=mariadb -e MYSQL_PORT=3306 -e MYSQL_DATABASE=$GM_db_name -e MYSQL_USER=$GM_db_user -e MYSQL_PASSWORD=$GM_db_password -e GUACD_HOSTNAME=guacamoleDEAMON -e GUACD_PORT=4822 -v /share_docker/guacamole_conf:/guacamole_home -e GUACAMOLE_HOME=/guacamole_home --network net -h guacamoleWEB -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_guacamoleWEB --log-opt syslog-address=udp://10.20.0.30:514 guacamole/guacamole

# init DB for guacamole
docker run --rm -e PUID=1000 -e PGID=1000 -e MYSQL_HOSTNAME=mariadb -e MYSQL_PORT=3306 -e MYSQL_DATABASE=$GM_db_name -e MYSQL_USER=$GM_db_user -e MYSQL_PASSWORD=$GM_db_password -e GUACD_HOSTNAME=guacamoleDEAMON -e GUACD_PORT=4822 -v /share_docker/guacamole_conf:/guacamole_home -e GUACAMOLE_HOME=/guacamole_home --network net guacamole/guacamole  /opt/guacamole/bin/initdb.sh --mysql > initdb.sql 

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_guacamoleDEAMON
docker start CTX_guacamoleWEB

# one shitty line
mkdir -p /share_docker/guacamole_conf /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/mariadb_data /share_docker/guacamole_conf && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$GM_db_user -e MARIADB_PASSWORD=$GM_db_password -e  MARIADB_ROOT_PASSWORD=$GM_db_password -e MYSQL_DATABASE=$GM_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -h mariadb -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_GM_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8070:8080 --log-driver syslog --log-opt tag=DOCKER_GM_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull guacamole/guacd && docker create --name CTX_guacamoleDEAMON --restart unless-stopped -e PUID=1000 -e PGID=1000 -e GUACD_LOG_LEVEL=info --network net -h guacamoleDEAMON -p 4822:4822 --log-driver syslog --log-opt tag=DOCKER_guacamoleDEAMON  --log-opt syslog-address=udp://10.20.0.30:514 guacamole/guacd && docker pull guacamole/guacamole && docker create --name CTX_guacamoleWEB --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MYSQL_DATABASE=$GM_db_name -e MYSQL_USER=$GM_db_user -e MYSQL_PASSWORD=$GM_db_password -e GUACD_HOSTNAME=guacamoleDEAMON -e GUACD_PORT=4822 -v /share_docker/guacamole_conf:/guacamole_home -e GUACAMOLE_HOME=/guacamole_home --network net -h guacamoleWEB -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_guacamoleWEB --log-opt syslog-address=udp://10.20.0.30:514 guacamole/guacamole && docker run --rm -e PUID=1000 -e PGID=1000 -e MYSQL_DATABASE=$GM_db_name -e MYSQL_USER=$GM_db_user -e MYSQL_PASSWORD=$GM_db_password --network net guacamole/guacamole  /opt/guacamole/bin/initdb.sh --mysql > initdb.sql && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_guacamoleDEAMON && docker start CTX_guacamoleWEB
    
# login at http://10.40.0.110:8080/guacamole user-> guacadmin password -> guacadmin
# setup on ssh server
#  echo "PubkeyAcceptedAlgorithms=+ssh-rsa" >> /etc/ssh/sshd_config && echo "HostKeyAlgorithms +ssh-rsa" >> /etc/ssh/sshd_config && service sshd restart
