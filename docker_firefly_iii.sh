#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/firefly /share_docker/mariadb_data
chown 1000:1000 -R /share_docker/mariadb_data /share_docker/firefly

# create tmp network
docker network create net

# get mariadb image
docker pull mariadb

# create mariadb container
docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$FF_db_user -e MARIADB_PASSWORD=$FF_db_password -e  MARIADB_ROOT_PASSWORD=$FF_db_password -e MYSQL_DATABASE=$FF_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_ff_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb

# get adminer image
docker pull adminer

# create adminer cotainer
docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_ff_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer

# get firefly image
docker pull fireflyiii/core:latest

# create firefly container
docker create --name CTX_firefly --restart unless-stopped -e PUID=1000 -e PGID=1000 -e APP_KEY=$FF_app_key -e DB_HOST=CTX_mariadb -e DB_PORT=3306 -e DB_CONNECTION=mysql -e DB_DATABASE=$FF_db_name -e DB_USERNAME=$FF_db_user -e DB_PASSWORD=$FF_db_password -v /share_docker/firefly:/var/www/html/storage/upload --network net -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_firefly  --log-opt syslog-address=udp://10.20.0.30:514 fireflyiii/core:latest

# start container
docker start CTX_mariadb
docker start CTX_adminer
docker start CTX_firefly

# one shitty line
mkdir -p /share_docker/firefly /share_docker/mariadb_data && chown 1000:1000 -R /share_docker/mariadb_data /share_docker/nextcloud && docker network create net && docker pull mariadb && docker create --name CTX_mariadb --restart unless-stopped -e PUID=1000 -e PGID=1000 -e MARIADB_USER=$FF_db_user -e MARIADB_PASSWORD=$FF_db_password -e  MARIADB_ROOT_PASSWORD=$FF_db_password -e MYSQL_DATABASE=$FF_db_name -v /share_docker/mariadb_data:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_mariadb --log-opt syslog-address=udp://10.20.0.30:514 mariadb && docker pull adminer && docker create --name CTX_adminer --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -p 8080:8080 --log-driver syslog --log-opt tag=DOCKER_adminer --log-opt syslog-address=udp://10.20.0.30:514 adminer && docker pull fireflyiii/core:latest && docker create --name CTX_firefly --restart unless-stopped -e PUID=1000 -e PGID=1000 -e APP_KEY=$FF_app_key -e DB_HOST=CTX_mariadb -e DB_PORT=3306 -e DB_CONNECTION=mysql -e DB_DATABASE=$FF_db_name -e DB_USERNAME=$FF_db_user -e DB_PASSWORD=$FF_db_password -v /share_docker/firefly:/var/www/html/storage/upload --network net -p 80:8080 --log-driver syslog --log-opt tag=DOCKER_firefly  --log-opt syslog-address=udp://10.20.0.30:514 fireflyiii/core:latest && docker start CTX_mariadb && docker start CTX_adminer && docker start CTX_firefly

### setup a cronjob for the recursive operation
# add a cron job on the host system 
# 0 3 * * * docker exec --user www-data <container> /usr/local/bin/php /var/www/html/artisan firefly-iii:cron
# get the number of CTX with 
# docker container ls -a -f name=firefly --format="{{.ID}}"
