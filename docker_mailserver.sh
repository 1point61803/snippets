#!/bin/bash

# create share folder
mkdir -p /share_docker/mail-data /share_docker/mail-state /share_docker/mail-logs /share_docker/config
chown 1000:1000 -R /share_docker/mail-data /share_docker/mail-state /share_docker/mail-logs /share_docker/config

# create tmp network
docker network create net

# get AIO mailserver image
docker pull docker.io/mailserver/docker-mailserver:latest

# create AIO container
docker create --name CTX_mail-aio --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/mail-data/:/var/mail/ -p /share_docker/mail-state/:/var/mail-state/ -v /share_docker/mail-logs/:/var/log/mail/ -v /share_docker/config/:/tmp/docker-mailserver/ --network net -h mail-aio --domainname 1point61803.com -p 25:25 -p 143:143 -p 587:587 -p 993:993  --log-driver syslog --log-opt tag=DOCKER_mail-aio --log-opt syslog-address=udp://10.20.0.30:514 docker.io/mailserver/docker-mailserver:latest

# start container
docker start CTX_mail-aio

# one shitty line
mkdir -p /share_docker/mail-data /share_docker/mail-state /share_docker/mail-logs /share_docker/config && chown 1000:1000 -R /share_docker/mail-data /share_docker/mail-state /share_docker/mail-logs /share_docker/config && docker network create net && docker pull docker.io/mailserver/docker-mailserver:latest && docker create --name CTX_mail-aio --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/mail-data/:/var/mail/ -p /share_docker/mail-state/:/var/mail-state/ -v /share_docker/mail-logs/:/var/log/mail/ -v /share_docker/config/:/tmp/docker-mailserver/ --network net -h mail-aio --domainname 1point61803.com -p 25:25 -p 143:143 -p 587:587 -p 993:993  --log-driver syslog --log-opt tag=DOCKER_mail-aio --log-opt syslog-address=udp://10.20.0.30:514 docker.io/mailserver/docker-mailserver:latest && docker start CTX_mail-aio
