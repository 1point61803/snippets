#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/ralph_media /share_docker/ralph_dbdata /share_docker/ralph_static
chown 1000:1000 -R /share_docker/ralph_media /share_docker/ralph_dbdata /share_docker/ralph_static

# create tmp network
docker network create net

# get nginx image
docker pull allegro/ralph-static-nginx:latest

# create nginx container
docker create --name CTX_nginx --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/ralph_media:/opt/media --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_RH_nginx --log-opt syslog-address=udp://10.20.0.30:514 allegro/ralph-static-nginx:latest

# get mysql image
docker pull mysql:5.7

# create mysql container
docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e MYSQL_DATABASE=$RH_db_name -e MYSQL_ROOT_PASSWORD=$RH_db_root_password -e MYSQL_USER=$RH_db_user -e MYSQL_PASSWORD=$RH_db_password -v /share_docker/ralph_dbdata:/var/lib/mysql --network net -p 3306:3306 -h mysql --log-driver syslog --log-opt tag=DOCKER_RH_mysql --log-opt syslog-address=udp://10.20.0.30:514  mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

# get redis image
docker pull redis:3.0

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --network net -p 6379:6379 -h redis --log-driver syslog --log-opt tag=DOCKER_RH_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:3.0

# get inkpy image
docker pull allegro/inkpy:latest

# create inkpy container
docker create --name CTX_inkpy --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e REDIS_HOST=redis -e REDIS_PASSWORD="" -e REDIS_PORT=6379 -e REDIS_DB=0 --network net --log-driver syslog --log-opt tag=DOCKER_RH_inkpy --log-opt syslog-address=udp://10.20.0.30:514 allegro/inkpy:latest

# get ralph image
docker pull allegro/ralph:latest

# create ralph container
docker create --name CTX_ralph --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e DATABASE_NAME=$RH_db_name -e DATABASE_USER=$RH_db_user -e DATABASE_PASSWORD=$RH_db_password -e DATABASE_HOST=mysql -e REDIS_HOST=redis -e REDIS_PASSWORD="" -e REDIS_PORT=6379 -e REDIS_DB=0 -v /share_docker/ralph_media:/var/local/ralph/media -v /share_docker/ralph_static:/usr/share/ralph/static --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_RH_ralph --log-opt syslog-address=udp://10.20.0.30:514 allegro/ralph:latest

# init app
docker exec --rm CTX_ralph /root/init.sh

# start container
docker start CTX_nginx
docker start CTX_mysql 
docker start CTX_redis 
docker start CTX_inkpy 
docker start CTX_ralph 
 
# one shitty line
mkdir -p /share_docker/ralph_media /share_docker/ralph_dbdata /share_docker/ralph_static && chown 1000:1000 -R /share_docker/ralph_media /share_docker/ralph_dbdata /share_docker/ralph_static && docker network create net && docker pull allegro/ralph-static-nginx:latest && docker create --name CTX_nginx --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -v /share_docker/ralph_media:/opt/media --network net -p 80:80 --log-driver syslog --log-opt tag=DOCKER_RH_nginx --log-opt syslog-address=udp://10.20.0.30:514 allegro/ralph-static-nginx:latest && docker pull mysql:5.7 && docker create --name CTX_mysql --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e MYSQL_DATABASE=$RH_db_name -e MYSQL_ROOT_PASSWORD=$RH_db_root_password -e MYSQL_USER=$RH_db_user -e MYSQL_PASSWORD=$RH_db_password -v /share_docker/ralph_dbdata:/var/lib/mysql --network net -p 3306:3306 --log-driver syslog --log-opt tag=DOCKER_RH_mysql --log-opt syslog-address=udp://10.20.0.30:514  mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci && docker pull redis:3.0 && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome --network net -p 6379:6379 -h redis --log-driver syslog --log-opt tag=DOCKER_RH_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:3.0 && docker pull allegro/inkpy:latest && docker create --name CTX_inkpy --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e REDIS_HOST=redis -e REDIS_PASSWORD="" -e REDIS_PORT=6379 -e REDIS_DB=0 --network net --log-driver syslog --log-opt tag=DOCKER_RH_inkpy --log-opt syslog-address=udp://10.20.0.30:514 allegro/inkpy:latest && docker pull allegro/ralph:latest && docker create --name CTX_ralph --restart unless-stopped -e PUID=1000 -e PGID=1000  -e TZ=Europe/Rome -e DATABASE_NAME=$RH_db_name -e DATABASE_USER=$RH_db_user -e DATABASE_PASSWORD=$RH_db_password -e DATABASE_HOST=mysql -e REDIS_HOST=redis -e REDIS_PASSWORD="" -e REDIS_PORT=6379 -e REDIS_DB=0 -v /share_docker/ralph_media:/var/local/ralph/media -v /share_docker/ralph_static:/usr/share/ralph/static --network net -p 8000:8000 --log-driver syslog --log-opt tag=DOCKER_RH_ralph --log-opt syslog-address=udp://10.20.0.30:514 allegro/ralph:latest && docker start CTX_nginx && docker start CTX_mysql && docker start CTX_redis && docker start CTX_inkpy && docker start CTX_ralph
