#!/bin/bash

# setup LAMP on debian 10 lxc

# install apache

apt install apache2 apache2-utils
systemctl enable apache2
chown www-data:www-data /var/www/html/ -R
apache2ctl -t 

systemctl status apache2

# install mysql
apt install mariadb-server mariadb-client
systemctl enable mariadb
mysql_secure_installation

systemctl status mariadb

# install php 7.4
apt -y install lsb-release apt-transport-https ca-certificates
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" |  tee /etc/apt/sources.list.d/php.list
 
apt install -y php7.4 libapache2-mod-php7.4 php7.4-{cli,zip,gd,fpm,json,common,mysql,zip,mbstring,curl,xml,bcmath,imap,ldap,intl}

a2dismod php7.4
a2enmod proxy_fcgi setenvif
a2enconf php7.4-fpm
a2enmod ssl

systemctl restart apache2

# in line cmd
apt -y install apache2 apache2-utils && systemctl enable apache2 && chown www-data:www-data /var/www/html/ -R && apache2ctl -t  && apt install mariadb-server mariadb-client -y && systemctl enable mariadb &&  mysql_secure_installation

# in line cmd
apt -y install lsb-release apt-transport-https ca-certificates && wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" |  tee /etc/apt/sources.list.d/php.list && apt update && apt-get install php7.4 php7.4-{cli,zip,gd,fpm,json,common,mysql,zip,mbstring,curl,xml,bcmath,imap,ldap,intl} -y && a2enmod php7.4 && systemctl restart apache2


