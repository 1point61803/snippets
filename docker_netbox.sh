#!/bin/bash

# create share folder
mkdir -p /share_docker/redis_conf /share_docker/redis_data /share_docker/redis_data_cache /share_docker/postgres_conf /share_docker/postgres_data /share_docker/netbox_conf/{config,reports,script,media}
chown 1000:1000 -R /share_docker/redis_conf /share_docker/redis_data /share_docker/redis_data_cache /share_docker/postgres_conf /share_docker/postgres_data /share_docker/netbox_conf

# get env files for services
wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/redis-cache.env -P /share_docker/redis_conf
wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/redis.env -P /share_docker/redis_conf
wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/postgres.env -P /share_docker/postgres_conf
wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/netbox.env -P /share_docker/netbox_conf

# create tmp network
docker network create net

# get redis image
docker pull redis:7-alpine

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/redis_conf/redis.env -v /share_docker/redis_data:/data --network net -h redis --log-driver syslog --log-opt tag=DOCKER_nt_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:7-alpine redis-server --appendonly yes --requirepass $NT_redis_password

# create redis cache container
docker create --name CTX_redis_cache --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/redis_conf/redis-cache.env -v /share_docker/redis_data_cache:/data --network net -h redis-cache --log-driver syslog --log-opt tag=DOCKER_nt_redis_cache --log-opt syslog-address=udp://10.20.0.30:514 redis:7-alpine redis-server --requirepass $NT_redis_password

# get postgres image
docker pull postgres:15-alpine

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/postgres_conf/postgres.env -v /share_docker/postgres_data:/var/lib/postgresql/data --network net -h postgres --log-driver syslog --log-opt tag=DOCKER_nt_redis_cache --log-opt syslog-address=udp://10.20.0.30:514  postgres:15-alpine

# get netbox image
docker pull netboxcommunity/netbox:latest

# create netbox container
docker create --name CTX_netbox --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/netbox_conf/netbox.env -v /share_docker/netbox_conf/config:/etc/netbox/config -v /share_docker/netbox_conf/reports:/etc/netbox/reports -v /share_docker/netbox_conf/scripts:/etc/netbox/scripts -v /share_docker/netbox_conf/media:/opt/netbox/netbox/media --network net -p 8000:8080 --log-driver syslog --log-opt tag=DOCKER_nt_netbox --log-opt syslog-address=udp://10.20.0.30:514  netboxcommunity/netbox:latest

# get netboxW image
docker pull netboxcommunity/netbox:latest

# create netboxW container
docker create --name CTX_netboxW --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/netbox_conf/netbox.env -v /share_docker/netbox_conf/config:/etc/netbox/config:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/media:/opt/netbox/netbox/media:z --network net --log-driver syslog --log-opt tag=DOCKER_nt_netboxW --log-opt syslog-address=udp://10.20.0.30:514  netboxcommunity/netbox:latest /opt/netbox/venv/bin/python /opt/netbox/netbox/manage.py rqworker

# start container
docker start CTX_redis
docker start CTX_redis_cache
docker start CTX_postgres
docker start CTX_netbox
docker start CTX_netboxW

# one shitty line
mkdir -p /share_docker/redis_conf /share_docker/redis_data /share_docker/redis_data_cache /share_docker/postgres_conf /share_docker/postgres_data /share_docker/netbox_conf/{config,reports,script,media} && chown 1000:1000 -R /share_docker/redis_conf /share_docker/redis_data /share_docker/redis_data_cache /share_docker/postgres_conf /share_docker/postgres_data /share_docker/netbox_conf && wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/redis-cache.env -P /share_docker/redis_conf && wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/redis.env -P /share_docker/redis_conf && wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/postgres.env -P /share_docker/postgres_conf && wget https://raw.githubusercontent.com/netbox-community/netbox-docker/release/env/netbox.env -P /share_docker/netbox_conf && docker network create net && docker pull redis:7-alpine && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/redis_conf/redis.env -v /share_docker/redis_data:/data --network net --log-driver syslog --log-opt tag=DOCKER_nt_redis --log-opt syslog-address=udp://10.20.0.30:514 redis:7-alpine redis-server --appendonly yes --requirepass $NT_redis_password && docker create --name CTX_redis_cache --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/redis_conf/redis-cache.env -v /share_docker/redis_data_cache:/data --network net --log-driver syslog --log-opt tag=DOCKER_nt_redis_cache --log-opt syslog-address=udp://10.20.0.30:514 redis:7-alpine redis-server --requirepass $NT_redis_password && docker pull postgres:15-alpine && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/postgres_conf/postgres.env -v /share_docker/postgres_data:/var/lib/postgresql/data --network net --log-driver syslog --log-opt tag=DOCKER_nt_redis_cache --log-opt syslog-address=udp://10.20.0.30:514  postgres:15-alpine && docker pull netboxcommunity/netbox:latest && docker create --name CTX_netbox --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/netbox_conf/netbox.env -v /share_docker/netbox_conf/config:/etc/netbox/config:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/media:/opt/netbox/netbox/media:z --network net -p 8000:8080 --log-driver syslog --log-opt tag=DOCKER_nt_netbox --log-opt syslog-address=udp://10.20.0.30:514  netboxcommunity/netbox:latest && docker pull netboxcommunity/netbox:latest && docker create --name CTX_netboxW --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/netbox_conf/netbox.env -v /share_docker/netbox_conf/config:/etc/netbox/config:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/reports:/etc/netbox/reports:z,ro -v /share_docker/netbox_conf/media:/opt/netbox/netbox/media:z --network net --log-driver syslog --log-opt tag=DOCKER_nt_netboxW --log-opt syslog-address=udp://10.20.0.30:514  netboxcommunity/netbox:latest /opt/netbox/venv/bin/python /opt/netbox/netbox/manage.py rqworker && docker start CTX_redis && docker start CTX_redis_cache && docker start CTX_postgres && docker start CTX_netbox && docker start CTX_netboxW
