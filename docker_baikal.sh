#!/bin/bash

# create share folder
mkdir -p /share_docker/baikal_conf /share_docker/baikal_data
chown 1000:1000 -R /share_docker/baikal_conf /share_docker/baikal_data

# create tmp network
docker network create net

# get baikal image
docker pull  ckulka/baikal:nginx

# create baikal container
docker create --name CTX_baikal --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /share_docker/baikal_conf:/var/www/baikal/config -v /share_docker/baikal_data:/var/www/baikal/Specific --network net -h baikal -p 80:80 --log-driver syslog --log-opt tag=DOCKER_baikal --log-opt syslog-address=udp://10.20.0.30:514 ckulka/baikal:nginx

# start container
docker start CTX_baikal

# one shitty line
mkdir -p /share_docker/baikal_conf /share_docker/baikal_data && chown 1000:1000 -R /share_docker/baikal_conf /share_docker/baikal_data && docker network create net && docker pull  ckulka/baikal:nginx && docker create --name CTX_baikal --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -v /share_docker/baikal_conf:/var/www/baikal/config -v /share_docker/baikal_data:/var/www/baikal/Specific --network net -h baikal -p 80:80 --log-driver syslog --log-opt tag=DOCKER_baikal --log-opt syslog-address=udp://10.20.0.30:514 ckulka/baikal:nginx && docker start CTX_baikal
