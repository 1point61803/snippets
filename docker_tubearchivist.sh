#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/redis_data /share_docker/elastisearch_data /share_docker/ta-data /share_docker/ta-cache
chown 1000:1000 -R /share_docker/*

# create tmp network
docker network create net

# get redis image
docker pull redislabs/rejson:latest

# create redis container
docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h redis -p 6379:6379 -v /share_docker/redis_data:/data --log-driver syslog --log-opt tag=DOCKER_ta_redis --log-opt syslog-address=udp://10.20.0.30:514 redislabs/rejson:latest

# start redis container
docker start CTX_redis

# get elasticsearch image
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.16.2

# create elasticsearch container
docker create --name CTX_elasticsearch --restart unless-stopped -e PUID=1000 -e PGID=1000 -e xpack.security.enabled=true -e ELASTIC_PASSWORD=$TA_elasticsearchpassword -e discovery.type=single-node -e ES_JAVA_OPTS="-Xms512m -Xmx512m" --network net -h elasticsearch -p 9200:9200 -v /share_docker/elastisearch_data:/usr/share/elasticsearch/data --log-driver syslog --log-opt tag=DOCKER_ta_leastic --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch:7.16.2

# start elastichsearh container
docker start CTX_elasticsearch

# get tubearchivist image
docker pull bbilly1/tubearchivist:latest

# create tubearchivist container
docker create --name CTX_tubearchivist --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ES_URL=http://elasticsearch:9200 -e REDIS_HOST=redis -e HOST_UID=1000 -e HOST_GID=1000 -e TA_USERNAME=$TA_user -e TA_PASSWORD=$TA_password -e ELASTIC_PASSWORD=$TA_elasticsearchpassword -e TZ=Europe/Rome --network net -h tubearchivist -p 8000:8000 -v /share_docker/ta-data:/youtube -v /share_docker/ta-cache:/cache --log-driver syslog --log-opt tag=DOCKER_ta_tubearchivist --log-opt syslog-address=udp://10.20.0.30:514 bbilly1/tubearchivist:latest

# start tubearchivist container
docker start CTX_tubearchivist

# one shitty line
mkdir -p /share_docker/redis_data /share_docker/elastisearch_data /share_docker/ta-data /share_docker/ta-cache && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull redislabs/rejson:latest && docker create --name CTX_redis --restart unless-stopped -e PUID=1000 -e PGID=1000 --network net -h redis -p 6379:6379 -v /share_docker/redis_data:/data --log-driver syslog --log-opt tag=DOCKER_ta_redis --log-opt syslog-address=udp://10.20.0.30:514 redislabs/rejson:latest && docker start CTX_redis && docker pull docker.elastic.co/elasticsearch/elasticsearch:7.16.2 && docker create --name CTX_elasticsearch --restart unless-stopped -e PUID=1000 -e PGID=1000 -e xpack.security.enabled=true -e ELASTIC_PASSWORD=$TA_elasticsearchpassword -e discovery.type=single-node -e ES_JAVA_OPTS="-Xms512m -Xmx512m" --network net -h elasticsearch -p 9200:9200 -v /share_docker/elastisearch_data:/usr/share/elasticsearch/data --log-driver syslog --log-opt tag=DOCKER_ta_leastic --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch:7.16.2 && docker start CTX_elasticsearch && docker pull bbilly1/tubearchivist:latest && docker create --name CTX_tubearchivist --restart unless-stopped -e PUID=1000 -e PGID=1000 -e ES_URL=http://elasticsearch:9200 -e REDIS_HOST=redis -e HOST_UID=1000 -e HOST_GID=1000 -e TA_USERNAME=$TA_user -e TA_PASSWORD=$TA_password -e ELASTIC_PASSWORD=$TA_elasticsearchpassword -e TZ=Europe/Rome --network net -h tubearchivist -p 8000:8000 -v /share_docker/ta-data:/youtube -v /share_docker/ta-cache:/cache --log-driver syslog --log-opt tag=DOCKER_ta_tubearchivist --log-opt syslog-address=udp://10.20.0.30:514 bbilly1/tubearchivist:latest && docker start CTX_tubearchivist
