#!/bin/bash

# create persistent folder
mkdir -p /share_docker/elasticsearch_data /share_docker/cassandra_data /share_docker/minio_data
chown 1000:1000 -R /share_docker/*
  
# create tmp network
docker network create net

# get elasticsearch image
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.17.9

# create elasticsearch container
docker create --name CTX_elasticsearch --restart unless-stopped --memory-reservation=512m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e discovery.type=single-node -e xpack.security.enabled=false -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net -h elasticsearch -p 9200:9200 --log-driver syslog --log-opt tag=DOCKER_TH_elastisearch --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch:7.17.9

# get cassandra image
docker pull cassandra:4

# create cassandra container
docker create --name CTX_cassandra --restart unless-stopped --memory-reservation=1024m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e CASSANDRA_CLUSTER_NAME=TheHive -v /share_docker/cassandra_data:/var/lib/cassandra --network net -h cassandra -p 9042:9042 --log-driver syslog --log-opt tag=DOCKER_TH_cassandra --log-opt syslog-address=udp://10.20.0.30:514 cassandra:4

# get minio image
docker pull quay.io/minio/minio

# create minio container
docker create --name CTX_minio --restart unless-stopped --memory-reservation=512m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e NODE_ENV=Production_hualian -v /share_docker/minio_data:/data --network net -h minio -p 9090:9090 -p 62992:62992 --log-driver syslog --log-opt tag=DOCKER_TH_minio --log-opt syslog-address=udp://10.20.0.30:514 quay.io/minio/minio  minio server /data --console-address :9090

# get cortex image
docker pull thehiveproject/cortex:3.1.7

# create cortex container
docker create --name CTX_cortex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e job_directory=/tmp/cortex-jobs -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/cortex-jobs:/tmp/cortex-jobs --network net -h cortex -p 9001:9001 --log-driver syslog --log-opt tag=DOCKER_TH_cortex --log-opt syslog-address=udp://10.20.0.30:514 thehiveproject/cortex:3.1.7

# get thehive image
docker pull strangebee/thehive:5.2

# create thehive container
docker create --name CTX_thehive --restart unless-stopped --memory-reservation=1500m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e JVM_OPTS="-Xms1024M -Xmx1024M" --network net -h thhive -p 9000:9000 --log-driver syslog --log-opt tag=DOCKER_TH_thehive --log-opt syslog-address=udp://10.20.0.30:514 strangebee/thehive:5.2 --secret mySecretForTheHive --cql-hostnames cassandra --index-backend elasticsearch --es-hostnames elasticsearch --s3-endpoint http://minio:9000 --s3-access-key $TH_minio_admin --s3-secret-key $TH_minio_adminPpwd --s3-bucket thehive --s3-use-path-access-style --cortex-hostnames cortex --cortex-keys <cortex_api_key>

# start container
docker start CTX_elasticsearch 
docker start CTX_cassandra
docker start CTX_minio
docker start CTX_cortex
docker start CTX_thehive

# one shitty line
mkdir -p /share_docker/elasticsearch_data /share_docker/cassandra_data /share_docker/minio_data && chown 1000:1000 -R /share_docker/* && docker network create net && docker pull docker.elastic.co/elasticsearch/elasticsearch:7.17.9 && docker create --name CTX_elasticsearch --restart unless-stopped --memory-reservation=512m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e discovery.type=single-node -e xpack.security.enabled=false -v /share_docker/elasticsearch_data:/usr/share/elasticsearch/data --network net -h elasticsearch -p 9200:9200 --log-driver syslog --log-opt tag=DOCKER_TH_elastisearch --log-opt syslog-address=udp://10.20.0.30:514 docker.elastic.co/elasticsearch/elasticsearch:7.17.9 && docker pull cassandra:4 && docker create --name CTX_cassandra --restart unless-stopped --memory-reservation=1024m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e CASSANDRA_CLUSTER_NAME=TheHive -v /share_docker/cassandra_data:/var/lib/cassandra --network net -h cassandra -p 9042:9042 --log-driver syslog --log-opt tag=DOCKER_TH_cassandra --log-opt syslog-address=udp://10.20.0.30:514 cassandra:4 && docker pull quay.io/minio/minio && docker create --name CTX_minio --restart unless-stopped --memory-reservation=512m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e NODE_ENV=Production_hualian -v /share_docker/minio_data:/data --network net -h minio -p 9090:9090 -p 62992:62992 --log-driver syslog --log-opt tag=DOCKER_TH_minio --log-opt syslog-address=udp://10.20.0.30:514 quay.io/minio/minio  minio server /data --console-address :9090 && docker pull thehiveproject/cortex:3.1.7 && docker create --name CTX_cortex --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e job_directory=/tmp/cortex-jobs -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/cortex-jobs:/tmp/cortex-jobs --network net -h cortex -p 9001:9001 --log-driver syslog --log-opt tag=DOCKER_TH_cortex --log-opt syslog-address=udp://10.20.0.30:514 thehiveproject/cortex:3.1.7 && docker pull strangebee/thehive:5.2 && echo "GET API KEY FROM CORTEX CTX" && docker create --name CTX_thehive --restart unless-stopped --memory-reservation=1500m -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e JVM_OPTS="-Xms1024M -Xmx1024M" --network net -h thhive -p 9000:9000 --log-driver syslog --log-opt tag=DOCKER_TH_thehive --log-opt syslog-address=udp://10.20.0.30:514 strangebee/thehive:5.2 --secret mySecretForTheHive --cql-hostnames cassandra --index-backend elasticsearch --es-hostnames elasticsearch --s3-endpoint http://minio:9000 --s3-access-key $TH_minio_admin --s3-secret-key $TH_minio_adminPpwd --s3-bucket thehive --s3-use-path-access-style --cortex-hostnames cortex --cortex-keys <cortex_api_key> && docker start CTX_elasticsearch  && docker start CTX_cassandra && docker start CTX_minio && docker start CTX_cortex && docker start CTX_thehive
