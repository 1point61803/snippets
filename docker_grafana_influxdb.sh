#!/bin/bash

# create share folder
mkdir -p /share_docker/grafana_data /share_docker/grafana_config /share_docker/influxdb_data /share_docker/influxdb_conf
touch /share_docker/grafana_config/grafana.ini
chown 472:root -R /share_docker/grafana_data /share_docker/grafana_config 
chown 1000:1000 -R /share_docker/influxdb_data /share_docker/influxdb_conf

# create tmp network
docker network create net

# get influxdb image
docker pull influxdb

# create influxdb container
docker create --name CTX_influxdb --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/influxdb_data:/var/lib/influxdb2 -v /share_docker/influxdb_conf:/etc/influxdb2  --network net -p 8086:8086  --log-driver syslog --log-opt tag=DOCKER_influxdb --log-opt syslog-address=udp://10.20.0.30:514 influxdb

# get grafana image
docker pull grafana/grafana

# create grafana container
docker create --name CTX_grafana --restart unless-stopped -e PUID=472 -e PGID=0 -e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource"  -v /share_docker/grafana_data:/var/lib/grafana -v /share_docker/grafana_config:/etc/grafana --network net -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_grafana --log-opt syslog-address=udp://10.20.0.30:514 grafana/grafana

# start container
docker start CTX_influxdb
docker start CTX_grafana

# one shitty line
mkdir -p /share_docker/grafana_data /share_docker/grafana_config /share_docker/influxdb_data && touch /share_docker/grafana_config/grafana.ini && chown 472:root -R /share_docker/grafana_data /share_docker/grafana_config && chown 1000:1000 -R /share_docker/influxdb_data && docker network create net && docker pull influxdb && docker create --name CTX_influxdb --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/influxdb_data:/var/lib/influxdb2 --network net -p 8086:8086  --log-driver syslog --log-opt tag=DOCKER_influxdb --log-opt syslog-address=udp://10.20.0.30:514 influxdb && docker pull grafana/grafana && docker create --name CTX_grafana --restart unless-stopped -e PUID=472 -e PGID=0 -e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource"  -v /share_docker/grafana_data:/var/lib/grafana -v /share_docker/grafana_config:/etc/grafana --network net -p 3000:3000  --log-driver syslog --log-opt tag=DOCKER_grafana --log-opt syslog-address=udp://10.20.0.30:514 grafana/grafana && docker start CTX_influxdb && docker start CTX_grafana
