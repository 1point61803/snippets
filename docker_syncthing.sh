#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/syncthing
chown 1000:1000 -R /share_docker/jelly-config/ 

# get syncthing image
docker pull syncthing/syncthing

# create syncthing container
docker create --name CTX_syncthing --restart unless-stopped -e PUID=1000 -e PGID=1000 --network=host -h syncthing -p 8384:8384 -p 22000:22000/tcp -p 22000:22000/udp -p 21027:21027/udp  --mount type=bind,source=/mnt/misc/sync,target=/var/syncthing --log-driver syslog --log-opt tag=DOCKER_syncthing --log-opt syslog-address=udp://10.20.0.30:514 syncthing/syncthing:latest

# start syncthing container
docker start CTX_syncthing

# one shitty line
mkdir -p /share_docker/syncthing && chown 1000:1000 -R /share_docker/jelly-config/ && docker pull syncthing/syncthing && docker create --name CTX_syncthing --restart unless-stopped -e PUID=1000 -e PGID=1000 --network=host -h syncthing -p 8384:8384 -p 22000:22000/tcp -p 22000:22000/udp -p 21027:21027/udp  --mount type=bind,source=/mnt/misc/sync,target=/var/syncthing --log-driver syslog --log-opt tag=DOCKER_syncthing --log-opt syslog-address=udp://10.20.0.30:514 syncthing/syncthing:latest && docker start CTX_syncthing
