#!/bin/bash

# create share folder
mkdir -p /share_docker/g_data

# create share folder
chown 1000:1000 -R /share_docker/g_data

# create tmp network
docker network create net

# get mldonkey image
docker pull ghcr.io/beryju/gravity:stable

# create mldonkey container
docker create --name CTX_gravity --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e LOG_LEVEL=info -e  BOOTSTRAP_ROLES="dns;api;etcd;discovery;monitoring;tsdb" -v /share_docker/g_data:/data:rw --network net -p 8008:8008 -h gravity-dns --log-driver syslog --log-opt tag=DOCKER_gravity --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/beryju/gravity:stable

# start container
docker start CTX_mldonkey

# one shitty line
mkdir -p /share_docker/g_data && chown 1000:1000 -R /share_docker/g_data && docker network create net && docker pull ghcr.io/beryju/gravity:stable && docker create --name CTX_gravity --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e LOG_LEVEL=info -e  BOOTSTRAP_ROLES="dns;api;etcd;discovery;monitoring;tsdb" -v /share_docker/g_data:/data:rw --network net -p 8008:8008 -h gravity-dns --log-driver syslog --log-opt tag=DOCKER_gravity --log-opt syslog-address=udp://10.20.0.30:514 ghcr.io/beryju/gravity:stable && docker start CTX_gravity
