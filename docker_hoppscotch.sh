#!/bin/bash

# include secret
source SECRET

# create persistent folder
mkdir -p /share_docker/hs_env
chown 1000:1000 -R /share_docker/*

# create env file
echo -e "DATABASE_URL=postgresql://$HS_pg_user:$HS_pg_password@postgres:5432/$HS_pg_db \nJWT_SECRET=$HS_secret\nTOKEN_SALT_COMPLEXITY=10\nMAGIC_LINK_TOKEN_VALIDITY=3\nREFRESH_TOKEN_VALIDITY=604800000\nACCESS_TOKEN_VALIDITY=86400000\nSESSION_SECRET=$HS_session_secret\nREDIRECT_URL=http://hs-frontend:3000\nWHITELISTED_ORIGINS=http://hs-backend:3170,http://hs-frontend:3000,http://hs-admin:3100\nVITE_ALLOWED_AUTH_PROVIDERS=GITHUB\nRATE_LIMIT_TTL=60\nRATE_LIMIT_MAX=100\nVITE_BASE_URL=http://hs-frontend:3000\nVITE_SHORTCODE_BASE_URL=http://hs-frontend:3000\nVITE_ADMIN_URL=http://hs-admin:3100\nVITE_BACKEND_GQL_URL=http://hs-backend:3170/graphql\nVITE_BACKEND_WS_URL=wss://hs-frontend:3170/graphql\nVITE_BACKEND_API_URL=http://hs-backend:3170/v1\nVITE_APP_TOS_LINK=https://docs.hoppscotch.io/support/terms\nVITE_APP_PRIVACY_POLICY_LINK=https://docs.hoppscotch.io/support/privacy\nENABLE_SUBPATH_BASED_ACCESS=false" > /share_docker/hs_env/prod.env

# create tmp network
docker network create net

# get postgres image
docker pull postgres:13

# create postgres container
docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_DB=$HS_pg_db -e POSTGRES_USER=$HS_pg_user -e POSTGRES_PASSWORD=$HS_pg_password -v /share_docker/PG_data:/var/lib/postgresql/data --network net -h postgres -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_hs_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13

# get hoppscotch frontend image
docker pull hoppscotch/hoppscotch-frontend

# create hoppscotch frontend container
docker create --name CTX_hs_frontend --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-frontend -p 3000:3000 --log-driver syslog --log-opt tag=DOCKER_hs_frontend --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-frontend

# get hoppscotch backend image
docker pull hoppscotch/hoppscotch-backend

# create hoppscotch backend container
docker create --name CTX_hs_backend --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-backend -p 3170:3170 --log-driver syslog --log-opt tag=DOCKER_hs_backend --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-backend

# get hoppscotch admin image
docker pull hoppscotch/hoppscotch-admin

# create hoppscotch admin container
docker create --name CTX_hs_admin --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-admin -p 3100:3100 --log-driver syslog --log-opt tag=DOCKER_hs_admin --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-admin


# start container
docker start CTX_postgres
docker start CTX_hs_frontend
docker start CTX_hs_backend
docker start CTX_hs_admin

# one shitty line
mkdir -p /share_docker/hs_env && chown 1000:1000 -R /share_docker/* && echo -e "DATABASE_URL=postgresql://$HS_pg_user:$HS_pg_password@postgres:5432/$HS_pg_db \nJWT_SECRET=$HS_secret\nTOKEN_SALT_COMPLEXITY=10\nMAGIC_LINK_TOKEN_VALIDITY=3\nREFRESH_TOKEN_VALIDITY=604800000\nACCESS_TOKEN_VALIDITY=86400000\nSESSION_SECRET=$HS_session_secret\nREDIRECT_URL=http://hs-frontend:3000\nWHITELISTED_ORIGINS=http://hs-backend:3170,http://hs-frontend:3000,http://hs-admin:3100\nVITE_ALLOWED_AUTH_PROVIDERS=GITHUB\nRATE_LIMIT_TTL=60\nRATE_LIMIT_MAX=100\nVITE_BASE_URL=http://hs-frontend:3000\nVITE_SHORTCODE_BASE_URL=http://hs-frontend:3000\nVITE_ADMIN_URL=http://hs-admin:3100\nVITE_BACKEND_GQL_URL=http://hs-backend:3170/graphql\nVITE_BACKEND_WS_URL=wss://hs-frontend:3170/graphql\nVITE_BACKEND_API_URL=http://hs-backend:3170/v1\nVITE_APP_TOS_LINK=https://docs.hoppscotch.io/support/terms\nVITE_APP_PRIVACY_POLICY_LINK=https://docs.hoppscotch.io/support/privacy\nENABLE_SUBPATH_BASED_ACCESS=false" > /share_docker/hs_env/prod.env && docker network create net && docker pull postgres:13 && docker create --name CTX_postgres --restart unless-stopped -e PUID=1000 -e PGID=1000 -e POSTGRES_DB=$HS_pg_db -e POSTGRES_USER=$HS_pg_user -e POSTGRES_PASSWORD=$HS_pg_password -v /share_docker/PG_data:/var/lib/postgresql/data --network net -h postgres -p 5432:5432 --log-driver syslog --log-opt tag=DOCKER_hs_postgres --log-opt syslog-address=udp://10.20.0.30:514 postgres:13 && docker pull hoppscotch/hoppscotch-frontend && docker create --name CTX_hs_frontend --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-frontend -p 3000:3000 --log-driver syslog --log-opt tag=DOCKER_hs_frontend --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-frontend && docker pull hoppscotch/hoppscotch-backend && docker create --name CTX_hs_backend --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-backend -p 3170:3170 --log-driver syslog --log-opt tag=DOCKER_hs_backend --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-backend && docker pull hoppscotch/hoppscotch-admin && docker create --name CTX_hs_admin --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome --env-file /share_docker/hs_env/prod.env --network net -h hs-admin -p 3100:3100 --log-driver syslog --log-opt tag=DOCKER_hs_admin --log-opt syslog-address=udp://10.20.0.30:514 hoppscotch/hoppscotch-admin && docker start CTX_postgres && docker start CTX_hs_frontend && docker start CTX_hs_backend && docker start CTX_hs_admin
