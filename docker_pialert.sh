#!/bin/bash

# create share folder
mkdir -p /share_docker/pialert_conf/ /share_docker/pialert_data /share_docker/pialert_log
chown 1000:1000 -R /share_docker/pialert_conf/ /share_docker/pialert_data /share_docker/pialert_log

# get image
docker pull jokobsk/pi.alert:latest

# create container
docker create --name CTX_pialert --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e PORT=20211 -e HOST_USER_ID=1000 -e HOST_USER_GID=1000 --network host -v /share_docker/pialert_conf:/home/pi/pialert/config -v /share_docker/pialert_data:/home/pi/pialert/db -v /share_docker/pialert_log:/home/pi/pialert/front/log --log-driver syslog --log-opt tag=DOCKER_pialert --log-opt syslog-address=udp://10.20.0.30:514 jokobsk/pi.alert:latest

# start container
docker start CTX_pialert

# one shitty line
mkdir -p /share_docker/pialert_conf /share_docker/pialert_data /share_docker/pialert_log && chown 1000:1000 -R /share_docker/pialert_conf/ /share_docker/pialert_data /share_docker/pialert_log && docker pull jokobsk/pi.alert:latest && docker create --name CTX_pialert --restart unless-stopped -e PUID=1000 -e PGID=1000 -e TZ=Europe/Rome -e PORT=20211 -e HOST_USER_ID=1000 -e HOST_USER_GID=1000 --network host -v /share_docker/pialert_conf:/home/pi/pialert/config -v /share_docker/pialert_data:/home/pi/pialert/db -v /share_docker/pialert_log:/home/pi/pialert/front/log --log-driver syslog --log-opt tag=DOCKER_pialert --log-opt syslog-address=udp://10.20.0.30:514 jokobsk/pi.alert:latest && docker start CTX_pialert
