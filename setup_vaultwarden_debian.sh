#!/bin/bash
# prepare system
apt install  docker git patch curl

# prepare dpkg pkg
git clone https://github.com/greizgh/vaultwarden-debian.git
cd vaultwarden-debian
./build.sh -r 1.22.2 -o bulleyes
# install it
dpkg -i vaultwarden-bullseye-1.22.2-sqlite-amd64.deb 

# create cert for https traffic
openssl genpkey -algorithm RSA -aes128 -out private-ca.key -outform PEM -pkeyopt rsa_keygen_bits:2048
openssl req -x509 -new -nodes -sha256 -days 3650 -key private-ca.key -out self-signed-ca-cert.crt
openssl genpkey -algorithm RSA -out bitwarden.key -outform PEM -pkeyopt rsa_keygen_bits:2048
openssl req -new -key bitwarden.key -out bitwarden.csr
vim bitwarden.ext
## bitwarden.ext
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1:localhost.
DNS.2 = passwd.lan
DNS.3 = passwd.dns.lan
##
openssl x509 -req -in bitwarden.csr -CA self-signed-ca-cert.crt -CAkey private-ca.key -CAcreateserial -out bitwarden.crt -days 365 -sha256 -extfile bitwarden.ext
#convert to pem format
openssl x509 -in bitwarden.crt -out vault.pem -outform PEM
openssl rsa -in bitwarden.key -out vault_key.pem -outform PEM

# move to the right diretory and change permission and owner
cp -R /path/to/cert/folder  /var/lib/vaultwarden/
chown vaultwarden:nogroup -R /var/lib/vaultwarden/cert/folder

# modificate the config file to add new certs
vim /etc/vaultwarden/config.env
## config.env
...
ROCKET_TLS={certs="/var/lib/vaultwarden/something/vault.pem",key="/var/lib/vaultwarden/something/vault_key.pem"}
...
##

