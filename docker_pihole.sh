#!/bin/bash

# create share folder
mkdir -p /share_docker/etc-pihole/ /share_docker/etc-dnsmasq.d/
chown 1000:1000 -R /share_docker/etc-pihole/ /share_docker/etc-dnsmasq.d/

# get image
docker pull pihole/pihole:latest 

# create container
docker create --name CTX_pihole --restart unless-stopped -e PUID=1000 -e PGID=1000 -p 53:53/tcp -p 53:53/udp -p 80:80 -v /share_docker/etc-pihole/:/etc/pihole/ -v /share_docker/etc-dnsmasq.d/:/etc/dnsmasq.d/ --dns=127.0.0.1 --dns=10.20.0.10 --hostname pihole.homelab.local -e VIRTUAL_HOST="pi.hole" -e PROXY_LOCATION="pi.hole" --log-driver syslog --log-opt tag=DOCKER_pihole --log-opt syslog-address=udp://10.20.0.30:514 pihole/pihole:latest

# start container
docker start CTX_pihole

# one shitty line
mkdir -p /share_docker/etc-pihole/ /share_docker/etc-dnsmasq.d/ && chown 1000:1000 -R /share_docker/etc-pihole/ /share_docker/etc-dnsmasq.d/ && docker pull pihole/pihole && docker create --name CTX_pihole --restart unless-stopped -e PUID=1000 -e PGID=1000 -p 53:53/tcp -p 53:53/udp -p 80:80 -v /share_docker/etc-pihole/:/etc/pihole/ -v /share_docker/etc-dnsmasq.d/:/etc/dnsmasq.d/ --dns=127.0.0.1 --dns=10.20.0.10 --hostname pi.hole -e VIRTUAL_HOST="pi.hole" -e PROXY_LOCATION="pi.hole" --log-driver syslog --log-opt tag=DOCKER_pihole --log-opt syslog-address=udp://10.20.0.30:514 pihole/pihole:latest && docker start CTX_pihole
