#!/bin/bash

# setup rsyslog with this setting:
#ruleset(name="remote"){
#    # https://www.rsyslog.com/doc/v8-stable/configuration/modules/omfwd.html
#    # https://grafana.com/docs/loki/latest/clients/promtail/scraping/#rsyslog-output-configuration
#  action(type="omfwd" Target="localhost" Port="1514" Protocol="tcp" Template="RSYSLOG_SyslogProtocol23Format" TCP_Framing="octet-counted")
#}
## https://www.rsyslog.com/doc/v8-stable/configuration/modules/imudp.html
#module(load="imudp")
#input(type="imudp" port="514" ruleset="remote")
## https://www.rsyslog.com/doc/v8-stable/configuration/modules/imtcp.html
#module(load="imtcp")
#input(type="imtcp" port="514" ruleset="remote")

# create share folder
mkdir -p /share_docker/promtail_conf /share_docker/loki_conf
wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/cmd/loki/loki-local-config.yaml -O /share_docker/loki_conf/loki-config.yaml
wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/clients/cmd/promtail/promtail-docker-config.yaml -O /share_docker/promtail_conf/promtail-config.yaml
chown 1000:1000 -R /share_docker/promtail_conf /share_docker/loki_conf

# create tmp network
docker network create net --subnet=172.18.0.0/24

# get promtail image
docker pull grafana/promtail:2.6.1

# create promtail container
docker create --name CTX_promtail --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/promtail_conf:/mnt/config -v /var/log:/var/log --network net --ip 172.18.0.10 -p 1514:1514 --log-driver syslog --log-opt tag=DOCKER_promtail --log-opt syslog-address=udp://10.20.0.30:514 grafana/promtail:2.6.1 -config.file=/mnt/config/promtail_rsyslog.yaml

# get adminer image
docker pull grafana/loki:2.6.1

# create adminer cotainer
docker create --name CTX_loki --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/loki_conf:/mnt/config --network net --ip 172.18.0.20 -p 3100:3100 --log-driver syslog --log-opt tag=DOCKER_loki --log-opt syslog-address=udp://10.20.0.30:514 grafana/loki:2.6.1 -config.file=/mnt/config/loki-config.yaml

# start container
docker start CTX_promtail
docker start CTX_loki

# one shitty line
mkdir -p /share_docker/promtail_conf /share_docker/loki_conf && wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/cmd/loki/loki-local-config.yaml -O /share_docker/ loki_conf/loki-config.yaml && wget https://raw.githubusercontent.com/grafana/loki/v2.6.1/clients/cmd/promtail/promtail-docker-config.yaml -O /share_docker/promtail_conf/promtail-config.yaml && chown 1000:1000 -R /share_docker/promtail_conf /share_docker/loki_conf && docker network create net --subnet=172.18.0.0/24 && docker pull grafana/promtail:2.6.1 && docker create --name CTX_promtail --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/promtail_conf:/mnt/config -v /var/log:/var/log --network net --ip 172.18.0.10 -p 1514:1514 --log-driver syslog --log-opt tag=DOCKER_promtail --log-opt syslog-address=udp://10.20.0.30:514 grafana/promtail:2.6.1 -config.file=/mnt/config/promtail_rsyslog.yaml && docker pull grafana/loki:2.6.1 && docker create --name CTX_loki --restart unless-stopped -e PUID=1000 -e PGID=1000 -v /share_docker/loki_conf:/mnt/config --network net --ip 172.18.0.20 -p 3100:3100 --log-driver syslog --log-opt tag=DOCKER_loki --log-opt syslog-address=udp://10.20.0.30:514 grafana/loki:2.6.1 -config.file=/mnt/config/loki-config.yaml && docker start CTX_promtail && docker start CTX_loki
