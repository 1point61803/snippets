#!/bin/bash

# get papermerge image
docker pull mauricenino/dashdot

# create papermerge container
docker create --name CTX_dashdot --privileged --restart unless-stopped -e PUID=1000 -e PGID=1000  -p 3001:3001  --log-driver syslog --log-opt tag=DOCKER_dashdot --log-opt syslog-address=udp://10.20.0.30:514 mauricenino/dashdot

# start container
docker start CTX_dashdot

# one shitty line
docker pull mauricenino/dashdot && docker create --name CTX_dashdot --privileged --restart unless-stopped -e PUID=1000 -e PGID=1000  -p 3001:3001  --log-driver syslog --log-opt tag=DOCKER_dashdot --log-opt syslog-address=udp://10.20.0.30:514 mauricenino/dashdot && docker start CTX_dashdot
