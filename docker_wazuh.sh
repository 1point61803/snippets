#!/bin/bash

# include secret
source SECRET

# create share folder
mkdir -p /share_docker/watchtower /share_docker/wazuh_api_configuration /share_docker/wazuh_etc /share_docker/wazuh_logs /share_docker/wazuh_queue /share_docker/wazuh_var_multigroups /share_docker/wazuh_integrations /share_docker/wazuh_active_response /share_docker/wazuh_agentless /share_docker/wazuh_wodles /share_docker/filebeat_etc /share_docker/filebeat_var /share_docker/config/wazuh_indexer_ssl_certs/ /share_docker/config/wazuh_cluster/ /share_docker/wazuh-indexer-data
chown 1000:1000 -R /share_docker/*

# create file env
echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env

# create tmp network
docker network create net

# get watchtower image
docker pull containrrr/watchtower

# create watchtowr container
docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower

# get wazuh certs image
docker pull wazuh/wazuh-certs-generator:0.0.2

# copy folder config into the /share_docker/
git clone https://github.com/wazuh/wazuh-docker.git -b v4.9.2
cp -r wazuh-docker/single-node/config /share_docker/
rm -rf wwazuh-docker
chown 1000:1000 -R /share_docker/*

# create wazuh certs container 
docker create --name CTX_w_cert_genrator --restart unless-stopped -v /share_docker/config/wazuh_indexer_ssl_certs/:/certificates/ -v /share_docker/config/certs.yml:/config/certs.yml --network net -h certs-generator-wazuh --log-driver syslog --log-opt tag=DOCKER_certs_gen_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-certs-generator:0.0.2

# get wazuh manager image
docker pull wazuh/wazuh-manager:4.5.0

# create wazuh manager container
docker create --name CTX_w_manager --restart unless-stopped -e INDEXER_URL=$WZ_indexer_url -e INDEXER_USERNAME=$WZ_indexer_user -e INDEXER_PASSWORD=$WZ_indexer_password -e FILEBEAT_SSL_VERIFICATION_MODE=$WZ_ssl_ver -e SSL_CERTIFICATE_AUTHORITIES=$WZ_ssl_cert_auth -e SSL_CERTIFICATE=$WZ_ssl_crt -e SSL_KEY=$WZ_key -e API_USERNAME=$WZ_api_username -e API_PASSWORD=$WZ_api_password -v /share_docker/wazuh_api_configuration:/var/ossec/api/configuration -v /share_docker/wazuh_etc:/var/ossec/etc -v /share_docker/wazuh_logs:/var/ossec/logs -v /share_docker/wazuh_queue:/var/ossec/queue -v /share_docker/wazuh_var_multigroups:/var/ossec/var/multigroups -v /share_docker/wazuh_integrations:/var/ossec/integrations -v /share_docker/wazuh_active_response:/var/ossec/active-response/bin -v /share_docker/wazuh_agentless:/var/ossec/agentless -v /share_docker/wazuh_wodles:/var/ossec/wodles -v /share_docker/filebeat_etc:/etc/filebeat -v /share_docker/filebeat_var:/var/lib/filebeat -v /share_docker/config/wazuh_indexer_ssl_certs/:/etc/ssl -v /share_docker/config/wazuh_cluster/wazuh_manager.conf:/wazuh-config-mount/etc/ossec.conf --network net -h wazuh.manager -p 1514:1514 -p 1515:1515 -p 514:514/udp -p 55000:55000 wazuh/wazuh-manager:4.5.0

# get wazuh indexer image
docker pull wazuh/wazuh-indexer:4.5.0

# create wazuh indexer container
docker create --name CTX_w_indexer --restart unless-stopped --ulimit nofile=65536:65536 -e OPENSEARCH_JAVA_OPTS="$WZ_java_opt" -v /share_docker/wazuh-indexer-data:/var/lib/wazuh-indexer -v /share_docker/config/wazuh_indexer_ssl_certs/root-ca.pem:/usr/share/wazuh-indexer/config/certs/root-ca.pem -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.indexer-key.pem:/usr/share/wazuh-indexer/config/certs/wazuh.indexer.key -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.indexer.pem:/usr/share/wazuh-indexer/config/certs/wazuh.indexer.pem -v /share_docker/config/wazuh_indexer_ssl_certs/admin.pem:/usr/share/wazuh-indexer/config/certs/admin.pem -v /share_docker/config/wazuh_indexer_ssl_certs/admin-key.pem:/usr/share/wazuh-indexer/config/certs/admin-key.pem -v /share_docker/config/wazuh_indexer/wazuh.indexer.yml:/usr/share/wazuh-indexer/config/opensearch.yml -v /share_docker/config/wazuh_indexer/internal_users.yml:/usr/share/wazuh-indexer/plugins/opensearch-security/securityconfig/internal_users.yml --network net -h wazuh.indexer -p 9200:9200 --log-driver syslog --log-opt tag=DOCKER_manager_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-indexer:4.5.0

# get wazuh dashboard image
docker pull wazuh/wazuh-dashboard:4.5.0

# create wazuh dashboard container
docker create --name CTX_w_dashboard --restart unless-stopped -e INDEXER_URL=$WZ_indexer_url -e INDEXER_USERNAME=$WZ_indexer_user -e INDEXER_PASSWORD=$WZ_indexer_password  -e API_USERNAME=$WZ_api_username -e API_PASSWORD=$WZ_api_password -e WAZUH_API_URL=$WZ_manager_url -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.dashboard.pem:/usr/share/wazuh-dashboard/certs/wazuh-dashboard.pem -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.dashboard-key.pem:/usr/share/wazuh-dashboard/certs/wazuh-dashboard-key.pem -v /share_docker/config/wazuh_indexer_ssl_certs/root-ca.pem:/usr/share/wazuh-dashboard/certs/root-ca.pem -v /share_docker/config/wazuh_dashboard/opensearch_dashboards.yml:/usr/share/wazuh-dashboard/config/opensearch_dashboards.yml -v /share_docker/config/wazuh_dashboard/wazuh.yml:/usr/share/wazuh-dashboard/data/wazuh/config/wazuh.yml --network net -h wazuh.dashboard -p 443:5601 --log-driver syslog --log-opt tag=DOCKER_manager_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-dashboard:4.5.0

# start container
docker start CTX_watchtower
docker start CTX_w_cert_genrator
docker start CTX_w_manager
docker start CTX_w_indexer
docker start CTX_w_dashboard

# one shitty line
mkdir -p /share_docker/watchtower /share_docker/wazuh_api_configuration /share_docker/wazuh_etc /share_docker/wazuh_logs /share_docker/wazuh_queue /share_docker/wazuh_var_multigroups /share_docker/wazuh_integrations /share_docker/wazuh_active_response /share_docker/wazuh_agentless /share_docker/wazuh_wodles /share_docker/filebeat_etc /share_docker/filebeat_var /share_docker/config/wazuh_indexer_ssl_certs/ /share_docker/config/wazuh_cluster/ /share_docker/wazuh-indexer-data && chown 1000:1000 -R /share_docker/* && echo -e "PUID=1000\nPGID=1000\nTZ=Europe/Rome\nWATCHTOWER_CLEANUP=true\nWATCHTOWER_INCLUDE_STOPPED=false\nWATCHTOWER_MONITOR_ONLY=false\nWATCHTOWER_SCHEDULE=0 0 4 * * *\nWATCHTOWER_TIMEOUT=10s" > /share_docker/watchtower/run.env && docker network create net && docker pull containrrr/watchtower && docker create --name CTX_watchtower --restart unless-stopped --env-file /share_docker/watchtower/run.env -v /var/run/docker.sock:/var/run/docker.sock --network net --log-driver syslog --log-opt tag=DOCKER_watchtower --log-opt syslog-address=udp://10.20.0.30:514 containrrr/watchtower && docker pull wazuh/wazuh-certs-generator:0.0.2 && git clone https://github.com/wazuh/wazuh-docker.git -b v4.9.2 && cp -r wazuh-docker/single-node/config /share_docker/ && rm -rf wwazuh-docker && chown 1000:1000 -R /share_docker/*  && docker create --name CTX_w_cert_genrator --restart unless-stopped -v /share_docker/config/wazuh_indexer_ssl_certs/:/certificates/ -v /share_docker/config/certs.yml:/config/certs.yml --network net -h certs-generator-wazuh --log-driver syslog --log-opt tag=DOCKER_certs_gen_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-certs-generator:0.0.2 && docker pull wazuh/wazuh-manager:4.5.0 && docker create --name CTX_w_manager --restart unless-stopped -e INDEXER_URL=$WZ_indexer_url -e INDEXER_USERNAME=$WZ_indexer_user -e INDEXER_PASSWORD=$WZ_indexer_password -e FILEBEAT_SSL_VERIFICATION_MODE=$WZ_ssl_ver -e SSL_CERTIFICATE_AUTHORITIES=$WZ_ssl_cert_auth -e SSL_CERTIFICATE=$WZ_ssl_crt -e SSL_KEY=$WZ_key -e API_USERNAME=$WZ_api_username -e API_PASSWORD=$WZ_api_password -v /share_docker/wazuh_api_configuration:/var/ossec/api/configuration -v /share_docker/wazuh_etc:/var/ossec/etc -v /share_docker/wazuh_logs:/var/ossec/logs -v /share_docker/wazuh_queue:/var/ossec/queue -v /share_docker/wazuh_var_multigroups:/var/ossec/var/multigroups -v /share_docker/wazuh_integrations:/var/ossec/integrations -v /share_docker/wazuh_active_response:/var/ossec/active-response/bin -v /share_docker/wazuh_agentless:/var/ossec/agentless -v /share_docker/wazuh_wodles:/var/ossec/wodles -v /share_docker/filebeat_etc:/etc/filebeat -v /share_docker/filebeat_var:/var/lib/filebeat -v /share_docker/config/wazuh_indexer_ssl_certs/:/etc/ssl -v /share_docker/config/wazuh_cluster/wazuh_manager.conf:/wazuh-config-mount/etc/ossec.conf --network net -h wazuh.manager -p 1514:1514 -p 1515:1515 -p 514:514/udp -p 55000:55000 wazuh/wazuh-manager:4.5.0 && docker pull wazuh/wazuh-indexer:4.5.0 && docker create --name CTX_w_indexer --restart unless-stopped --ulimit nofile=65536:65536 -e OPENSEARCH_JAVA_OPTS="$WZ_java_opt" -v /share_docker/wazuh-indexer-data:/var/lib/wazuh-indexer -v /share_docker/config/wazuh_indexer_ssl_certs/root-ca.pem:/usr/share/wazuh-indexer/config/certs/root-ca.pem -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.indexer-key.pem:/usr/share/wazuh-indexer/config/certs/wazuh.indexer.key -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.indexer.pem:/usr/share/wazuh-indexer/config/certs/wazuh.indexer.pem -v /share_docker/config/wazuh_indexer_ssl_certs/admin.pem:/usr/share/wazuh-indexer/config/certs/admin.pem -v /share_docker/config/wazuh_indexer_ssl_certs/admin-key.pem:/usr/share/wazuh-indexer/config/certs/admin-key.pem -v /share_docker/config/wazuh_indexer/wazuh.indexer.yml:/usr/share/wazuh-indexer/config/opensearch.yml -v /share_docker/config/wazuh_indexer/internal_users.yml:/usr/share/wazuh-indexer/plugins/opensearch-security/securityconfig/internal_users.yml --network net -h wazuh.indexer -p 9200:9200 --log-driver syslog --log-opt tag=DOCKER_manager_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-indexer:4.5.0 && docker pull wazuh/wazuh-dashboard:4.5.0 && docker create --name CTX_w_dashboard --restart unless-stopped -e INDEXER_URL=$WZ_indexer_url -e INDEXER_USERNAME=$WZ_indexer_user -e INDEXER_PASSWORD=$WZ_indexer_password  -e API_USERNAME=$WZ_api_username -e API_PASSWORD=$WZ_api_password -e WAZUH_API_URL=$WZ_manager_url -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.dashboard.pem:/usr/share/wazuh-dashboard/certs/wazuh-dashboard.pem -v /share_docker/config/wazuh_indexer_ssl_certs/wazuh.dashboard-key.pem:/usr/share/wazuh-dashboard/certs/wazuh-dashboard-key.pem -v /share_docker/config/wazuh_indexer_ssl_certs/root-ca.pem:/usr/share/wazuh-dashboard/certs/root-ca.pem -v /share_docker/config/wazuh_dashboard/opensearch_dashboards.yml:/usr/share/wazuh-dashboard/config/opensearch_dashboards.yml -v /share_docker/config/wazuh_dashboard/wazuh.yml:/usr/share/wazuh-dashboard/data/wazuh/config/wazuh.yml --network net -h wazuh.dashboard -p 443:5601 --log-driver syslog --log-opt tag=DOCKER_manager_wazuh --log-opt syslog-address=udp://10.20.0.30:514 wazuh/wazuh-dashboard:4.5.0 && docker start CTX_watchtower && docker start CTX_w_cert_genrator && docker start CTX_w_manager && docker start CTX_w_indexer && docker start CTX_w_dashboard
